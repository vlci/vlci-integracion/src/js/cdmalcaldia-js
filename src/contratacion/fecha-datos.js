import { resultsetWithFields } from 'vlcishared';
import { TEXTS_CONTRATACION } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

export function setFechaDatosRecientes(datos) {
  const resultSet = resultsetWithFields(datos);

  document.querySelector(
    '#ultimaActualizacion',
  ).textContent = `${TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('last_update')} ${
    resultSet[0].fecha_maxima
  }`;

  document.querySelector('#anyo').textContent = `${TEXTS_CONTRATACION[
    DEFAULT_LANGUAGE
  ].get('current_year')} ${resultSet[0].fecha_maxima.substring(10, 6)}`;
}
