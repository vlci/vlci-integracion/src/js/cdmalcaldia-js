export { echartSumaTipoContrato } from './suma-tipo-contrato';
export { echartOfertasPorArea } from './ofertas-por-area';
export { echartSumaPorArea } from './suma-por-area';
export { echartContratosPorDuracion } from './contratos-por-duracion';
export { setFechaDatosRecientes } from './fecha-datos';
export { indicadoresMacro } from './indicadores-macro';
