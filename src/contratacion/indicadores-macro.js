import {
  printDataInHTMLElements,
  resultsetWithFields,
  formatMilMillionEuros,
  formatDayMonthYear,
} from 'vlcishared';

export function indicadoresMacro(data) {
  const dataConfig = {
    totalImporte: {
      value: {
        element: '#valorGlobal',
        format: formatMilMillionEuros,
      },
      calculationperiod: {
        element: '#fechaDatos',
        format: formatDayMonthYear,
      },
    },
  };
  const dataWithFields = resultsetWithFields(data);
  printDataInHTMLElements(dataWithFields, dataConfig);
}
