import { resultsetWithFields, initChart } from 'vlcishared';
import { COLORS } from '../colors';
import { TEXTS_CONTRATACION } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

function contractsByMonthformatter(params) {
  // Check if the parameter indicating the month is provided
  if (params.name) {
    // If params.name is provided, return the original string format
    return `${params.name} ${TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get(
      'months',
    )}: ${params.value} ${TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get(
      'contracts',
    )}`;
  } else {
    // If params.name is not provided, assume a total is being requested
    return `Total: ${params.value} ${TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('contracts')}`;
  };
}


function contractsByMonthTooltipformatter(params) {
  const colorSpan = (color) =>
    `<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;font-weight: 500;background-color:${color}"></span>`;
  if (params.name) {
    // If params.name is provided, return the original string format
    return `${params.name} ${TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get(
      'months',
    )}: <span style="font-weight: 600; color: #565656;">${params.value} ${TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get(
      'contracts',
    )}</span>`;
  } else {
    // If params.name is not provided, assume a total is being requested
    return `Total:<span style="font-weight: 600; color: #565656;"> ${params.value} ${TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('contracts')} </span>`;
  };
}

const option = {
  color: [
    COLORS['tealPalido'],
    COLORS['naranjaPalido'],
    COLORS['amarilloPalido'],
    COLORS['azulPalido'],
    COLORS['cranberryPalido'],
    COLORS['marinoPalido'],
    COLORS['naranja'],
    COLORS['teal'],
    COLORS['amarillo'],
    COLORS['azulCeruleo'],
    COLORS['cranberry'],
    COLORS['marino'],
  ],
  grid: {
    left: '0',
    right: '0',
    bottom: '0',
    top: '50',
    containLabel: true,
  },
  title: {
    text: TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get(
      'contratos_por_duracion_title',
    ),
    left: 'center',
    top: '0',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  tooltip: {
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    formatter: contractsByMonthTooltipformatter,
  },
  legend: {show: true,},
  toolbox: {
    show: true,
    feature: {
      saveAsImage: {
        title: TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('download'),
      },
    },
  },
  series: [
    {
      type: 'treemap',
      roam: false,
      nodeClick: 'zoomToNode',
      data: [], 
      colorMappingBy: 'index',
      visualMax: 500,
      itemStyle: {
        gapWidth: 5,
      },
      label: {
        textStyle: {
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        position: 'insideBottomLeft',
        formatter: contractsByMonthformatter,
      },
      breadcrumb: {
        show: false,
      },
    },
  ],
};

export function echartContratosPorDuracion(datos) {
  const resultSet = resultsetWithFields(datos);
  resultSet.forEach((row) => {
    option.series[0].data.push({
      name: row.duracion_contrato_meses,
      value: row.num_contratos,
    });
  });
  initChart('chartContratosPorDuracion', option);
}
