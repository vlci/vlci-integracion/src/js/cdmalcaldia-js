//import { resultsetWithFields, initChart, addPointInteger } from 'vlcishared';
import {
  resultsetWithFields,
  initChart,
  formatToMillions,
  addPointInteger,
  formatTooltip,
} from 'vlcishared';
import { COLORS } from '../colors';
import { TEXTS_CONTRATACION } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

const option = {
  color: [
    COLORS['naranjaPalido'],
    COLORS['tealPalido'],
  ],
  grid: {
    left: '0',
    right: '0',
    bottom: '30',
    top: '100',
    containLabel: true,
  },
  title: {
    show: false,
    text: TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('anual_per_type'),
  },
  legend: {
    show: true,
    orient: 'horizontal',
    bottom: '0',
    left: 'center',
    width: '100%',
    textStyle: {
      fontSize: 11,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      restore: {
        title: TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('reset'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        }, 
      },
      saveAsImage: {
        title: TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      }
    },
    emphasis: { 
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },        
    },
  },
  tooltip: {
    trigger: 'axis',
    confine: true,
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    formatter: (params) => formatTooltip(params, true, true, '€'),
  },
  xAxis: {
    type: 'category',
    axisLabel: {
      textStyle: {
        fontFamily: 'Montserrat',
        fontSize: 8,
      },
    },
    data: [],
  },
  yAxis: {
    type: 'value',
    position: 'left',
    axisLabel: {
      textStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
      },
      formatter: (value) => `${formatToMillions(value)} €`,
    },
  },
  series: [
    {
      data: [],
      type: 'bar',
      barWidth: 23,
    },
    {
      type: 'bar',
      barWidth: 23,
      data: [],
    },
  ],
  media: [
    {
        query: {
            minWidth: 420,
        },
        option: {
          grid: {
            top: '85',
          },
          xAxis: {
            axisLabel: {
              textStyle: {
                fontSize: 9,
              },
            },
          },
          series: [
            {
              barWidth: 28,
            },
            {
              barWidth: 28,
            },
          ],
        },
    },
  ]
};

export function echartSumaTipoContrato(datos) {
  const resultSet = resultsetWithFields(datos);

  resultSet.forEach((row) => {
    option.xAxis.data.push(row.tipo_contrato);
    option.series[1].data.push(row.current_year);
    option.series[0].data.push(row.year_before);
  });

  const currentYear = resultSet[0].current_year_number;
  const yearBefore = resultSet[0].year_before_number;
  option.series[1].name = currentYear;
  option.series[0].name = yearBefore;

  initChart('chartSumaPorTipoContrato', option);
}
