import { resultsetWithFields, initChart, addPointInteger, formatTooltip } from 'vlcishared';
import { COLORS } from '../colors';
import { TEXTS_CONTRATACION } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

const option = {
  grid: {
    left: '0',
    right: '3%',
    bottom: '0%',
    top: '15%',
    containLabel: true,
  },
  title: {
    text: TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('suma_por_area_title'),
    left: 'center',
    top: '0',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    show: false,
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
        title: TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      }
    },
    emphasis: { 
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },        
    },
  },
  tooltip: {
    trigger: 'axis',
    confine: true,
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontWeight: 600,
      fontSize: 12,
    },
    
    formatter: (params) => formatTooltip(params, true, true, '€'),
  },
  xAxis: [
    {
      type: 'value',
      boundaryGap: [0, 0.01],
      axisLabel: {
        formatter: (value) => addPointInteger(value),
        textStyle: {
          fontSize: 11,
          fontFamily: 'Montserrat',
          fontSize: 10,
          lineHeight: 13,
        },
      },
    },
  ],
  yAxis: {
    type: 'category',
    axisTick: {show: false,},
    axisLabel: {
      textStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
        fontWeight: 400,
      },
    },
    data: [],
  },
  series: [
    {
      name: TEXTS_CONTRATACION[DEFAULT_LANGUAGE].get('amount'),
      type: 'bar',
      data: [],
      color: COLORS['naranjaPalido'],
    },
  ],
};

export function echartSumaPorArea(datos) {
  const resultSet = resultsetWithFields(datos);
  resultSet.forEach((row) => {
    option.yAxis.data.push(row.area);
    option.series[0].data.push(row.importe_sin_iva);
  });

  initChart('chartSumaPorArea', option);
}
