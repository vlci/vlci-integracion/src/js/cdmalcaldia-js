import { buildCamarasTrafico } from 'vlcishared';

export function printCamarasTrafico() {
  const IFRAME_ACCESO_CIUDAD = '#if-accesos-ciudad';
  const IFRAME_VIAS_PRINCIPALES = '#if-vias-principales';
  const SECTION_CONTAINER = '#camaras-section-container';
  const DIV_CAMARAS = 'div.camaras';
  const BLUR_BUTTON = '#blur-button';
  const MILLIS_BEFORE_BLUR = 80000;

  buildCamarasTrafico(
    IFRAME_ACCESO_CIUDAD,
    IFRAME_VIAS_PRINCIPALES,
    SECTION_CONTAINER,
    DIV_CAMARAS,
    BLUR_BUTTON,
    MILLIS_BEFORE_BLUR,
  );
}
