export const COLORS = [  
  '#48A9A6', // teal palido
  '#ffbf69', // amarillo palido
  '#4BA3C3', // azul palido
  '#e56b6f', // cranberry palido
  '#284B63', // marino palido
  '#F96E46', // naranja
  '#307473', // teal
  '#F6AE2D', // amarillo
  '#05668d', // azul ceruleo
  '#A23E48', // cranberry
  '#0B3948', // marino
  '#F1AB86', // naranja palido
];