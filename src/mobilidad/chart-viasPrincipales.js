import {
  initBarChart,
  addPointInteger,
} from 'vlcishared';
import { DEFAULT_LANGUAGE, MONTHS_YEAR } from '../common-language';
import { TEXTS_MOVILIDAD } from './texts';

export function echartViasPrincipales(datos) {
  const chartName = `${TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get(
    'chartNameViasPrincipales',
  )}`;

  
  const chartMonth =  `${TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('chartNameAcumulado')} ${MONTHS_YEAR[DEFAULT_LANGUAGE].get(
    datos.resultset[0][1].toString(),
  ).toUpperCase()}`;

    const dataArray = datos.resultset.reverse().map((element) => [element[0], element[2]]);
    
    const updateOptions = {
      grid: { 
        top: '15%',
        bottom: '3%',
        left: '3%',
        right: '3%',
      },
      title: {
        text: chartName,
        margin:  '0',
        padding: 0,
        top: '0',
        left: '50%',
        textAlign: 'center',
        itemGap: 5,
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 18,
          fontWeight: 600,
          color: '#666',
          width: '100%',
        },
        subtext: chartMonth,
        subtextStyle: {
          fontFamily: 'Montserrat',
          fontSize: 17,
          fontWeight: 500,
          color: '#666',
          width: '100%',
        },
      },
      tooltip: {
        show: true,
        textStyle: {
          fontFamily: 'Montserrat',
        },
        renderMode: 'html',
        formatter: (value) => `<b style="color: #000;">${value.name}</b>:<br /> ${addPointInteger(value.value)} ${TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('IntensidadUnidad')} `,
      },
      yAxis: {
        data: dataArray.map((element) => element[0]),
      },
      label: {
        show: false,
      },
      series: [
        {
          data: dataArray.map((element) => element[1]),
          label: {
            show: true,
            position: ['5%', '30%'],
            formatter: '{b}',
            fontSize: 19,
            lineHeight: 19,
            align: 'left',
            color: '#000',
            width: '440',
            overflow: 'truncate',
            ellipsis: '...',
            textStyle: {
                fontFamily: 'Montserrat',
                fontWeight: '500',
                color: '#000',
              },
            },
          itemStyle: {
            color: (params) => {
              const colors = [
                '#B7C258',
                '#A3BF62',
                '#8EBC6D',
                '#7AB877',
                '#65B581',
                '#50B28B',
              ];
              return colors[params.dataIndex];
            },
          },
        },
      ],
      media: [
        {
            query: {
                maxAspectRatio: 1
            },
            option: {
            }
        },
        {
            query: {
                maxWidth: 350,
            },
            option: {
                series: [
                  {
                    label: {
                      position: ['5%', '25%'],
                      fontSize: 16,
                      width: '350',
                    },
                  },
                ],
            }
        },
        {
            query: {
                maxWidth: 306,
            },
            option: {
              title: {
                textStyle: {
                  fontSize: 15,
                },
                 subtextStyle: {
                  fontSize: 15,
                },
              },
                series: [
                  {
                    label: {
                      position: ['5%', '20%'],
                      fontSize: 16,
                      width: '270',
                    },
                  },
                ],
            }
        },
        {
          query: {
              maxWidth: 275,
          },
          option: {
            title: {
              textStyle: {
                fontSize: 15,
              },
               subtextStyle: {
                fontSize: 15,
              },
            },
              series: [
                {
                  label: {
                    position: ['5%', '30%'],
                    fontSize: 14,
                    lineHeight: 14,
                    width: '250',
                  },
                },
              ],
          }
      },
      {
        query: {
            maxWidth: 265,
        },
        option: {
          title: {
            textStyle: {
              fontSize: 14,
            },
             subtextStyle: {
              fontSize: 14,
            },
          },
            series: [
              {
                label: {
                  position: ['5%', '30%'],
                  fontSize: 14,
                  lineHeight: 14,
                  width: '250',
                },
              },
            ],
        }
    },
        {
          query: {
              maxWidth: 255,
          },
          option: {
            title: {
              textStyle: {
                fontSize: 13,
              },
               subtextStyle: {
                fontSize: 13,
              },
            },
              series: [
                {
                  label: {
                    position: ['5%', '20%'],
                    fontSize: 13,
                    lineHeight: 13,
                    width: '235',
                  },
                },
              ],
          }
      },
      {
        query: {
            maxWidth: 230,
        },
        option: {
          title: {
            textStyle: {
              fontSize: 13,
            },
             subtextStyle: {
              fontSize: 13,
            },
          },
            series: [
              {
                label: {
                  position: ['5%', '20%'],
                  fontSize: 13,
                  lineHeight: 13,
                  width: '220',
                },
              },
            ],
        }
    },
    ],
    };

    initBarChart('chartViasPrincipales', updateOptions);
}
