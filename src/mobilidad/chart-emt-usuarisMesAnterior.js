import {
  configureDataGaugeMonthCommon,
  printNoDataChart,
  printDataChart,
  initChart,
} from 'vlcishared';
import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_MOVILIDAD } from './texts';
import { defaultOptions } from '../common-options/gauge-month';

export function echartEmtUsuariosMesAnterior(datos) {
  const misDatosArray = datos.resultset;
  const idChart = 'chartEmtUsuariosMesAnterior';
  const idNoData = 'no_dataEmtUsuariosMesAnterior';
  if (datos.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);
    const objColData = {
      type: 0,
      time: 1,
      data: 2,
    };

    const [maxValue, seriesDetailFormatter, gaugeDataMonth] =
      configureDataGaugeMonthCommon(idChart, misDatosArray, objColData, 1);

    defaultOptions.title.text = TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get(
      'chartNameUsuariosMesAnterior',
    );
    defaultOptions.series.max = maxValue;
    defaultOptions.series.detail.formatter = seriesDetailFormatter;
    defaultOptions.series.data = gaugeDataMonth;
    defaultOptions.series.progress.itemStyle.color = COLORS['naranjaPalido'];

    initChart(idChart, defaultOptions);
  }
}
