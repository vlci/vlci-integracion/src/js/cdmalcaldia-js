import {
  configureDataGaugeCommon,
  printNoDataChart,
  printDataChart,
  initChart,
  addPointInteger,
} from 'vlcishared';
import { COLORS } from './constants';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_MOVILIDAD } from './texts';
import { defaultOptions } from '../common-options/gauge-year';
import { formatRoundTooltip } from '../common-chart';

export function echartEmtUsuariosUltimosAnyos(datos) {
  const idChart = 'chartEmtUsuariosUltimosAnyos';
  const idNoData = 'no_dataEmtUsuariosUltimosAnyos';
  const fontSize = undefined;

  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    const objColData = {
      type: 0,
      data: 1,
      time: 0,
    };

    const chartName = TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get(
      'chartNameUsuariosUltimosAnyos',
    );

    const [maxValue, gaugeDataYears] = configureDataGaugeCommon(
      datos.resultset,
      objColData,
      COLORS.slice(),
      fontSize,
      30,
      80,
      -100,
      100,
    );

    gaugeDataYears.forEach((obj) => {
      const objeto = obj;
      objeto.title.show = false;
      objeto.detail.show = false;
    });

    const options = JSON.parse(JSON.stringify(defaultOptions));

    options.title.text = chartName;
    //options.title.textStyle.fontSize = fontSize;
    options.series.max = maxValue;
    options.series.data = gaugeDataYears;
    //options.series.detail.fontSize = fontSize;
    //options.series.center = ['50%', '70%'];
    options.color = COLORS.slice();
    options.series.axisLabel.formatter = (value) =>
      addPointInteger(Math.round(value));
    options.tooltip.formatter = (params) => formatRoundTooltip(params, false);

    initChart(idChart, options);
  }
}
