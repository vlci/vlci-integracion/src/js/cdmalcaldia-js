import {
  configureDataLine,
  printNoDataChart,
  printDataChart,
  formatTooltip,
  initChart,
  calculateXAxisStart,
} from 'vlcishared';

import * as constants from './constants';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_MOVILIDAD } from './texts';

export function echartUsuarisTotals(datos) {
    const chartName = TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('chartNameUsuarisTotals');
    const yAxisName = TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('UnidadMedida');
    const categoryName = TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('UsuarisTotalsValor');
    const idChart = 'chartUsuarisTotals';
    const idNoData = 'no_dataUsuarisTotals';

    if (datos.length === 0) {
      printNoDataChart(`#${idChart}`, `#${idNoData}`);
    } else {
      printDataChart(`#${idNoData}`, `#${idChart}`);

    const [series, date, tiposArray] = configureDataLine(
      datos.resultset,
      constants.COLORS.slice(),
      false,
    );

    defaultOptions.title.text = chartName;
    if (tiposArray) {
      defaultOptions.legend.data = tiposArray;
    }
    defaultOptions.xAxis.data = Array.from(date.values());
    defaultOptions.yAxis.yAxisName = yAxisName;
    defaultOptions.series = series;
    defaultOptions.dataZoom[0].data = Array.from(date.values());
    defaultOptions.dataZoom[0].start = calculateXAxisStart(date.size);

    initChart(idChart, defaultOptions);
  }
}

const defaultOptions = {
  grid: {
    left: '0',
    right: '0',
    top: '50',
    bottom: '40',
    containLabel: true,
  },
  title: {
    text: '',
    left: 'center',
    top: '0',
    margin: [0, 0, 30, 0],
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 15,
      fontWeight: 600,
      color: '#666',
      width: '100%',
    },
  },
  legend: {
    type: 'plain',
    data: null,
    orient: 'horizontal',
    top: '30',
    left: 'center',
    height: '50',
    width: '100%',
    textStyle: {
      fontSize: 11,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
    show: false,
  },
  toolbox: {
    show: true,
    right: '1%',
    top: '0',
    feature: {
      restore: {
        title: TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('reset'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        }, 
      },
      saveAsImage: {
        title: TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      }
    },
    emphasis: { 
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },        
    },
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    formatter: (params) => {
      params[0].seriesName = TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('UsuarisTotalsValor');      
      return formatTooltip(params, false, true, TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('UnidadMedida'));
    },
  },
  xAxis: {
    type: 'category',
    data: undefined,
    max: (values) => values.max +4,
    boundaryGap: false,
    margin: 10,
    splitLine: {
      show: false,
    },
    axisLabel: {
      margin: 10,
      alignMaxLabel: 'left',
      textStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
        color: '#666',
        fontWeight: 400,
      },
    },
  },
  yAxis: {
    type: 'value',
    scale: 'true',
    splitNumber: '5',
    axisLabel: {
      margin: 10,
      textStyle: {
        fontSize: 11,
        fontFamily: 'Montserrat',
        fontWeight: 400,
        color: '#666',
        width: '100%',
      },
    },
  },
  series: {
    name: TEXTS_MOVILIDAD[DEFAULT_LANGUAGE].get('UsuarisTotalsValor'),
  },
  dataZoom: [
    {
      show: true,
      bottom: '0',
      data: undefined,
      start: undefined,
      end: 100,
      borderColor: '#eee',
      fillerColor: 'rgba(0,0,0,.05)',
      textStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
        color: '#333',
        textBorderColor: '#fff',
        textBorderWidth: 3,
        fontWeight: '600',
      },
      selectedDataBackground: {
        lineStyle: {
          color: '#333',
        },
        areaStyle: {
          color: '#F1AB86',
        },
      },
      dataBackground: {
        backgroundColor: 'rgba(0,0,0,1)',
        lineStyle: {
          color: '#333',
        },
        areaStyle: {
          color: 'rgba(0,0,0,0.5)',
        },
      },
      handleStyle: {
        color: 'rgba(0,0,0,0.2)',
      },
      moveHandleStyle: {
        color: 'rgba(0,0,0,0.3)',
      },
      emphasis: {
        handleStyle: {
          color: 'rgba(255,205,0,1)',
        },
        moveHandleStyle: {
          color: 'rgba(255,205,0,1)',
        },
      },
    },
  ],
};
