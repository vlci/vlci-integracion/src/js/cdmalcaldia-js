import { getEnvironment } from 'vlcishared';

const externalLinks = {
  PRO: {
    pmp: 'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3AGE%3AMenu_GE_PagamentProveidors.wcdf/generatedContent',
    movilidad_top6:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ACiutat%3AVehicles.wcdf/generatedContent',
    movilidad_emt:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ACiutat%3AUs_Demt.wcdf/generatedContent',
    ruido:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ACiutat%3ASoroll.wcdf/generatedContent',
    residuos:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ACiutat%3AResidus.wcdf/generatedContent',
    reciclaje:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ACiutat%3AResidus.wcdf/generatedContent',
    contaminacion_aire:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ACiutat%3AContaminacio.wcdf/generatedContent',
    actividad_admin:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3AGE%3AMenu_GE-RendimentAdministratiu.wcdf/generatedContent',
    acceso_sede_electronica:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ACiutat%3ASeu_Electronica.wcdf/generatedContent',
    cuadro_ciudad:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ACiutat%3AQuadre_de_Comandament.wcdf/generatedContent',
    cuadro_unificado:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ASmartCity%3AQuadre_Unificado.wcdf/generatedContent',
    cuadro_gestion_economica:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3AGE%3AQuadre_de_GE.wcdf/generatedContent',
    cuadro_movilidad:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3AMobilitat%3AMobilitat.wcdf/generatedContent',
    contaminacion_acustica:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia%3ACiutat%3ASoroll.wcdf/generatedContent',
  },
  PRE: {
    pmp: 'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3AGE%3AMenu_GE_PagamentProveidors.wcdf/generatedContent',
    movilidad_top6:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ACiutat%3AVehicles.wcdf/generatedContent',
    movilidad_emt:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ACiutat%3AUs_Demt.wcdf/generatedContent',
    ruido:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ACiutat%3ASoroll.wcdf/generatedContent',
    residuos:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ACiutat%3AResidus.wcdf/generatedContent',
    reciclaje:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ACiutat%3AResidus.wcdf/generatedContent',
    contaminacion_aire:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ACiutat%3AContaminacio.wcdf/generatedContent',
    actividad_admin:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3AGE%3AMenu_GE-RendimentAdministratiu.wcdf/generatedContent',
    acceso_sede_electronica:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ACiutat%3ASeu_Electronica.wcdf/generatedContent',
    cuadro_ciudad:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ACiutat%3AQuadre_de_Comandament.wcdf/generatedContent',
    cuadro_unificado:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ASmartCity%3AQuadre_Unificado.wcdf/generatedContent',
    cuadro_gestion_economica:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3AGE%3AQuadre_de_GE.wcdf/generatedContent',
    cuadro_movilidad:
      'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3AMobilitat%3AMobilitat.wcdf/generatedContent',
      contaminacion_acustica:
        'https://cdm.vlci.valencia.es/pentaho/api/repos/%3Apublic%3Asc_valencia_pre%3ACiutat%3ASoroll.wcdf/generatedContent',
  },
};

export function getLink(id) {
  let entorno = getEnvironment();
  if (entorno === 'local') {
    entorno = 'PRE';
  }
  const url = externalLinks[entorno][id];
  return url;
}
