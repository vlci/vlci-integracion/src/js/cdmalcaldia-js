export const TEXTS_GASTOS = {
  es_ES: new Map([
    ['chartNameGastosCap6', 'CAPÍTULO 6 - INVERSIÓN'],
    ['chartNameGastosCap6Dic', 'CAPÍTULO 6 - INVERSIÓN (FINALES DE AÑO)'],
    ['chartNameGastosCap2', 'CAPÍTULO 2 - GASTO CORRIENTE'],
    ['chartNameGastosCap2Dic', 'CAPÍTULO 2 - GASTO CORRIENTE (FINALES DE AÑO)'],
    ['cred_definitivo', 'Crédito Definitivo'],
    ['importe_obligacion', 'Obligaciones reconocidas'],
    ['oblig_div_cred_definitivo', '% Ejecución mensual'],
    ['reset', 'Restablecer'],
    ['download', 'Descargar'],
  ]),
  ca_ES: new Map([
    ['chartNameGastosCap6', 'CAPÍTOL 6 - INVERSIÓ'],
    ['chartNameGastosCap6Dic', "CAPÍTOL 6 - INVERSIÓ (FINALS D'ANY)"],
    ['chartNameGastosCap2', 'CAPÍTOL 2 - DESPESA CORRENT'],
    ['chartNameGastosCap2Dic', "CAPÍTOL 2 - DESPESA CORRENT (FINALS D'ANY)"],
    ['cred_definitivo', 'Crèdit Definitiu'],
    ['importe_obligacion', 'Obligacions reconegudes'],
    ['oblig_div_cred_definitivo', '% Execució mensual'],
    ['reset', 'Restablir'],
    ['download', 'Desar'],
  ]),
};
