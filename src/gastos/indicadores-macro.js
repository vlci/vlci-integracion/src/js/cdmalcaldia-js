import {
  printDataInHTMLElements,
  formatMonthAnyo,
  resultsetWithFields,
  addPointInteger,
} from 'vlcishared';

export function indicadoresMacro(data) {
  const dataConfig = {
    gastosCap6: {
      value: {
        element: '#kpiCap6',
        format: (value) => `${addPointInteger(value)}%`,
      },
      calculationperiod: {
        element: '#fechakpiCap6',
        format: formatMonthAnyo,
      },
    },
    gastosCap2: {
      value: {
        element: '#kpiCap2',
        format: (value) => `${addPointInteger(value)}%`,
      },
      calculationperiod: {
        element: '#fechakpiCap2',
        format: formatMonthAnyo,
      },
    },
  };
  const dataWithFields = resultsetWithFields(data);
  printDataInHTMLElements(dataWithFields, dataConfig);
}
