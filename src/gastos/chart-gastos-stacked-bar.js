import {
  resultsetWithFields,
  formatMonthAnyo,
  formatShortMonthYear,
  printNoDataChart,
  printDataChart,
  initChart,
  formatTooltip,
  updateStackedBarOptions,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_GASTOS } from './texts';

const DATE = 'fecha';
const CREDITO_DEFINITIVO = 'credito_definitivo';
const OBLIG_RECONOCIDAS = 'importe_obligacion';
const OBLIG_DIV_DEFINITIVO = 'importe_obligacion_entre_credito_definitivo';
const dateWithMonthLength = 6;

/**
 * Opciones por defecto para todos los graficos stacked bar
 * Atributos que son necesarios rellenar : title{text},data,series.
 */
const options = {
  grid: {
    left: '0',
    right: '0',
    bottom: '30',
    top: '120',
  },
  title: {
    subtext: '',
    subtextStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
  },
  toolbox: {
    feature: {
      restore: {
        title: TEXTS_GASTOS[DEFAULT_LANGUAGE].get('reset'),
      },
      saveAsImage: {
        title: TEXTS_GASTOS[DEFAULT_LANGUAGE].get('download'),
      },
    },
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    formatter: (params) => {
      if (params[0].name.length === dateWithMonthLength) {
        // eslint-disable-next-line no-param-reassign
        params[0].name = formatMonthAnyo(params[0].name);
      }
      return formatTooltip(params, false, true);
    },
  },
  xAxis: [
    {
      axisLabel: {
        margin: 10,
        textStyle: {
          fontSize: 9,
          fontFamily: 'Montserrat',
          fontWeight: 400,
          color: '#333',
        },
        formatter: (value) => {
          if (value.length === dateWithMonthLength) {
            return formatShortMonthYear(value);
          }
          return value;
        },
      },
    },
  ],
  series: [
    {
      name: `${TEXTS_GASTOS[DEFAULT_LANGUAGE].get('cred_definitivo')}(€)`,
      yAxisIndex: 1,
      type: 'bar',
      barWidth: 22,
      data: [],
      color: COLORS.naranjaPalido,
    },
    {
      name: `${TEXTS_GASTOS[DEFAULT_LANGUAGE].get('importe_obligacion')}(€)`,
      yAxisIndex: 1,
      type: 'bar',
      barWidth: 22,
      data: [],
      color: COLORS.tealPalido,
    },
    {
      name: TEXTS_GASTOS[DEFAULT_LANGUAGE].get('oblig_div_cred_definitivo'),
      type: 'line',
      data: [],
      color: COLORS.cranberry,
    },
  ],
  media: [
    {
      query: {
        minWidth: 535,
      },
      option: {
        grid: {
          bottom: '30',
          top: '120',
        },
      },
    },
    {
      query: {
        minWidth: 574,
      },
      option: {
        grid: {
          bottom: '30',
          top: '100',
        },
        xAxis: [
          {
            axisLabel: {
              textStyle: {
                fontSize: 10,
              },
            },
          },
        ],
      },
    },
    {
      query: {
        minWidth: 635,
      },
      option: {
        grid: {
          bottom: '30',
          top: '100',
        },
      },
    },
  ],
};

function echartStackedBarGastos(idChart, idNoData, datos, nameChart) {
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    let updatedOptions = {};
    updatedOptions = updateStackedBarOptions(options);

    // Reiniciar datos
    updatedOptions.xAxis[0].data = [];
    updatedOptions.series.forEach((serie, index) => {
      updatedOptions.series[index].data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      updatedOptions.xAxis[0].data.push(`${row[DATE]}`);
      updatedOptions.series[0].data.push(row[CREDITO_DEFINITIVO]);
      updatedOptions.series[1].data.push(row[OBLIG_RECONOCIDAS]);
      updatedOptions.series[2].data.push(row[OBLIG_DIV_DEFINITIVO]);
    });

    updatedOptions.title.text = TEXTS_GASTOS[DEFAULT_LANGUAGE].get(nameChart);

    initChart(idChart, updatedOptions);
  }
}

export function echartStackedBarGastosCap6(vis, datos) {
  const idChart = 'chartCap6';
  const idNoData = 'no_dataCap6';
  const nameChart = 'chartNameGastosCap6';
  echartStackedBarGastos(idChart, idNoData, datos, nameChart);
}

export function echartStackedBarGastosCap6Dic(vis, datos) {
  const idChart = 'chartCap6Dic';
  const idNoData = 'no_dataCap6Dic';
  const nameChart = 'chartNameGastosCap6Dic';
  echartStackedBarGastos(idChart, idNoData, datos, nameChart);
}

export function echartStackedBarGastosCap2(vis, datos) {
  const idChart = 'chartCap2';
  const idNoData = 'no_dataCap2';
  const nameChart = 'chartNameGastosCap2';
  echartStackedBarGastos(idChart, idNoData, datos, nameChart);
}

export function echartStackedBarGastosCap2Dic(vis, datos) {
  const idChart = 'chartCap2Dic';
  const idNoData = 'no_dataCap2Dic';
  const nameChart = 'chartNameGastosCap2Dic';
  echartStackedBarGastos(idChart, idNoData, datos, nameChart);
}
