import {
  configureDataGaugeCommon,
  printNoDataChart,
  printDataChart,
  initChart,
  addPointInteger,
} from 'vlcishared';

import { TEXTS_AIGUA } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';
import { defaultOptions } from '../common-options/gauge-year';
import { formatRoundTooltip } from '../common-chart';

const COLORS_AGUA_BARRIOS_GAUGE_YEAR = [
  '#9c89b8',
  '#4ea8de',
  '#05668d',
  '#ffbf69',
  '#e56b6f',
  '#83c5be',
  '#02c39a',
  '#ee9b00',
  '#cdb4db',
  '#ffadad',
  '#ce4257',
  '#b7b7a4',
];

export function echartBarrisGaugeYear(vis, datos) {
  const misDatosArray = datos.resultset;
  const idChart = 'chartAguaBarriosGauge';
  const idNoData = 'noDataChartAguaBarriosGauge';
  const fontSize = 12;
  if (misDatosArray.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);
  }
  const objColData = {
    type: 0,
    time: 2,
    data: 1,
  };
  const chartName = `${TEXTS_AIGUA[DEFAULT_LANGUAGE].get(
    'chartNameBarrisGaugeYear',
  )} ${misDatosArray[0][objColData.time]}`;

  const [maxValue, gaugeDataYears] = configureDataGaugeCommon(
    misDatosArray,
    objColData,
    COLORS_AGUA_BARRIOS_GAUGE_YEAR,
    fontSize,
    15,
    30,
    -70,
    70,
  );

  const options = JSON.parse(JSON.stringify(defaultOptions));

  options.title.text = chartName;
  options.title.textStyle.fontSize = fontSize;
  options.series.max = maxValue;
  options.series.data = gaugeDataYears;
  options.series.detail.fontSize = fontSize;
  options.color = COLORS_AGUA_BARRIOS_GAUGE_YEAR;
  options.series.axisLabel.formatter = (value) =>
    addPointInteger(Math.round(value));
  options.tooltip.formatter = (params) => formatRoundTooltip(params, false);

  initChart(idChart, options);
}
