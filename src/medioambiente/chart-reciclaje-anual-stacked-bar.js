import {
  splitDataset,
  printNoDataChart,
  printDataChart,
  initStackedBarChart,
  initStackedBarChartMonthSeries,
  formatToTons,
} from 'vlcishared';
import { TEXTS_RESIDUOS } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

const COLORS_RECICLAJE_ANUAL = [
  '#0077b6', // Cartó
  '#ffc300', // Envasos
  '#55828b', // Mobles
  '#a26769', // Orgánica
  '#b1a7a6', // RSU Resta
  '#38b000', // Vidre
];

const colorsReciclable = COLORS_RECICLAJE_ANUAL.slice(0, -2).concat(
  COLORS_RECICLAJE_ANUAL.slice(-1),
);

export function echartKgsResidusStackedBarYears(vis, datos) {
  const idChart = 'chartResiduosContenedoresStackedBarYears';
  const idNoData = 'noDatachartResiduosContenedoresStackedBarYears';
  //const fontSize = 10;
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);
    const splittedData = splitDataset(datos.resultset);
    const dataContenidor = splittedData.Cont;


    // Opciones especificas sobre los datos
    const updateDataOptions = {
     label: {
      width: '50',
      overflow: 'truncate',
      ellipsis: 'Tn',
      textStyle: {
          fontWeight: 500,
      },
     },
    };

    const updateOptions = {
      grid: {
        left: '0',
        right: '30',
        bottom: '0',
        top: '70',
        containLabel: true,
      },
      title: {
        text: TEXTS_RESIDUOS[DEFAULT_LANGUAGE].get('chartNameStackedBarYears'),
        left: 'center',
        top: '0',
        textStyle: {
          fontSize: 15,
          fontFamily: 'Montserrat',
          fontWeight: 500,
          color: '#666',
        },
        subtext: '',
        subtextStyle: {
          fontSize: 15,
          fontFamily: 'Montserrat',
          fontWeight: 400,
          color: '#666',
        },
      },
      legend: {
        orient: 'horizontal',
        align: 'auto',
        top: '40',
        left: 'center',
        width: '100%',
        padding: 0,
        margin: 0,
        icon: 'circle',
        textStyle: {
          fontSize: 11,
          fontFamily: 'Montserrat',
          fontWeight: 400,
          color: '#333',
        },
      },
      toolbox: {
        show: true,
        right: '-5',
        top: '-2',
        feature: {
          restore: {
            title: 'Restablecer',
            textStyle: {
              fontSize: 6,
              fontFamily: 'Montserrat',
              fontWeight: 300,
            }, 
          },
          saveAsImage: {
            title: 'Guardar',
            textStyle: {
              fontSize: 6,
              fontFamily: 'Montserrat',
              fontWeight: 300,
            },
          }
        },
        emphasis: { 
          iconStyle: {
            borderColor: '#ffcd00',
            textFill: '#666',
          },        
        },
      },
      tooltip: {
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 12,
        },
      },
      xAxis: [
        {
          splitNumber: 5,
          max: (values) => values.max +1,
          axisLabel: {
            formatter: (value) => formatToTons(value),
            textStyle: {
              fontSize: 11,
              fontFamily: 'Montserrat',
              fontWeight: 400,
            },
          },
        },
      ],
      yAxis: {
        axisLine: { show: false,},
        axisTick: { show: false,},
        axisLabel: {
          textStyle: {
            fontSize: 10,
            fontFamily: 'Montserrat',
            fontWeight: 400,
            color: '#565656',
          },
        },
      },
    };

    initStackedBarChartMonthSeries(
      'chartResiduosContenedoresStackedBarYears',
      dataContenidor,
      updateOptions,
      updateDataOptions,
    );
  }
}
export function echartDesKgsHorStackedBar(vis, datos) {
  const idChart = 'chartResiduosReciclajeStackedBar';
  const idNoData = 'noDatachartResiduosReciclajeStackedBar';
  const fontSize = 10;
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);
    const misDatosArray = datos.resultset;
    const updateOptions = {
      color: colorsReciclable,
      title: {
        text: TEXTS_RESIDUOS[DEFAULT_LANGUAGE].get(
          'chartNameStackedBarRecyclingYears',
        ),
      },
      xAxis: {
        name: `\n\nKgs`,
        nameTextStyle: {
          padding: 0,
        },
        axisLabel: {
          textStyle: fontSize,
        },
      },
      yAxis: {
        axisLabel: {
          textStyle: fontSize,
        },
      },
    };

    initStackedBarChart(
      'chartResiduosReciclajeStackedBar',
      misDatosArray,
      updateOptions,
    );
  }
}
