import { resultsetWithFields, buildTableCalidadAire } from 'vlcishared';

export function tableCalidadAire(data) {
  const resultset = resultsetWithFields(data);
  const trElements = buildTableCalidadAire(resultset);
  trElements.forEach((tr) => {
    const tbody = document.querySelector('#tabla_dinamica > tbody');
    tbody.appendChild(tr);
  });
}
