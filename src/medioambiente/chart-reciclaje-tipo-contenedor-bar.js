import { initBarChart, updateObject, formatTooltip, formatToTons, } from 'vlcishared';
import { DEFAULT_LANGUAGE, MONTHS_YEAR } from '../common-language';

function getMonthList() {
  const monthsMap = MONTHS_YEAR[DEFAULT_LANGUAGE];
  const monthNames = Array.from(monthsMap.values()).map((monthName) =>
    monthName.slice(0, 3),
  );
  return monthNames;
}

function getLastYear(dataArray) {
  return Math.max(...dataArray.map((element) => Number(element[0])));
}

function getLastYearValues(dataArray) {
  const arrayUltimoAnyo = dataArray.filter(
    (arr) => Number(arr[0]) === getLastYear(dataArray),
  );
  const valoresUltimoAnyo = Array.from({ length: 12 }, () => 0);
  arrayUltimoAnyo.forEach(([anio, mes, valor]) => {
    const mesIndex = parseInt(mes, 10) - 1; // Restar 1 porque los meses comienzan desde 1
    valoresUltimoAnyo[mesIndex] = valor;
  });
  return valoresUltimoAnyo;
}

function getPenultimateYearValues(dataArray) {
  const arrayPenultimoAnyo = dataArray.filter(
    (arr) => Number(arr[0]) === getLastYear(dataArray) - 1,
  );
  const valoresPenultimoAnyo = Array.from({ length: 12 }, () => 0);
  arrayPenultimoAnyo.forEach(([anio, mes, valor]) => {
    const mesIndex = parseInt(mes, 10) - 1; // Restar 1 porque los meses comienzan desde 1
    valoresPenultimoAnyo[mesIndex] = valor;
  });
  return valoresPenultimoAnyo;
}

function getGenericOptions(dataArray) {
  const lastYearString = getLastYear(dataArray).toString();
  const penultimateYearString = (getLastYear(dataArray) - 1).toString();
  
  const genericOptions = {
    grid: {
      left: '0',
      right: '2%',
      bottom: '0',
      top: '45',
      containLabel: true,
    },
    title: {
      text: '',
      left: '40',
      top: '6',
      padding: 0,
      textStyle: {
        fontSize: 15,
        fontFamily: 'Montserrat',
        fontWeight: 500,
        color: '#333',
      },
      subtext: '',
      subtextStyle: {
        fontSize: 15,
        fontFamily: 'Montserrat',
        fontWeight: 400,
        color: '#666',
      },
    },  
    legend: {
      show: true,
      data: [penultimateYearString, lastYearString],
      orient: 'horizontal',
      top: '5',
      left: 'right',
      width: '100%',
      textStyle: {
        fontSize: 11,
        fontFamily: 'Montserrat',
        fontWeight: 400,
        color: '#333',
      },
    },
    tooltip: {
      show: true,
      trigger: 'axis',
      axisPointer: {
        type: 'shadow',
      },
      textStyle: {
        fontFamily: 'Montserrat',
        fontSize: 12,
      },
      formatter: (params) => {
          return formatTooltip(params, false, true, 'Kgs.');
      },
    },
    xAxis: [
      {
        type: 'category',
        axisLine: {
          lineStyle: {
            color: '#999',
          },
        },
        axisTick: {
          lineStyle: {
            color: '#ccc',
          },
        },
        axisLabel: {
          textStyle: {
            fontSize: 11,
            fontFamily: 'Montserrat',
            fontWeight: 400,
            color: '#333',
          },
        },
        data: getMonthList(),
      },
    ],
    yAxis: [
      {
        type: 'value',
        splitNumber: '3',
        axisLabel: {
          formatter: (value) => formatToTons(value),
          textStyle: {
            fontSize: 10,
            fontFamily: 'Montserrat',
            fontWeight: 400,
            color: '#333',
          },
        },
      },
    ],
    series: [
      {
        name: penultimateYearString,
        type: 'bar',
        barGap: 0,
        data: getPenultimateYearValues(dataArray),
      },
      {
        name: lastYearString,
        type: 'bar',
        data: getLastYearValues(dataArray),
      },
    ],
  };
  return genericOptions;
}

export function echartReciclajeOrganicos(datos) {
  const dataArray = datos.resultset;
  const specificOptions = {
    title: {
      text: 'Restos Orgánicos',
    },
    series: [
      {
        color: '#AFABAB',
      },
      {
        color: '#A5540C',
      },
    ],
  };

  const mergedOptions = updateObject(
    getGenericOptions(dataArray),
    specificOptions,
  );
  initBarChart('chartResiduosOrganicos', mergedOptions);
}

export function echartReciclajeEnvases(datos) {
  const dataArray = datos.resultset;
  const specificOptions = {
    title: {
      text: 'Envases de plástico',
    },
    series: [
      {
        color: '#AFABAB',
      },
      {
        color: '#F9EB22',
      },
    ],
  };

  const mergedOptions = updateObject(
    getGenericOptions(dataArray),
    specificOptions,
  );
  initBarChart('chartResiduosEnvases', mergedOptions);
}

export function echartReciclajeVidrio(datos) {
  const dataArray = datos.resultset;
  const specificOptions = {
    title: {
      text: 'Vidrio',
    },
    series: [
      {
        color: '#AFABAB',
      },
      {
        color: '#3BB24A',
      },
    ],
  };

  const mergedOptions = updateObject(
    getGenericOptions(dataArray),
    specificOptions,
  );
  initBarChart('chartResiduosVidrio', mergedOptions);
}

export function echartReciclajeCarton(datos) {
  const dataArray = datos.resultset;
  const specificOptions = {
    title: {
      text: 'Cartón',
    },
    series: [
      {
        color: '#AFABAB',
      },
      {
        color: '#5582C3',
      },
    ],
  };

  const mergedOptions = updateObject(
    getGenericOptions(dataArray),
    specificOptions,
  );
  initBarChart('chartResiduosCarton', mergedOptions);
}

export function echartReciclajeMuebles(datos) {
  const dataArray = datos.resultset;
  const specificOptions = {
    title: {
      text: 'Muebles',
    },
    series: [
      {
        color: '#AFABAB',
      },
      {
        color: '#C9773F',
      },
    ],
  };

  const mergedOptions = updateObject(
    getGenericOptions(dataArray),
    specificOptions,
  );
  initBarChart('chartResiduosMuebles', mergedOptions);
}
