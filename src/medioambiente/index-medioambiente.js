export {
  echartKgsResidusStackedBarYears,
  echartDesKgsHorStackedBar,
} from './chart-reciclaje-anual-stacked-bar';
export { echartBarrisGaugeYear } from './chart-agua-barrios-gauge-year';
export { tableCalidadAire } from './table-calidad-aire';
export {
  echartReciclajeOrganicos,
  echartReciclajeEnvases,
  echartReciclajeVidrio,
  echartReciclajeCarton,
  echartReciclajeMuebles
} from './chart-reciclaje-tipo-contenedor-bar';
