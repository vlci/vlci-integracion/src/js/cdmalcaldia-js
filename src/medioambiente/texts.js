export const TEXTS_RESIDUOS = {
  es_ES: new Map([
    ['chartNameStackedBarYears', 'TOTAL RESIDUOS POR MESES'],
    ['chartNameStackedBarRecyclingYears', 'RESIDUOS POR CONTENEDOR']
  ]),
  ca_ES: new Map([
    ['chartNameStackedBarYears', 'TOTAL RESIDUS PER MESOS'],
    ['chartNameStackedBarRecyclingYears', 'RESIDUS PER TIPUS DE CONTENIDOR'],
  ]),
};
export const TEXTS_AIGUA = {
  es_ES: new Map([['chartNameBarrisGaugeYear', 'EVOLUCIÓN DEL AÑO']]),
  ca_ES: new Map([['chartNameBarrisGaugeYear', 'EVOLUCIÓ ANY']]),
};
