export const COLORS = {
  naranjaPalido: '#F1AB86',
  tealPalido: '#48A9A6',
  amarilloPalido: '#ffbf69',
  azulPalido: '#4BA3C3',
  cranberryPalido: '#e56b6f',
  marinoPalido: '#284B63',
  naranja: '#F96E46',
  teal: '#307473',
  amarillo: '#F6AE2D',
  azulCeruleo: '#05668d',
  cranberry: '#A23E48',
  marino: '#0B3948',
  verdeClaro: '#6a9c81'
};

export const COLORS_GENDER = {
  azulMasculino: '#2673a5',
  lilaFemenino: '#ab91c9',
  azulMasculinoPalido: '#3d92c1',
  lilaFemeninoPalido: '#cdb4db',
  grisClaro: '#d3d3d3'
};
