import {
  resultsetWithFields,
  printNoDataChart,
  printDataChart,
  formatTooltip,
  initChart,
  formatName,
  addPointInteger,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_PARTICIPACION } from './texts';

const VALUE_COLUMN = 'valor';
const DISTRITO_COLUMN = 'distrito_localizacion';
const SIZE_VALUE = 12;

/**
 * Opciones por defecto para todos los graficos stacked bar
 * Atributos que son necesarios rellenar : title{text},data,series.
 */
const options = {
  grid: {
    left: '40',
    right: '0',
    bottom: '70',
    top: '40',
  },
  title: {
    text: '',
    left: 'center',
    top: 'top',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
        title: TEXTS_PARTICIPACION[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      }
    },
    emphasis: { 
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },        
    },
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    formatter: (params) => formatTooltip(params, true, false, TEXTS_PARTICIPACION[DEFAULT_LANGUAGE].get('issues'), ),
  },
  xAxis: [
    {
      type: 'category',
      boundaryGap: ['20%', '20%'],
      gridIndex: 0 ,
      offset: 0,
      axisTick: {show: false,},
      axisLine: {
        lineStyle: {
          color: '#ccc',
        },
      },
      axisLabel: {
        show: true,
        interval: 0,
        rotate: 50,
        padding: 0,
        margin: 5,
        verticalAlign: 'top',
        fontSize: 11,
        lineHeight: 12,
        fontFamily: 'Montserrat',
        fontWeight: 400,
        color: '#333',
        formatter: (value) => formatName(value, SIZE_VALUE),
      },
      data: [],
    },
  ],
  yAxis: {
    type: 'value',
    gridIndex: 0 ,
    offset: 0 ,
    axisLabel: {
      textStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
        fontWeight: 400,
      },
      formatter: (value) => addPointInteger(value),
    },
  },
  series: [
    {
      type: 'bar',
      color: COLORS['naranjaPalido'],
      barWidth: 28,
      data: [],
    },
  ],
};

export function echartStackedBarDistrito(vis, datos) {
  const idChart = 'chartIncidenciasDistritos';
  const idNoData = 'no_dataIncidenciasDistritos';
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    options.title.text = TEXTS_PARTICIPACION[DEFAULT_LANGUAGE].get(
      'chartNameIncidenciasDistrito',
    );
    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      options.xAxis[0].data.push(`${row[DISTRITO_COLUMN].substring(3)}`);
      options.series[0].data.push(row[VALUE_COLUMN]);
    });

    initChart(idChart, options);
  }
}
