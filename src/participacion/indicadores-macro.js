import { printDataInHTMLElements, resultsetWithFields } from 'vlcishared';

export function indicadoresMacro(data) {
  const dataConfig = {
    fechaReciente: {
      value: '#ultimaFecha',
    },
  };
  const dataWithFields = resultsetWithFields(data);
  printDataInHTMLElements(dataWithFields, dataConfig);
}
