import { TEXTS_PARTICIPACION } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

export async function isUrlReachable(url) {
  try {
    const response = await fetch(url, { method: 'HEAD' });
    return response.ok; // returns true if status is 200-299
  } catch (error) {
    return false; // if an error occurs (e.g., network error), return false
  }
}

export function getMapIframe(mapUrl) {
  const iframe = document.createElement('iframe');
  iframe.width = '100%';
  iframe.height = '100%';
  iframe.src = mapUrl;
  return iframe;
}

export function getErrorMessage() {
  const paragraph = document.createElement('p');
  paragraph.style.fontWeight = 'bold';
  paragraph.style.color = 'black';
  paragraph.textContent =
    TEXTS_PARTICIPACION[DEFAULT_LANGUAGE].get('notAvailableOutVPN');
  return paragraph;
}
