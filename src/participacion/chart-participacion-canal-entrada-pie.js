import {
  printNoDataChart,
  printDataChart,
  formatTooltipPie,
  initChart,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_PARTICIPACION } from './texts';

const defaultOptions = {
  color: [
    COLORS['amarilloPalido'],
    COLORS['tealPalido'],
    COLORS['naranjaPalido'],
    COLORS['cranberryPalido'],
    COLORS['azulPalido'],
    COLORS['marinoPalido'],
    COLORS['naranja'],
    COLORS['teal'],
    COLORS['amarillo'],
    COLORS['azulCeruleo'],
    COLORS['cranberry'],
    COLORS['marino'],
  ],
  grid: {
    left: '0',
    right: '0',
    bottom: '50',
    top: '40',
    containLabel: true,
  },
  title: {
    text: '',
    left: 'center',
    top: 'top',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
  },
  legend: {
    selectedMode: false,
    orient: 'horizontal',
    padding: 0,
    margin: 0,
    itemGap: 10,
    bottom: '0',
    left: 'center',
    align: 'auto',
    itemHeight: '11',
    width: 'auto' ,
    textStyle: {
      fontSize: 11,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
  }, 
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
        title: TEXTS_PARTICIPACION[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      }
    },
    emphasis: { 
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },        
    },
  },
  tooltip: {
    trigger: 'item',
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    formatter: (params) => formatTooltipPie(params, true),
  },
  series: [
    {
      type: 'pie',
      center: ['50%', '50%'],
      radius: '50%',
      data: [],
      startAngle: 140,
      label: {
        show: true,
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 11,
        },
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  ], 
  media: [
    {
      query: {
          minWidth: 461,
      },
      option: {
        legend: {
          itemGap: 8,
          itemHeight: '11',
          textStyle: {
            fontSize: 11,
          },
        }, 
        series: {
          radius: '50%',
          label: {
            textStyle: {
              fontSize: 12,
            },
          },
        },
      },
    },
    {
        query: {
            minWidth: 675,
        },
        option: {
          legend: {
            itemGap: 50,
            itemHeight: '14',
            textStyle: {
              fontSize: 14,
            },
          }, 
          series: {
            label: {
              textStyle: {
                fontSize: 14,
              },
            },
          },
        },
    },
    {
        query: {
            minWidth: 675,
            minHeight: 395,
        },
        option: {
          series: {
            radius: '70%',
          },
        },
    },
  ],
};

export function echartPieCanalEntrada(vis, datos) {
  const misDatosArray = datos.resultset;
  const marginLeftLegend = datos.length > 5 ? '1%' : '3%';

  const idChart = 'chartIncidenciasCanal';
  const idNoData = 'no_dataIncidenciasCanal';
  if (misDatosArray.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);
  }

  const data = misDatosArray.map(([value, name]) => ({
    value,
    name,
  }));

  const options = { ...defaultOptions };
  options.title.text = TEXTS_PARTICIPACION[DEFAULT_LANGUAGE].get(
    'chartNameIncidenciasCanal',
  );
  options.legend.left = marginLeftLegend;
  options.series[0].data = data;

  initChart(idChart, options);
}
