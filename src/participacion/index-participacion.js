import { isUrlReachable, getMapIframe, getErrorMessage } from './map-quejas';
import { sigvalUrl } from '../constants';

export { getSelectCommonOptions } from 'vlcishared';
export { echartPieCanalEntrada } from './chart-participacion-canal-entrada-pie';
export { echartPieTipologia } from './chart-participacion-tipologia-pie';
export { echartStackedBarTema } from './chart-participacion-stacked-tema-bar';
export { echartStackedBarDistrito } from './chart-participacion-stacked-distrito-bar';
export { indicadoresMacro } from './indicadores-macro';

const mapId = '8b25e839ccf547f3998f07a95fe61bcf';
const scale = '100000';
const breakPoint = '300';
const mapUrl = `${sigvalUrl}?id=${mapId}&amp;scale=${scale}&amp;mobileBreakPoint=${breakPoint};`;

window.addEventListener('load', () => {
  isUrlReachable(mapUrl).then((isReachable) => {
    const mapContainer = document.querySelector('#map-quejas');
    if (isReachable) {
      mapContainer.append(getMapIframe(mapUrl));
    } else {
      mapContainer.append(getErrorMessage());
    }
  });
});
