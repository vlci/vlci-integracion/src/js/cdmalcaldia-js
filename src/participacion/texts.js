export const TEXTS_PARTICIPACION = {
  es_ES: new Map([
    ['chartNameIncidenciasCanal', 'CANAL DE ENTRADA'],
    ['chartNameIncidenciasTipo', 'TIPOLOGÍA'],
    ['chartNameIncidenciasTema', 'INCIDENCIAS POR TEMA'],
    ['chartNameIncidenciasDistrito', 'INCIDENCIAS POR DISTRITO'],
    ['issues', 'incidencias'],
    ['reset', 'Restablecer'],
    ['download', 'Descargar'],
    ['notAvailableOutVPN', 'No disponible fuera de la red municipal'],
  ]),
  ca_ES: new Map([
    ['chartNameIncidenciasCanal', "CANAL D'ENTRADA"],
    ['chartNameIncidenciasTipo', 'TIPOLOGIA'],
    ['chartNameIncidenciasTema', 'INCIDÈNCIES PER TEMA'],
    ['chartNameIncidenciasDistrito', 'INCIDÈNCIES PER DISTRICTE'],
    ['issues', 'incidències'],
    ['reset', 'Restablir'],
    ['download', 'Desar'],
    ['notAvailableOutVPN', 'No disponible anara de la xarxa municipa'],
  ]),
};
