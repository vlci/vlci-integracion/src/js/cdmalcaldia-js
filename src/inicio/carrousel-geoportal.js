import { TEXTS_INICIO } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

export function carrouselLoad() {
  const carouselMapas = document.getElementById('carouselMapas');

  const iframeSrc = [
    {
      id: 'iframe-estado-trafico',
      src: 'https://geoportal.valencia.es/portal/apps/webappviewer/index.html?id=5ea0ac61fe02477383083fcf84ce81f3&mobileBreakPoint=300;',
    },
    {
      id: 'iframe-incidencias-trafico',
      src: 'https://geoportal.valencia.es/portal/apps/webappviewer/index.html?id=7d266fa21678425f9f796695d4b78ec8&scale=15000&mobileBreakPoint=300;',
    },
  ];
  // Cargar el iframe Inicial
  const iframeFirstElement = document.getElementById(iframeSrc[0].id);
  iframeFirstElement.src = iframeSrc[0].src;

  carouselMapas.addEventListener('slide.bs.carousel', (event) => {
    // Cargar el iframe que se esta visualizando
    const iframeElementSelected = document.getElementById(
      iframeSrc[event.to].id,
    );
    const iframeTabName = TEXTS_INICIO[DEFAULT_LANGUAGE].get(
      iframeElementSelected.id,
    );

    iframeElementSelected.src = iframeSrc[event.to].src;
    document.querySelector('#tituloTab > h2').textContent = iframeTabName;

    // Vaciar el resto de iframes
    iframeSrc.forEach((element) => {
      if (element !== iframeSrc[event.to]) {
        document.getElementById(element.id).src = '';
      }
    });
  });
}
