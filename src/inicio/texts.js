export const TEXTS_INICIO = {
  es_ES: new Map([
    ['iframe-estado-trafico', 'Estado del tráfico'],
    ['iframe-incidencias-trafico', 'Ocupación en vía pública'],
  ]),
  ca_ES: new Map([
    ['iframe-estado-trafico', 'Estat del trànsit'],
    ['iframe-incidencias-trafico', 'Ocupació en via pública'],
  ]),
};
