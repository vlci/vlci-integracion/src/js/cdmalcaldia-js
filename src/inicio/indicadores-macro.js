import {
  printDataInHTMLElements,
  formatMonthAnyo,
  resultsetWithFields,
  addPointInteger,
} from 'vlcishared';

export function indicadoresMacro(data) {
  const dataConfig = {
    NumeroQuejasSugerenciasMes: {
      value: {
        element: '#indQuejasVal',
        format: addPointInteger,
      },
      variacion: {
        element: '#indQuejasVar',
        format: (value) =>
          `${value
            .toLocaleString('es-ES', { signDisplay: 'always' })
            .replace('.', ',')}`,
      },
      calculationperiod: {
        element: '#indQuejasMes',
        format: formatMonthAnyo,
      },
    },
    RendimientoAdministrativo: {
      value: {
        element: '#indRendAdVal',
        format: (value) => `${value.toLocaleString('es-ES').replace('.', ',')}`,
      },
      variacion: {
        element: '#indRendAdVar',
        format: (value) =>
          `${value
            .toLocaleString('es-ES', { signDisplay: 'always' })
            .replace('.', ',')}`,
      },
      calculationperiod: {
        element: '#indRendAdMes',
        format: formatMonthAnyo,
      },
    },
    EjecucionPresupuesto: {
      value: {
        element: '#indEjPresVal',
        format: (value) => `${value.toLocaleString('es-ES').replace('.', ',')}`,
      },

      variacion: {
        element: '#indEjPresVar',
        format: (value) =>
          value != undefined ?
          `${value
            .toLocaleString('es-ES', { signDisplay: 'always' })
            .replace('.', ',')}`
            : '-',
      },
      calculationperiod: {
        element: '#indEjPresMes',
        format: formatMonthAnyo,
      },
    },
  };

  const dataWithFields = resultsetWithFields(data);
  printDataInHTMLElements(dataWithFields, dataConfig);
}
