export function initValueDesfaultDates(data, filterParam) {
  Dashboards.fireChange(filterParam, data.resultset[0][0]);
}
export function selectValueDefaultInFilter(FilterComponent, parameterOrigin) {
  const paramDefault = Dashboards.getParameterValue(parameterOrigin);
  const paramFilter = FilterComponent.parameter;
  FilterComponent.dashboard.setParameter(paramFilter, [paramDefault]);
}
