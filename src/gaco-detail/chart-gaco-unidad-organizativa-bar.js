import {
  resultsetWithFields,
  formatShortMonthShortYear,
  printNoDataChart,
  printDataChart,
  formatTooltip,
  initChart,
  updateStackedBarOptions,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_GACO } from './texts';

const DATE_COLUMN = 'fecha';
const CRED_DEFINITIVO_COLUMN = 'cred_definitivo';
const IMP_AUTORIZADO_COLUMN = 'imp_autorizado';
const IMP_OBLIGACION_COLUMN = 'obl_reconocidas';
const PAGOS_LIQUIDOS_COLUMN = 'pag_liquidos';
const CRED_AUTOR_DIV_CRED_DEF_COLUMN = 'cred_auto_div_cred_def';
const IMP_COMPROM_DIV_CRED_DEF_COLUMN = 'imp_compro_div_cred_def';
const PAG_LIQUIDOS_DIV_CRED_DEF_COLUMN = 'pagos_liq_div_cred_def';

const options = {
  grid: {
    left: '0',
    right: '0',
    bottom: '80',
    top: '50',
  },
  title: {
    text: TEXTS_GACO[DEFAULT_LANGUAGE].get('chartNameUnidadOrganizativa'),
  },
  legend: {
    data: [
      TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definitivo'),
      TEXTS_GACO[DEFAULT_LANGUAGE].get('imp_autorizado'),
      TEXTS_GACO[DEFAULT_LANGUAGE].get('obl_reconocidas'),
      TEXTS_GACO[DEFAULT_LANGUAGE].get('pag_liquidos'),
      TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_auto_div_cred_def'),
      TEXTS_GACO[DEFAULT_LANGUAGE].get('imp_compro_div_cred_def'),
      TEXTS_GACO[DEFAULT_LANGUAGE].get('pagos_liq_div_cred_def'),
    ],
  },
  toolbox: {
    feature: {
      restore: {
        title: TEXTS_GACO[DEFAULT_LANGUAGE].get('reset'),
      },
      saveAsImage: {
        title: TEXTS_GACO[DEFAULT_LANGUAGE].get('download'),
      },
    },
  },
  tooltip: {
    confine: true,
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    formatter: (params) => {
      params[0].name = formatShortMonthShortYear(params[0].name);
      return formatTooltip(params, false, true);
    },
  },
  xAxis: [
    {
      type: 'category',
      axisLabel: {
        margin: 5,
        fontSize: 9,
        fontFamily: 'Montserrat',
        fontWeight: 400,
        color: '#333',
        formatter: (value) => formatShortMonthShortYear(value),
        textStyle: {
          lineHeight: 13,
        },
      },
    },
  ],
  series: [
    {
      name: TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definitivo'),
      yAxisIndex: 1,
      type: 'bar',
      data: [],
      color: COLORS.naranjaPalido,
    },
    {
      name: TEXTS_GACO[DEFAULT_LANGUAGE].get('imp_autorizado'),
      yAxisIndex: 1,
      type: 'bar',
      data: [],
      color: COLORS.tealPalido,
    },
    {
      name: TEXTS_GACO[DEFAULT_LANGUAGE].get('obl_reconocidas'),
      yAxisIndex: 1,
      type: 'bar',
      data: [],
      color: COLORS.amarilloPalido,
    },
    {
      name: TEXTS_GACO[DEFAULT_LANGUAGE].get('pag_liquidos'),
      yAxisIndex: 1,
      type: 'bar',
      data: [],
      color: COLORS.cranberryPalido,
    },
    {
      name: TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_auto_div_cred_def'),
      type: 'line',
      data: [],
      color: COLORS.azulPalido,
      azulCeruleo: '#05668d',
    },
    {
      name: TEXTS_GACO[DEFAULT_LANGUAGE].get('imp_compro_div_cred_def'),
      type: 'line',
      data: [],
      color: COLORS.cranberry,
    },
    {
      name: TEXTS_GACO[DEFAULT_LANGUAGE].get('pagos_liq_div_cred_def'),
      type: 'line',
      data: [],
      color: COLORS.teal,
    },
  ],
  media: [
    {
      query: {
        minWidth: 830,
      },
      option: {
        grid: {
          bottom: '60',
        },
        xAxis: [
          {
            axisLabel: {
              textStyle: {
                fontSize: 10,
              },
            },
          },
        ],
        series: [
          {
            name: TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definitivo'),
            barWidth: 23,
          },
          {
            name: TEXTS_GACO[DEFAULT_LANGUAGE].get('imp_autorizado'),
            barWidth: 23,
          },
          {
            name: TEXTS_GACO[DEFAULT_LANGUAGE].get('obl_reconocidas'),
            barWidth: 23,
          },
          {
            name: TEXTS_GACO[DEFAULT_LANGUAGE].get('pag_liquidos'),
            barWidth: 23,
          },
        ],
      },
    },
  ],
};

export function echartStackedBarGacoUniOrganizativa(vis, datos) {
  const idChart = 'chartUnidadOrganizativa';
  const idNoData = 'no_dataUnidadOrganizativa';
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    let updatedOptions = {};
    updatedOptions = updateStackedBarOptions(options);

    // Reiniciar datos
    updatedOptions.xAxis[0].data = [];
    updatedOptions.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      updatedOptions.xAxis[0].data.push(`${row[DATE_COLUMN]}`);
      updatedOptions.series[0].data.push(row[CRED_DEFINITIVO_COLUMN]);
      updatedOptions.series[1].data.push(row[IMP_AUTORIZADO_COLUMN]);
      updatedOptions.series[2].data.push(row[IMP_OBLIGACION_COLUMN]);
      updatedOptions.series[3].data.push(row[PAGOS_LIQUIDOS_COLUMN]);
      updatedOptions.series[4].data.push(row[CRED_AUTOR_DIV_CRED_DEF_COLUMN]);
      updatedOptions.series[5].data.push(row[IMP_COMPROM_DIV_CRED_DEF_COLUMN]);
      updatedOptions.series[6].data.push(row[PAG_LIQUIDOS_DIV_CRED_DEF_COLUMN]);
    });

    initChart(idChart, updatedOptions);
  }
}
