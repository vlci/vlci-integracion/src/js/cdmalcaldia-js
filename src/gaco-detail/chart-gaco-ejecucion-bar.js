import {
  resultsetWithFields,
  formatShortMonthShortYear,
  printNoDataChart,
  printDataChart,
  formatTooltip,
  initChart,
  updateStackedBarOptions,
} from 'vlcishared';
import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_GACO } from './texts';

const DATE_COLUMN = 'fecha';
const CRED_DEFINITIVO_COLUMN = 'cred_definitivo';
const OBLI_RECON_DIV_CRED_DEF_COLUMN = 'obli_recon_div_cred_def';

const options = {
  title: {
    text: TEXTS_GACO[DEFAULT_LANGUAGE].get('chartNameGacoEjecucion'),
  },
  grid: {
    left: '0',
    right: '0',
    bottom: '30',
    top: '45',
  },
  legend: {
    data: [
      TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definitivo'),
      TEXTS_GACO[DEFAULT_LANGUAGE].get('Obl_reconoc_div_cred_definitivo'),
    ],
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      restore: {
        title: TEXTS_GACO[DEFAULT_LANGUAGE].get('reset'),
      },
    },
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    formatter: (params) => {
      params[0].name = formatShortMonthShortYear(params[0].name);
      return formatTooltip(params, false, true);
    },
  },
  xAxis: [
    {
      type: 'category',
      axisLabel: {
        margin: 5,
        formatter: (value) => formatShortMonthShortYear(value),
        textStyle: {
          lineHeight: 13,
          fontSize: 9,
          fontFamily: 'Montserrat',
          fontWeight: 400,
          color: '#333',
        },
      },
      data: [],
    },
  ],
  series: [
    {
      name: TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definitivo'),
      yAxisIndex: 1,
      type: 'bar',
      barWidth: 23,
      data: [],
      color: COLORS.naranjaPalido,
    },
    {
      name: TEXTS_GACO[DEFAULT_LANGUAGE].get('Obl_reconoc_div_cred_definitivo'),
      type: 'line',
      data: [],
      color: COLORS.cranberry,
    },
  ],
  media: [
    {
      query: {
        minWidth: 595,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              textStyle: {
                fontSize: 10,
              },
            },
          },
        ],
      },
    },
  ],
};

export function echartStackedBarGacoEjecucion(vis, datos) {
  const idChart = 'chartEvolucionEjecucion';
  const idNoData = 'no_dataEvolucionEjecucion';
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);
    let updatedOptions = {};
    updatedOptions = updateStackedBarOptions(options);

    // Reiniciar datos
    updatedOptions.xAxis[0].data = [];
    updatedOptions.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      updatedOptions.xAxis[0].data.push(`${row[DATE_COLUMN]}`);
      updatedOptions.series[0].data.push(row[CRED_DEFINITIVO_COLUMN]);
      updatedOptions.series[1].data.push(row[OBLI_RECON_DIV_CRED_DEF_COLUMN]);
    });

    initChart(idChart, updatedOptions);
  }
}
