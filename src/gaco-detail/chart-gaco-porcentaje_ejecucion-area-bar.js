import {
  printNoDataChart,
  printDataChart,
  formatTooltip,
  formatShortMonthShortYear,
  initStackedBarChart,
  addPointInteger,
  updateStackedBarOptions
} from 'vlcishared';

import { COLORS, COLORS_GENDER } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_GACO } from './texts';

export function echartStackedBarGacoArea(vis, datos) {
  const idChart = 'chartGacoArea';
  const idNoData = 'no_dataGacoArea';

  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    const misDatosArray = datos.resultset;

    const date = formatShortMonthShortYear(misDatosArray[0][0]);
    const options = {
      grid: {
        left: '0',
        right: '2%',
        bottom: '30',
        top: '45',
      },
      title: {
        text: `${TEXTS_GACO[DEFAULT_LANGUAGE].get(
          'chartNamePocertEjecArea',
        )} - ${date.toUpperCase()}`,
      },
      legend: {
        show: false,
      },
      toolbox: {
        feature: {
          restore: {
            title: TEXTS_GACO[DEFAULT_LANGUAGE].get('reset'),
          },
          saveAsImage: {
            title: TEXTS_GACO[DEFAULT_LANGUAGE].get('download'),
          }
        },
      },
      tooltip: {
        position: 'top',
        confine: true,
        formatter: (params) => {
          const param = [params];
          param[0].seriesName = formatShortMonthShortYear(param[0].seriesName);
          return formatTooltip(param, true, true, '%');
        },
      },
      series: [
        {
          color: COLORS.naranjaPalido,
          offset: ['0','-35'],
          label: {
              show: true,
              position: 'right',
              lineHeight: 15,
              textStyle: {
                  fontSize: '12',
                  barWidth: '12',
                  fontFamily: 'Montserrat',
                  color: '#000',
              },
              formatter: '{c0}%',
           }
        },
      ],
    };

    let updatedOptions = {};
    updatedOptions = updateStackedBarOptions(options);

    updatedOptions.yAxis = {
      axisTick: {show: false,},
    };

    updatedOptions.xAxis = {
      name: '',
      max: (values) => values.max + 8,
      splitLine: {
        lineStyle: {
          color: COLORS_GENDER.grisClaro,
        },
      },
    };

    const updateDataOptions = {
      label: {
        formatter: (params) => `${addPointInteger(Math.round(params.value))} %`,
      },
    };

    initStackedBarChart(
      idChart,
      misDatosArray,
      updatedOptions,
      updateDataOptions,
    );
  }
}
