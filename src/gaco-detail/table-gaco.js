import {
  initTable,
  traverseRows,
  convertToTableTreeView,
  addPointInteger,
  downloadCSVTable,
  downloadPDFTable,
} from 'vlcishared';

import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_GACO } from './texts';

const PARENT_COLUMN = 'area';
const CHILDREN_COLUMN = 'valor';

const metricas = [
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_inicial'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_modif'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definitivo'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_disponible'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_no_disponible'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_retenido'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('imp_autorizado'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('imp_comprometido'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('obl_reconocidas'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('pag_liquidos'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('reintegros'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definit_menos_cred_reten'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_reten_div_cred_defin'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definit_menos_cred_autor'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_auto_div_cred_def'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definit_menos_imp_comprom'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('imp_compro_div_cred_def'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definit_menos_obli_reconoc'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('cred_definit_menos_pagos_liquidos'),
  TEXTS_GACO[DEFAULT_LANGUAGE].get('pagos_liq_div_cred_def'),
];

// Propiedades de las columnas de la tabla
const columnasTabla = [
  {
    title: TEXTS_GACO[DEFAULT_LANGUAGE].get('area'),
    field: PARENT_COLUMN,
    headerSort: false,
    width: '60%',
    vertAlign: 'left',
  },
  {
    field: CHILDREN_COLUMN,
    width: '40%',
    vertAlign: 'center',
    hozAlign: 'center',
    headerSort: false,
    formatter: (cell) => {
      const data = cell.getRow().getData();

      // Comprueba si no es porcentaje
      if (data.valor !== undefined) {
        if (!data.area.includes('%')) {
          return `${addPointInteger(data.valor)} €`;
        }
        return `${data.valor} %`;
      }
      return '';
    },
  },
];

export function tableGACO(datos) {
  const idChart = 'tableGaco';
  const nombreFichero = 'Gastos.Resumen Económico';
  const elementeHTMLPDF = 'download-pdf';
  const elementeHTMLCSV = 'download-csv';

  // Datos que se mostrará en la tabla
  const dataOrigen = datos.resultset;

  const dataObj = convertToTableTreeView(
    dataOrigen,
    PARENT_COLUMN,
    metricas,
    CHILDREN_COLUMN,
  );

  const option = {
    height: '100%',
    layout: 'fitDataTable',
    dataTree: true,
    dataTreeStartExpanded: true,
    columns: columnasTabla,
    data: dataObj,
  };

  const table = initTable(idChart, option);

  // Descargar tabla en CSV
  downloadCSVTable(table, nombreFichero, elementeHTMLCSV);

  // Descargar tabla PDF
  downloadPDFTable(table, nombreFichero, elementeHTMLPDF);

  // Expandir todos los elementos de la tabla
  document.getElementById('expandAll').addEventListener('click', () => {
    traverseRows(table, 'expand');
  });

  // Expandir todos los elementos de la tabla
  document.getElementById('contractAll').addEventListener('click', () => {
    traverseRows(table, 'collapse');
  });
}
