import {
  printDataInHTMLElements,
  resultsetWithFields,
  formatShortMonthShortYear,
} from 'vlcishared';

export function indicadoresMacro(data) {
  const dataConfig = {
    ultimaFecha: {
      value: {
        element: '#ultimaFecha',
        format: formatShortMonthShortYear,
      },
    },
  };
  const dataWithFields = resultsetWithFields(data);
  printDataInHTMLElements(dataWithFields, dataConfig);
}
