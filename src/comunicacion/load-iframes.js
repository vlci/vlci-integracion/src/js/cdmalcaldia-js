import { loadIframeOnClickEvent } from 'vlcishared';

export function loadComunicacionFrames() {
  document
    .querySelector('#iframeWebMunicipal')
    .setAttribute(
      'src',
      'https://lookerstudio.google.com/embed/reporting/3f408d59-3f25-4033-8798-8a65a4449693/page/6WdoC',
    );

  loadIframeOnClickEvent(
    '#nav-webmunicipal-tab',
    '#iframeWebMunicipal',
    'https://lookerstudio.google.com/embed/reporting/3f408d59-3f25-4033-8798-8a65a4449693/page/6WdoC',
  );

  loadIframeOnClickEvent(
    '#nav-vminut-tab',
    '#iframeValenciaMinut',
    'https://lookerstudio.google.com/embed/reporting/14a41533-3cd8-4f75-b303-9a494c0e4a9d/page/6WdoC',
  );

  loadIframeOnClickEvent(
    '#nav-smartcity-tab',
    '#iframeSmartCity',
    'https://lookerstudio.google.com/embed/reporting/ff43c17f-23cb-4edb-923b-a7b061d829dc/page/6WdoC',
  );
}
