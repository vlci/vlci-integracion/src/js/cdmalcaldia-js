export const TEXTS_INGRESOS = {
  es_ES: new Map([
    ['chartNameHaciendaImpIndirectos', 'IMPUESTOS INDIRECTOS'],
    ['chartNameHaciendaIngresos', 'TASAS Y OTROS INGRESOS'],
    ['chartNameHaciendaImpDirectos', 'IMPUESTOS DIRECTOS'],
    ['chartNameHaciendaTransfCorrientes', 'TRANSFERENCIAS CORRIENTES'],
    ['prev_definitiva', 'Previsión Definitiva (€)'],
    ['derechos_reconoc_netos', 'Derechos Reconocidos Netos (€)'],
    ['real_vs_presupuestados', 'Ejecución mensual (%)'],
    ['reset', 'Restablecer'],
    ['download', 'Descargar'],
  ]),
  ca_ES: new Map([
    ['chartNameHaciendaImpIndirectos', 'IMPOSTOS INDIRECTES'],
    ['chartNameHaciendaIngresos', 'TAXES I ALTRES INGRESSOS'],
    ['chartNameHaciendaImpDirectos', 'IMPOSTOS DIRECTES'],
    ['chartNameHaciendaTransfCorrientes', 'TRANSFERÈNCIES CORRENTS'],
    ['prev_definitiva', 'Previsió Definitiva (€)'],
    ['derechos_reconoc_netos', 'Drets Reconeguts Nets (€)'],
    ['real_vs_presupuestados', 'Execució mensual (%)'],
    ['reset', 'Restablir'],
    ['download', 'Desar'],
  ]),
};
