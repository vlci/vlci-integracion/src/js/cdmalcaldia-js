import {
  printDataInHTMLElements,
  formatMonthAnyo,
  resultsetWithFields,
  addPointInteger,
} from 'vlcishared';

export function indicadoresMacro(data) {
  const dataConfig = {
    impuestosIndirectos: {
      value: {
        element: '#impIndirectosKPI',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#CotaTemporalImpIndirectos',
        format: formatMonthAnyo,
      },
    },
    tasaseingresos: {
      value: {
        element: '#tasasIngresosKPI',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#CotaTemporalIngresos',
        format: formatMonthAnyo,
      },
    },
    impuestosDirectos: {
      value: {
        element: '#kpiCap1',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#fechakpiCap1',
        format: formatMonthAnyo,
      },
    },
    transfCorrientes: {
      value: {
        element: '#kpiCap4',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#fechakpiCap4',
        format: formatMonthAnyo,
      },
    },
  };
  const dataWithFields = resultsetWithFields(data);
  printDataInHTMLElements(dataWithFields, dataConfig);
}
