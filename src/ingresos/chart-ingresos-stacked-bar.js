import {
  resultsetWithFields,
  formatMonthAnyo,
  formatShortMonthYear,
  printNoDataChart,
  printDataChart,
  initChart,
  formatTooltip,
  updateStackedBarOptions,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_INGRESOS } from './texts';

const DATE = 'fecha';
const PREV_DEFINITIVA = 'prevision_definitiva';
const DERECHOS_RECONOCIDOS = 'derechos_reconocidos_netos';
const REAL_VS_PRESUPUESTADO = 'real_vs_presupuestado';

const options = {
  grid: {
    left: '0',
    right: '0',
    bottom: '60',
    top: '120',
  },
  toolbox: {
    feature: {
      restore: {
        title: TEXTS_INGRESOS[DEFAULT_LANGUAGE].get('reset'),
      },
      saveAsImage: {
        title: TEXTS_INGRESOS[DEFAULT_LANGUAGE].get('download'),
      },
    },
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    formatter: (params) => {
      // eslint-disable-next-line no-param-reassign
      params[0].name = formatMonthAnyo(params[0].name);
      return formatTooltip(params, false, true);
    },
  },
  xAxis: [
    {
      axisLabel: {
        margin: 10,
        formatter: (value) => formatShortMonthYear(value),
        textStyle: {
          fontSize: 9,
          fontFamily: 'Montserrat',
          fontWeight: 400,
          color: '#333',
        },
      },
    },
  ],
  series: [
    {
      name: TEXTS_INGRESOS[DEFAULT_LANGUAGE].get('prev_definitiva'),
      yAxisIndex: 1,
      type: 'bar',
      data: [],
      color: COLORS.naranjaPalido,
    },
    {
      name: TEXTS_INGRESOS[DEFAULT_LANGUAGE].get('derechos_reconoc_netos'),
      yAxisIndex: 1,
      type: 'bar',
      data: [],
      color: COLORS.tealPalido,
    },
    {
      name: TEXTS_INGRESOS[DEFAULT_LANGUAGE].get('real_vs_presupuestados'),
      type: 'line',
      data: [],
      color: COLORS.cranberry,
    },
  ],
  media: [
    {
      query: {
        minWidth: 535,
      },
      option: {
        grid: {
          bottom: '30',
          top: '100',
        },
      },
    },
    {
      query: {
        minWidth: 595,
      },
      option: {
        grid: {
          bottom: '30',
          top: '100',
        },
        xAxis: [
          {
            axisLabel: {
              textStyle: {
                fontSize: 10,
              },
            },
          },
        ],
      },
    },
  ],
};

function echartStackedBarIngresos(idChart, idNoData, datos, chartName) {
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    let updatedOptions = {};
    updatedOptions = updateStackedBarOptions(options);

    // Reiniciar datos
    updatedOptions.xAxis[0].data = [];
    updatedOptions.series.forEach((serie, index) => {
      updatedOptions.series[index].data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      updatedOptions.xAxis[0].data.push(`${row[DATE]}`);
      updatedOptions.series[0].data.push(row[PREV_DEFINITIVA]);
      updatedOptions.series[1].data.push(row[DERECHOS_RECONOCIDOS]);
      updatedOptions.series[2].data.push(row[REAL_VS_PRESUPUESTADO]);
    });

    updatedOptions.title.text = TEXTS_INGRESOS[DEFAULT_LANGUAGE].get(chartName);
    initChart(idChart, updatedOptions);
  }
}

export function echartStackedBarIngresosImpIndirectos(vis, datos) {
  const idChart = 'chartIndirectos';
  const idNoData = 'no_dataIndirectos';
  const chartName = 'chartNameHaciendaImpIndirectos';

  echartStackedBarIngresos(idChart, idNoData, datos, chartName);
}

export function echartStackedBarIngresosTasas(vis, datos) {
  const idChart = 'chartTasasIngresos';
  const idNoData = 'no_datachartTasasIngresos';
  const chartName = 'chartNameHaciendaIngresos';

  echartStackedBarIngresos(idChart, idNoData, datos, chartName);
}

export function echartStackedBarIngresosImpDirectos(vis, datos) {
  const idChart = 'chartCap1';
  const idNoData = 'no_dataCap1';
  const chartName = 'chartNameHaciendaImpDirectos';

  echartStackedBarIngresos(idChart, idNoData, datos, chartName);
}

export function echartStackedBarIngresosTransfCorrientes(vis, datos) {
  const idChart = 'chartCap4';
  const idNoData = 'no_dataCap4';
  const chartName = 'chartNameHaciendaTransfCorrientes';

  echartStackedBarIngresos(idChart, idNoData, datos, chartName);
}
