import { addPointInteger } from 'vlcishared';
import { COLORS } from '../colors';

export const defaultOptions = {
  grid: { 
    top: '40%',
    bottom: '0',
    left: '3%',
    right: '3%',
  },
  title: {
    text: '',
    margin:  '0',
    padding: 0,
    top: '3%',
    left: '50%',
    textAlign: 'center',
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 15,
      fontWeight: 600,
      color: '#666',
      width: '100%',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '8%',
    feature: {
      restore: {
        title: 'Restablecer',
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        }, 
      },
      saveAsImage: {
        title: 'Guardar',
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      }
    },
    emphasis: { 
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },        
    },
  },
  series: {
    type: 'gauge',
    radius: '100%',
    center: ['50%', '75%'],
    startAngle: -180,
    endAngle: 0,
    splitNumber: 2,
    max: undefined,
    itemStyle: {
      color: '#eee',
    },
    axisLine: {
      lineStyle: {
        width: 30,
        color:  [
          [1, '#eee']
        ],
      },
    },
    progress: {
      show: true,
      width: 30,
      itemStyle: {
        color: COLORS['naranjaPalido'],
      },
    },
    pointer: {
      show: false,
    },
    axisTick: {
      distance: 5,
      splitNumber: 10,
      lineStyle: {
        width: 1,
        color: '#999',
      },
    },
    splitLine: {
      distance: 5,
      length: 14,
      lineStyle: {
        width: 3,
        color: '#999',
      },
    },
    axisLabel: {
      fontFamily: 'Montserrat',
      color: '#666',
      fontSize: 11,
      fontWeight: 500,
      show: true,
      distance: 35,
      formatter: (value) => addPointInteger(Math.round(value)),
    },
    anchor: {
      show: false,
    },
    title: {
      show: false,
    },
    detail: {
      valueAnimation: true,
      width: '180%',
      borderRadius: 0,
      offsetCenter: [0, '20%'],
      formatter: undefined,
      fontSize: 17,
      lineHeight: 17,
      fontWeight: '600',
      fontFamily: 'Montserrat',
      color: '#666',
    },
    data: undefined,
  },  
  media: [
    {
        query: {
            minWidth: 288,
        },
        option: {
            series: [
              {
                center: ['50%', '70%'],
                detail: {
                  offsetCenter: [0, '25%'],
                  fontSize: 15,
                  lineHeight: 18,
                  padding: 0,
                },
              },
            ],
        },
    },
  ],
};
