import {
  initTable,
  formatNameHeaderColumn,
  addPointInteger,
  formatShortMonthYear,
  downloadCSVTable,
} from 'vlcishared';

import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_PMP_DETAIL } from './texts';

const FECHA_COLUMN = 'fecha_estado';
const COD_ORG_COLUMN = 'codigo_organico';
const DESCRIPCION_COLUMN = 'descripcion';
const PMO_PAGADAS_COLUMN = 'pmo_pagadas';
const PMO_PENDIENTES_COLUMN = 'pmo_pendientes';
const PMP_DIAS_COLUMN = 'pmp';
const PMO_IMP_PAGADO_COLUMN = 'imp_pagado';
const PMO_IMP_PEND_COLUMN = 'imp_pendiente';

// Propiedades de las columnas de la tabla
const columnasTabla = [
  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('fecha_estado'),
    field: FECHA_COLUMN,
    width: '8%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 7);
    },
    formatter(cell) {
      return formatShortMonthYear(cell.getValue());
    },
  },
  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('cod_organico'),
    field: COD_ORG_COLUMN,
    width: '8%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 7);
    },
  },
  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('desc_cod_organico'),
    field: DESCRIPCION_COLUMN,
    sorter: 'string',
    width: '24%',
  },

  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('pmo_pagadas'),
    field: PMO_PAGADAS_COLUMN,
    width: '12%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 25);
    },
    formatter: function formatDecimal(cell) {
      return addPointInteger(cell.getValue());
    },
  },
  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('pmo_pendientes'),
    field: PMO_PENDIENTES_COLUMN,
    width: '12%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 25);
    },
    formatter: function formatDecimal(cell) {
      return addPointInteger(cell.getValue());
    },
  },
  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('pmp'),
    field: PMP_DIAS_COLUMN,
    width: '12%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 21);
    },
    formatter: function formatDecimal(cell) {
      return addPointInteger(cell.getValue());
    },
  },

  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('imp_pendiente'),
    field: PMO_IMP_PEND_COLUMN,
    width: '12%',
    hozAlign: 'right',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 14);
    },
    formatter: 'money',
    formatterParams: {
      decimal: ',',
      thousand: '.',
      symbol: '€',
      symbolAfter: 'p',
      negativeSign: true,
      precision: false,
    },
  },
  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('imp_pagado'),
    field: PMO_IMP_PAGADO_COLUMN,
    width: '12%',
    hozAlign: 'right',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 14);
    },
    formatter: 'money',
    formatterParams: {
      decimal: ',',
      thousand: '.',
      symbol: '€',
      symbolAfter: 'p',
      negativeSign: true,
      precision: false,
    },
  },
];

export function tablePMPDetail(vis, datos) {
  const idChart = 'tablePMP';

  const nombreFichero = 'PMP.Detalle Pago Medio a Proveedores';
  const elementeHTMLCSV = 'download-csv';

  // Datos que se mostrará en la tabla

  const dataObj = datos.resultset.map((item) => ({
    [FECHA_COLUMN]: item[0],
    [COD_ORG_COLUMN]: item[1],
    [DESCRIPCION_COLUMN]: item[2],
    [PMO_PAGADAS_COLUMN]: item[3],
    [PMO_PENDIENTES_COLUMN]: item[4],
    [PMP_DIAS_COLUMN]: item[5],
    [PMO_IMP_PAGADO_COLUMN]: item[6],
    [PMO_IMP_PEND_COLUMN]: item[7],
  }));

  const option = {
    height: '100%',
    scrollable: true,
    layout: 'fitColumns',
    columns: columnasTabla,
    data: dataObj,
  };

  const table = initTable(idChart, option);

  // Descargar tabla en CSV
  downloadCSVTable(table, nombreFichero, elementeHTMLCSV);
}
