import {
  resultsetWithFields,
  printNoDataChart,
  printDataChart,
  initChart,
  addPointInteger,
  formatShortMonthYear,
  formatMonthAnyo,
  formatTooltip,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_PMP_DETAIL } from './texts';

const FECHA_COLUMN = 'fecha_estado';
const SUMA_PMP = 'pmp';

const dateWithMonthLength = 6;

/**
 * Modifica el valor de los parámetros del datazoom
 * @param {String} inicio Inicial del datazoom
 * @param {String} fin Final del datazoom
 */
function setValueParameterDataZoom(inicio, fin) {
  Dashboards.setParameter('filterParamFechaInicioEnt', inicio);
  Dashboards.setParameter('filterParamFechaFinEnt', fin);
  Dashboards.fireChange('filterParamFechaInicioEnt');
  Dashboards.fireChange('filterParamFechaFinEnt');
}

/**
 * Opciones por defecto para todos los graficos stacked bar
 * Atributos que son necesarios rellenar : title{text},data,series.
 */
const options = {
  title: {
    text: '',
    margin: '0',
    padding: 0,
    top: '3%',
    left: '50%',
    textAlign: 'center',
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 15,
      fontWeight: 600,
      color: '#666',
      width: '100%',
    },
  },
  grid: {
    left: '0',
    right: '10',
    bottom: '50',
    top: '50',
    containLabel: true,
  },
  legend: {
    show: false,
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
        title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
    },
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
  },
  tooltip: {
    trigger: 'axis',
    confine: true,
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    formatter: (params) => {
      if (params[0].name.length === dateWithMonthLength) {
        // eslint-disable-next-line no-param-reassign
        params[0].name = formatMonthAnyo(params[0].name);
      }
      return formatTooltip(params, false, true);
    },
  },
  xAxis: [
    {
      type: 'category',
      axisLine: {
        lineStyle: {
          color: '#ccc',
        },
      },
      axisLabel: {
        show: true,
        interval: 0,
        margin: 10,
        textStyle: {
          fontSize: 11,
          fontFamily: 'Montserrat',
          fontWeight: 400,
          color: '#333,',
        },
        formatter: (value) => {
          if (value.length === dateWithMonthLength) {
            return formatShortMonthYear(value);
          }
          return value;
        },
      },
      data: [],
    },
  ],
  yAxis: [
    {
      type: 'value',
      alignTicks: true,
      position: 'right',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => `${addPointInteger(value)} €`,
      },
    },
    {
      type: 'value',
      position: 'left',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => `${addPointInteger(value)} días`,
      },
    },
  ],
  series: [
    {
      name: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('suma_entidad'),
      yAxisIndex: 1,
      type: 'bar',
      barMaxWidth: 90,
      data: [],
      color: COLORS.naranjaPalido,
    },
  ],
  dataZoom: {
    type: 'slider',
    filterMode: 'filter',
    show: true,
    bottom: '0',
    borderColor: '#eee',
    fillerColor: 'rgba(0,0,0,.05)',
    textStyle: {
      fontSize: 10,
      fontFamily: 'Montserrat',
      color: '#333',
      textBorderColor: '#fff',
      textBorderWidth: 3,
      fontWeight: '600',
    },
    selectedDataBackground: {
      lineStyle: {
        color: '#333',
      },
      areaStyle: {
        color: COLORS.tealPalido,
      },
    },
    dataBackground: {
      backgroundColor: 'rgba(0,0,0,1)',
      lineStyle: {
        color: '#333',
      },
      areaStyle: {
        color: 'rgba(0,0,0,0.5)',
      },
    },
    handleStyle: {
      color: 'rgba(0,0,0,0.2)',
    },
    moveHandleStyle: {
      color: 'rgba(0,0,0,0.3)',
    },
    emphasis: {
      handleStyle: {
        color: COLORS.tealPalido,
      },
      moveHandleStyle: {
        color: COLORS.tealPalido,
      },
    },
  },
};

export async function echartPMPEntidad(datos) {
  const idChart = 'chartPMPGlobalEntidad';
  const idNoData = 'no_dataPMPGlobalEntidad';

  const titulo = `${datos.resultset[0][2]}`;

  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    // Reiniciar datos
    options.xAxis[0].data = [];
    options.series.forEach((serie) => {
      // eslint-disable-next-line no-param-reassign
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      options.xAxis[0].data.push(`${row[FECHA_COLUMN]}`);
      options.series[0].data.push(row[SUMA_PMP]);
    });

    options.title.text = `Evolución PMP Mensual para Entidad ${titulo}`;

    // Ajustar datazoom al cargar la página
    const dates = options.xAxis[0].data;
    const datazoomStart = (100 * (dates.length - 7)) / dates.length;
    options.dataZoom.data = dates;
    options.dataZoom.start = datazoomStart;

    // Dar valor a la fecha de inicio y fecha fin inicial
    const firstDateIni = dates.length >= 8 ? dates.at(-8) : dates[0];
    const [firstDate, lastDate] = [firstDateIni, dates.at(-1)];
    setValueParameterDataZoom(firstDate, lastDate);

    const chartInstance = await initChart(idChart, options);

    // Utilizar datazoom como filtro
    let dataZoomTimeout; // Variable para el temporizador
    chartInstance.on('datazoom', () => {
      // Reiniciar el temporizador en cada evento
      clearTimeout(dataZoomTimeout);

      dataZoomTimeout = setTimeout(() => {
        // Obtener los valores de start y end
        const dataZoomInfo = chartInstance.getOption().dataZoom[0];

        const startIndex = Math.round(
          (dataZoomInfo.start / 100) * (dates.length - 1),
        );
        const endIndex = Math.round(
          (dataZoomInfo.end / 100) * (dates.length - 1),
        );

        // Valores reales del rango
        const selectedXAxis = dates.slice(startIndex, endIndex + 1);

        // Modifica el parámetro de fecha Inicio y fecha Fin con el nuevo rango seleccionado en dataZoom
        const [first, last] = [selectedXAxis[0], selectedXAxis.at(-1)];
        setValueParameterDataZoom(first, last);
      }, 1000);
    });
  }
}
