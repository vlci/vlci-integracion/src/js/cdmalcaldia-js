export const TEXTS_PMP_DETAIL = {
  es_ES: new Map([
    ['fecha_estado', 'Fecha'],
    ['pmo_pagadas', 'Periodo Medio Operaciones Pagadas (días)'],
    ['pmo_pendientes', 'Periodo Medio Operaciones Pendientes (días)'],
    ['pmp', 'Periodo Medio de Pago a Proveedores (días)'],
    ['imp_pagado', 'Importe Total Pagado (€)'],
    ['imp_pendiente', 'Importe Total Pendiente de Pago (€)'],
    ['cod_organico', 'Código Orgánico'],
    ['desc_cod_organico', 'Servicio'],
    ['unidad_dias', 'días'],
    ['tiempo_importe_pendiente', 'Impacto PMP'],
    ['echartPMPGlobalName', 'Evolución PMP Mensual'],
    ['echartPMPGlobalUnidadMedida', 'Días'],
    ['echartPMPServicioName', 'Evolución PMP Mensual por Servicio'],
    ['echartPMPServicioUnidadMedida', 'Días'],
    ['reset', 'Restablecer'],
    ['download', 'Descargar'],
    ['suma_entidad', 'Suma Pago Medio a Proveedores (días)'],
    ['entidad', 'Entidad'],
  ]),
  ca_ES: new Map([
    ['fecha_estado', 'Fecha'],
    ['pmo_pagadas', 'Periode Midjà Operacions Pagades (días)'],
    ['pmo_pendientes', 'Periode Midjà Operacions Pendents de Pagament (días)'],
    ['pmp', 'Periode Midjà de Pagaments a Proveïdors (dies)'],
    ['imp_pagado', 'Import Total Pagat (€)'],
    ['imp_pendiente', 'Import Total Pendent de Pagament (€)'],
    ['cod_organico', 'Codi Orgànic'],
    ['desc_cod_organico', 'Servei'],
    ['unidad_dias', 'dies'],
    ['tiempo_importe_pendiente', 'Impacte PMP'],
    ['echartPMPGlobalName', 'Evolució PMP Mensual'],
    ['echartPMPGlobalUnidadMedida', 'Dies'],
    ['echartPMPServicioName', 'Evolució PMP Mensual per Servei'],
    ['echartPMPServicioUnidadMedida', 'Dies'],
    ['reset', 'Restablir'],
    ['download', 'Desar'],
    ['suma_entidad', 'Suma Pagament Midjà a Proveïdors (dies)'],
    ['entidad', 'Entidad'],
  ]),
};
