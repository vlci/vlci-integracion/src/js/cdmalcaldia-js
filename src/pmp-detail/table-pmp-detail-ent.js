import {
  initTable,
  formatNameHeaderColumn,
  formatShortMonthYear,
  downloadCSVTable,
} from 'vlcishared';

import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_PMP_DETAIL } from './texts';

const FECHA_COLUMN = 'fecha_estado';
const ENTIDAD = 'entidad';
const SUMA_PMP = 'suma_pmp';

// Propiedades de las columnas de la tabla
const columnasTabla = [
  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('entidad'),
    field: ENTIDAD,
    width: '60%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 7);
    },
  },
  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('fecha_estado'),
    field: FECHA_COLUMN,
    width: '20%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 7);
    },
    formatter(cell) {
      return formatShortMonthYear(cell.getValue());
    },
  },

  {
    title: TEXTS_PMP_DETAIL[DEFAULT_LANGUAGE].get('suma_entidad'),
    field: SUMA_PMP,
    sorter: 'string',
    width: '20%',
  },
];

export function tablePMPDetailEntidad(vis, datos) {
  const idChart = 'tablePMPEntidad';

  const nombreFichero = 'PMP.Detalle Entidad Pago Medio a Proveedores';
  const elementeHTMLCSV = 'download-csv-entidad';

  // Datos que se mostrará en la tabla

  const dataObj = datos.resultset.map((item) => ({
    [ENTIDAD]: item[0],
    [FECHA_COLUMN]: item[1],
    [SUMA_PMP]: item[2],
  }));

  const option = {
    height: '100%',
    scrollable: true,
    layout: 'fitColumns',
    columns: columnasTabla,
    data: dataObj,
  };

  const table = initTable(idChart, option);

  // Descargar tabla en CSV
  downloadCSVTable(table, nombreFichero, elementeHTMLCSV);
}
