import { initChart, resultsetWithFields } from 'vlcishared';
import { OPTIONS_EFECTIVOS_VINCULACION_LABORAL_BAR } from './options-charts';
let option = OPTIONS_EFECTIVOS_VINCULACION_LABORAL_BAR;

export function echartEfectivoVinculacionLaboralBar(vis, datos) {
  const resultSet = resultsetWithFields(datos);

  if (!resultSet || resultSet.length === 0) {
    console.error('resultSet is empty or undefined');
    return;
  }

  // Clear previous data
  option.xAxis.data = [];
  option.series[0].data = [];
  option.series[1].data = [];
  option.series[2].data = [];

  resultSet.forEach((row) => {
    option.xAxis.data.push(row.grupo);
    option.series[0].data.push(row.total);
    option.series[1].data.push(row.total_hombres);
    option.series[2].data.push(row.total_mujeres);
  });
  initChart('chartEfectivosVinculacion', option);
}
