import { initChart, resultsetWithFields } from 'vlcishared';

import { PERSONAL_AREA_PIE } from './options-charts';
let option = PERSONAL_AREA_PIE;

export function echartPersonalAreaPie(vis, datos) {
  option.series[0].data = [];

  const resultSet = resultsetWithFields(datos);
  resultSet.forEach((row) => {
    option.series[0].data.push({
      value: row.num_persones,
      name: row.descripcion_area,
    });
  });
  initChart('chartPersonalArea', option);
}
