import { initChart, resultsetWithFields } from 'vlcishared';
import { PIRAMIDE_LABORAL } from './options-charts';
let option = PIRAMIDE_LABORAL;

export function echartPiramideLaboral(vis, datos) {
  // Reiniciar datos
  option.series.data = [];
  const resultSet = resultsetWithFields(datos);
  resultSet.forEach((row) => {
    if (row.sexo === 'H') {
      option.series[0].data.push({
        value: row.count,
        name: row.grupo_edad,
      });
    } else {
      option.series[1].data.push({
        value: -row.count,
        name: row.grupo_edad,
      });
    }
  });
  initChart('chartPiramdeLaboral', option);
}
