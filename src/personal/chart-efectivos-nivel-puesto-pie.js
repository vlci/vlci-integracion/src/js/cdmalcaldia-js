import {
  initChart,
  printDataChart,
  printNoDataChart,
  resultsetWithFields,
} from 'vlcishared';

import { OPTIONS_EFECTIVOS_NIVEL_PUESTO_PIE } from './options-charts';

let options = OPTIONS_EFECTIVOS_NIVEL_PUESTO_PIE;

export function echartEfectivoNivelPuestoPie(vis, datos) {
  const idChart = 'chartEfectivosNivel';
  const idNoData = 'no_dataEfectivosNivel';
  // Reiniciar datos
  options.series[0].data = [];
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    const resultSet = resultsetWithFields(datos);

    resultSet.forEach((row) => {
      options.series[0].data.push({
        value: row.count_baremo,
        name: row.nivel,
      });
    });
    initChart('chartEfectivosNivel', options);
  }
}
