import {
  initChart,
  printDataChart,
  printNoDataChart,
  resultsetWithFields,
} from 'vlcishared';

import { OPTIONS_EVOLUCION_PLANTILLA_LINE } from './options-charts';
let options = OPTIONS_EVOLUCION_PLANTILLA_LINE;

export function echartEvolucionPlantillaLine(datos) {
  const idChart = 'chartEvolucionPlantilla';
  const idNoData = 'no_dataEvolucionPlantilla';

  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    // Reiniciar datos
    options.xAxis[0].data = [];
    options.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      options.xAxis[0].data.push(row.año);
      options.series[0].data.push(row.personas);
    });

    initChart(idChart, options);
  }
}
