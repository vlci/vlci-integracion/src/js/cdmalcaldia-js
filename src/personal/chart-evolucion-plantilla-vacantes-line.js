import {
  initChart,
  printDataChart,
  printNoDataChart,
  resultsetWithFields,
} from 'vlcishared';

import { PLANTILLA_VACANTES_LINE } from './options-charts';
let options = PLANTILLA_VACANTES_LINE;

export function echartEvolucionPlantillaVacantesLine(datos) {
  const idChart = 'chartEvolucionVacantes';
  const idNoData = 'no_dataEvolucionVacantes';

  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    // Reiniciar datos
    options.xAxis[0].data = [];
    options.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      options.xAxis[0].data.push(row.año);
      options.series[0].data.push(row.personas);
    });

    initChart(idChart, options);
  }
}
