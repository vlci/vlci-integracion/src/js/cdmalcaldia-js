import { addPointInteger } from 'vlcishared';
import { COLORS } from '../colors';
import { COLORS_GENDER } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_PERSONAL } from './texts';

export const OPTIONS_EFECTIVOS_NIVEL_PUESTO_PIE = {
  color: Object.values(COLORS),
  grid: {
    left: '0',
    right: '0',
    bottom: '0',
    top: '0',
    containLabel: true,
  },
  title: {
    text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('chartNameEfectivosPuestoPeq'),
    left: 'center',
    top: '5',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    orient: 'horizontal',
    bottom: '0',
    left: 'center',
    width: '100%',
    selectedMode: true,
    itemHeight: 8,
    itemWidth: 8,
    textStyle: {
      fontSize: '9',
      fontFamily: 'Montserrat',
      fontWeight: '400',
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
    feature: {
      saveAsImage: {
        title: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('download'),
      },
      textStyle: {
        fontSize: 6,
        fontFamily: 'Montserrat',
        fontWeight: 300,
      },
    },
  },
  tooltip: {
    trigger: 'item',
    confine: true,
    valueFormatter(value) {
      return (
        addPointInteger(value) +
        ' ' +
        TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('personas')
      );
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 11,
    },
  },
  series: [
    {
      data: [],
      type: 'pie',
      radius: '35%',
      center: ['50%', '55%'],
      label: {
        show: true,
        formatter(param) {
          return `${addPointInteger(param.percent)}%`;
        },
        textStyle: {
          fontFamily: 'Montserrat',
        },
      },
      labelLine: {
        length: 2,
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  ],
  media: [
    {
      query: {
        minWidth: 240,
      },
      option: {
        legend: {
          itemGap: 10,
          itemHeight: 10,
          itemWidth: 10,
          textStyle: {
            fontSize: 13,
          },
        },
        series: {
          label: {
            show: true,
          },
          radius: '40%',
        },
      },
    },
    {
      query: {
        minWidth: 350,
      },
      option: {
        legend: {
          itemHeight: 10,
          itemWidth: 10,
          textStyle: {
            fontSize: 10,
          },
        },
        series: {
          radius: '50%',
        },
      },
    },
    {
      query: {
        minWidth: 460,
      },
      option: {
        title: {
          top: '5',
          text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get(
            'chartNameEfectivosPuesto',
          ),
        },
        series: {
          center: ['50%', '50%'],
        },
      },
    },
  ],
};

export const OPTIONS_EVOLUCION_PLANTILLA_LINE = {
  grid: {
    left: '0',
    right: '15',
    bottom: '30',
    top: '70',
    containLabel: true,
  },
  title: {
    text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('titleEfectivosPuestoPeq'), //
    left: 'center',
    top: 'top',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    top: 'bottom',
    show: true,
    textStyle: {
      fontSize: '13',
      fontFamily: 'Montserrat',
      fontWeight: '400',
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
    feature: {
      saveAsImage: {
        title: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('download'),
      },
      textStyle: {
        fontSize: 6,
        fontFamily: 'Montserrat',
        fontWeight: 300,
      },
    },
  },
  tooltip: {
    valueFormatter(value) {
      return addPointInteger(value) + ' ' + TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('personas');
    },
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      fontWeight: 600,
    },
  },
  xAxis: [
    {
      type: 'category',
      boundaryGap: false,
      axisLabel: {
        show: true,
        interval: 'auto',
        margin: 10,
        textStyle: {
          fontSize: 12,
          fontFamily: 'Montserrat',
          fontWeight: '500',
        },
      },
      data: [],
    },
  ],
  yAxis: [
    {
      type: 'value',
      axisLabel: {
        textStyle: {
          fontSize: 11,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => addPointInteger(value),
      },
    },
  ],
  series: [
    {
      type: 'line',
      name: 'Plantilla',
      data: [],
      smooth: 'true',
      symbol: 'circle',
      symbolSize: '5',
      color: COLORS['naranjaPalido'],
      lineStyle: {
        width: '2',
      },
      itemStyle: {
        color: COLORS['naranjaPalido'],
      },
      smooth: 0.1,
    },
  ],
  media: [
    {
      query: {
        minWidth: 240,
      },
      option: {
        legend: {
          itemHeight: 10,
          itemWidth: 10,
          textStyle: {
            fontSize: '13',
          },
        },
      },
    },
    {
      query: {
        minWidth: 280,
      },
      option: {
        grid: {
          top: '50',
          right: '40',
        },
        title: {
          top: '0',
          text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('titleEfectivosPuesto'),
        },
        legend: {
          bottom: '0',
          itemGap: 20,
          padding: [10, 0],
          textStyle: {
            fontSize: '10',
          },
        },
      },
    },
    {
      query: {
        minWidth: 350,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              interval: '0',
            },
        },
        ],
      },
    },
  ],
};

export const PLANTILLA_VACANTES_LINE = {
  grid: {
    left: '0',
    right: '15',
    bottom: '30',
    top: '70',
    containLabel: true,
  },
  title: {
    text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('titleEfectivosPuestoVacantesPeq'),
    left: 'center',
    top: 'top',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    top: 'bottom',
    show: true,
    textStyle: {
      fontSize: '13',
      fontFamily: 'Montserrat',
      fontWeight: '400',
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
    feature: {
      saveAsImage: {
        title: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('download'),
      },
      textStyle: {
        fontSize: 6,
        fontFamily: 'Montserrat',
        fontWeight: 300,
      },
    },
  },
  tooltip: {
    valueFormatter(value) {
      return addPointInteger(value) + ' ' + TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('personas');
    },
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      fontWeight: 600,
    },
  },
  xAxis: [
    {
      type: 'category',
      boundaryGap: false,
      axisLabel: {
        show: true,
        interval: 'auto',
        margin: 10,
        textStyle: {
          fontSize: 12,
          fontFamily: 'Montserrat',
          fontWeight: '500',
        },
      },
      data: [],
    },
  ],
  yAxis: [
    {
      type: 'value',
      axisLabel: {
        textStyle: {
          fontSize: 11,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => addPointInteger(value),
      },
    },
  ],
  series: [
    {
      type: 'line',
      name: 'Plantilla',
      data: [],
      smooth: 'true',
      symbol: 'circle',
      symbolSize: '5',
      color: COLORS['naranjaPalido'],
      lineStyle: {
        width: '2',
      },
      itemStyle: {
        color: COLORS['naranjaPalido'],
      },
      smooth: 0.1,
    },
  ],
  media: [
    {
      query: {
        minWidth: 240,
      },
      option: {
        legend: {
          itemHeight: 10,
          itemWidth: 10,
          textStyle: {
            fontSize: '13',
          },
        },
      },
    },
    {
      query: {
        minWidth: 280,
      },
      option: {
        grid: {
          top: '50',
          right: '40',
        },
        title: {
          top: '0',
          text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('titleEfectivosPuestoVacantes'),
        },
        legend: {
          bottom: '0',
          itemGap: 20,
          padding: [10, 0],
          textStyle: {
            fontSize: '10',
          },
        },
      },
    },
    {
      query: {
        minWidth: 350,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              interval: '0',
            },
        },
        ],
      },
    },
  ],
};

export const PERSONAL_AREA_PIE = {
  color: Object.values(COLORS),
  grid: {
    left: '0',
    right: '0',
    bottom: '0',
    top: '50',
    containLabel: true,
  },
  title: {
    text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('chartNamePersonal'),
    left: 'center',
    top: 'top',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    orient: 'vertical',
    top: 'middle',
    left: 'left',
    itemHeight: '6',
    selectedMode: true,
    itemStyle: {
    },
    textStyle: {
      fontSize: 6,
      lineHeight: 6,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
        title: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
    },
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
  },
  tooltip: {
    trigger: 'item',
    confine: true,
    valueFormatter(value) {
      return `${addPointInteger(value)}` + ' ' + TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('personas');
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 11,
    },
  },
  series: [
    {
      type: 'pie',
      data: [],
      radius: '27%',
      center: ['80%', '50%'],
      label: {
        show: true,
        formatter(param) {
          return `${addPointInteger(param.percent)}%`;
        },
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 9,
        },
      },
      labelLine: {
        show: true,
        length: 1,
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  ],
  media: [
    {
        query: {
            minWidth: 510,
        },
        option: {
          legend: {
            itemHeight: 10,
            textStyle: {
              fontSize: 10,
            },
          },
            series:
              {
                radius: '36%',
                center: ['78%', '50%'],
                label: {
                  textStyle: {
                    fontSize: 11,
                  },
                },
              },
        },
    },
    {
        query: {
            minWidth: 550,
        },
        option: {
          legend: {
            itemHeight: 11,
            textStyle: {
              fontSize: 11,
            },
          },
          series:
            {
              radius: '38%',
              label: {
                textStyle: {
                  fontSize: 12,
                },
              },
            },
        },
    },
    {
        query: {
            minWidth: 600,
        },
        option: {
            series: {
              center: ['79%', '50%'],
              label: {
                textStyle: {
                  fontSize: 13,
                },
              },
            },
        },
    },
    {
        query: {
            minWidth: 640,
            maxHeight: 280,
        },
        option: {
          series: {
            radius: '50%',
            center: ['76%', '50%'],
          },
        },
    },
    {
        query: {
            minWidth: 720,
        },
        option: {
            series:
              {
                label: {
                  textStyle: {
                    fontSize: 13,
                  },
                },
              },
        },
    },
    {
        query: {
            minWidth: 880,
        },
        option: {
            series:
              {
                radius: '60%',
                center: ['73%', '50%'],
                labelLine: {
                  length: 8,
                },
              },
        },
    },
    {
        query: {
            minHeight: 266,
        },
        option: {
          legend: {
            top: '30',
            itemHeight: 10,
            textStyle: {
              fontSize: 10,
              lineHeight: 10,
            },
          },
        },
    },
    {
        query: {
            minWidth: 640,
            minHeight: 275,
        },
        option: {
          legend: {
            itemHeight: 12,
            textStyle: {
              fontSize: 12,
              lineHeight: 12,
            },
          },
        },
    },
    {
        query: {
            minHeight: 310,
            minWidth: 555,
        },
        option: {
            legend: {
              top: 'middle',
              itemHeight: 12,
              textStyle: {
                fontSize: 12,
                lineHeight: 12,
              },
            },
        },
    },
    {
        query: {
            maxWidth: 448,
            minHeight: 310,
        },
        option: {
            legend: {
              top: 'middle',
            },
        },
    },
  ],
};

export const PIRAMIDE_LABORAL  = {
  color: [COLORS_GENDER.azulMasculino, COLORS_GENDER.lilaFemenino],
  grid: {
    left: '50',
    right: '30',
    bottom: '30',
    top: '40',
    containLabel: true,
  },
  title: {
    text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('chartPiramideLaboral'),
    left: 'center',
    top: '0',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    data: ['Mujeres', 'Hombres'],
    orient: 'horizontal',
    bottom: '0',
    left: 'center',
    width: '100%',
    textStyle: {
      fontSize: 11,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
        title: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
    },
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
  },
  tooltip: {
    trigger: 'axis',
    confine: true,
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    valueFormatter: (value) => Math.abs(value),
  },
  xAxis: [
    {
      type: 'value',
      show: true,
      splitLine: {
        lineStyle: {
          type: 'dashed',
        },
      },
      axisLabel: {
        show: false,
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 8,
          fontWeight: 500,
          formatter: (value) => Math.abs(value),
        },
      },
    },
  ],
  yAxis: [
    {
      type: 'category',
      axisTick: { show: false },
      axisLine: {
        show: 'false',
        onZero: 'true',
        onZeroAxisIndex: '0',
        lineStyle: {
          color: '#999',
        },
      },
      axisLabel: {
        textStyle: {
          fontSize: 12,
          fontFamily: 'Montserrat',
          color: '#999',
          fontWeight: '500',
        },
      },
      data: [
        '- de 25',
        '26 a 30',
        '31 a 35',
        '36 a 40',
        '41 a 45',
        '46 a 50',
        '51 a 55',
        '56 a 60',
        '61 a 64',
        '+ de 65',
      ],
    },
  ],
  series: [
    {
      name: 'Hombres',
      type: 'bar',
      stack: 'cantidad',
      yAxisIndex: '0',
      offset: ['35', '0'],
      barWidth: '18',
      itemStyle: {
        borderWidth: '1',
        borderColor: '#fff',
        color: COLORS_GENDER.azulMasculinoPalido,
      },
      emphasis: {
        focus: 'series',
      },
      data: [],
      label: {
        show: true,
        position: 'insideLeft',
        textStyle: {
          fontSize: 12,
          fontFamily: 'Montserrat',
          color: '#000',
          fontWeight: '500',
        },
      },
    },
    {
      name: 'Mujeres',
      type: 'bar',
      stack: 'cantidad',
      offset: ['0', '-35'],
      barWidth: '18',
      itemStyle: {
        borderWidth: '1',
        borderColor: '#fff',
        color: COLORS_GENDER.lilaFemenino,
      },
      label: {
        show: true,
        position: 'insideRight',
        formatter(params) {
          const test = Math.abs(params.data.value);
          return test;
        },
        textStyle: {
          fontSize: '12',
          fontFamily: 'Montserrat',
          color: '#000',
          fontWeight: '500',
        },
      },
      emphasis: {
        focus: 'series',
      },
      data: [],
    },
  ],
  media: [
    {
      query: {
        minWidth: 510,
      },
      option: {
        series: [
          {
            barWidth: '20',
          },
          {
            barWidth: '20',
          },
        ],
      },
    },
    {
      query: {
        minWidth: 640,
        minHeight: 330,
      },
      option: {
        series: [
          {
            barWidth: '25',
            itemStyle: {
              borderWidth: '0',
            },
          },
          {
            barWidth: '25',
            itemStyle: {
              borderWidth: '0',
            },
          },
        ],
      },
    },
  ],
};

export const OPTIONS_EFECTIVOS_VINCULACION_LABORAL_BAR = {
  grid: {
    left: '0',
    right: '10',
    bottom: '30',
    top: '70',
    containLabel: true,
  },
  title: {
    text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('chartNameEfectivosPeq'),
    left: 'center',
    top: '5',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    orient: 'horizontal',
    bottom: '5',
    left: 'center',
    width: '100%',
    itemHeight: 10,
    itemWidth: 10,
    itemGap: 10,
    padding: [0, 0],
    textStyle: {
      fontSize: '11',
      fontFamily: 'Montserrat',
      fontWeight: '400',
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
    feature: {
      saveAsImage: {
        title: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('download'),
      },
      textStyle: {
        fontSize: 6,
        fontFamily: 'Montserrat',
        fontWeight: 300,
      },
    },
  },
  tooltip: {
    valueFormatter(value) {
      return addPointInteger(value) + ' ' + TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('personas');
    },
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      fontWeight: 600,
    },
  },
  xAxis: {
    type: 'category',
    axisLabel: {
      show: true,
      interval: 0,
      margin: 10,
      textStyle: {
        fontSize: 12,
        fontFamily: 'Montserrat',
        fontWeight: '500',
      },
    },
    data: [],
  },
  yAxis: {
    type: 'value',
    alignTicks: true,
    axisLabel: {
      textStyle: {
        fontSize: 11,
        fontFamily: 'Montserrat',
        fontWeight: 400,
      },
      formatter: (value) => addPointInteger(value),
    },
  },
  series: [
    {
      name: 'Total',
      type: 'bar',
      data: [],
      itemStyle: { color: COLORS_GENDER['grisClaro'] },
    },
    {
      name: 'Hombres',
      type: 'bar',
      data: [],
      itemStyle: { color: COLORS_GENDER['azulMasculinoPalido'] },
    },
    {
      name: 'Mujeres',
      type: 'bar',
      data: [],
      itemStyle: { color: COLORS_GENDER['lilaFemenino'] },
    },
  ],
  media: [
    {
      query: {
        minWidth: 240,
      },
      option: {
        legend: {
          itemHeight: 10,
          itemWidth: 10,
          textStyle: {
            fontSize: '13',
          },
        },
      },
    },
    {
      query: {
        minWidth: 350,
      },
      option: {
        grid: {
          top: '50',
          right: '40',
        },
        title: {
          top: '0',
          text: TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('chartNameEfectivos'),
        },
        legend: {
          bottom: '0',
          itemGap: 20,
          padding: [10, 0],
        },
      },
    },
  ],
};