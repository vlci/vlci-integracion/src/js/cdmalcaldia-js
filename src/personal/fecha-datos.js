import { resultsetWithFields, initElement } from 'vlcishared';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_PERSONAL } from './texts';

export function setFechaDatosRecientes(datos) {
  const resultSet = resultsetWithFields(datos);
  const fechaMaxima = resultSet[0].max;
  const [year, month] = fechaMaxima.split('-');
  const monthName = TEXTS_PERSONAL[DEFAULT_LANGUAGE].get(month);
  const name = TEXTS_PERSONAL[DEFAULT_LANGUAGE].get('last_update');
  const phrase = `${name} ${monthName} ${year}`;
  const initLastUpdate = (element) => {
    // eslint-disable-next-line no-param-reassign
    element.textContent = phrase;
  };
  initElement('ultimaActualizacion', initLastUpdate);
}
