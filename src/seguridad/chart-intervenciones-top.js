import {
  resultsetWithFields,
  printNoDataChart,
  printDataChart,
  initChart,
} from 'vlcishared';
import { OPTIONS_INTERVENCIONES_TOP as options} from './options-charts';

export function echartIntervencionesTop(vis, datos) {
  const idChart = 'chart_IntervencionesTop';
  const idNoData = 'no_data_IntervencionesTop';
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);
  }

  const resultSet = resultsetWithFields(datos).reverse();
  resultSet.forEach((row) => {
    options.yAxis.data.push(row.detailcategory);
    options.series[0].data.push(row.count);
  });

  initChart(idChart, options);
}
