export const TEXTS_SEGURIDAD = {
  es_ES: new Map([
    ['intervenciones_totales', 'Intervenciones totales'],
    ['intervenciones', 'Intervenciones'],
    ['tipologia_nivel_1', 'Tipología nivel 1'],
    ['top_5', 'Top 5'],
    ['tipologia_seguridad', 'Tipología seguridad ciudadana'],
    ['evolucion_horaria', 'Evolución horaria']
  ]),
  ca_ES: new Map([
    ['intervenciones_totales', 'Intervencions totals'],
    ['intervenciones', 'Intervencions'],
    ['tipologia_nivel_1', 'Tipología nivell 1'],
    ['top_5', 'Top 5'],
    ['tipologia_seguridad', 'Tipologia seguretat ciutadana'],
    ['evolucion_horaria', 'Evolució horària']
  ]),
};
