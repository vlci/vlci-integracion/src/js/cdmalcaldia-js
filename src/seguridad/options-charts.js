import { formatTooltipPie, addPointInteger } from 'vlcishared';
import { TEXTS_SEGURIDAD } from './texts';
import { DEFAULT_LANGUAGE, TEXTS_COMMON } from '../common-language';
import { COLORS } from '../colors';

// TODO: revisar por Lucia
export const OPTIONS_INTERVENCIONES_TOP = {
  color: COLORS['naranjaPalido'],
  grid: {
    left: '0',
    right: '0',
    bottom: '0',
    top: '50',
    containLabel: true,
  },
  title: {
    text: TEXTS_SEGURIDAD[DEFAULT_LANGUAGE].get('top_5'),
    subtext: TEXTS_SEGURIDAD[DEFAULT_LANGUAGE].get('intervenciones'),
    margin: '0',
    padding: 0,
    top: '0',
    left: '50%',
    textAlign: 'center',
    itemGap: 5,
    textStyle: {
      fontSize: 15,
      lineHeight: 15,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
    subtextStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      lineHeight: 12,
      fontWeight: 400,
      color: '#666',
      width: '100%',
    },
  },
  legend: {
    show: false,
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      color: '#666',
    },
  },
  tooltip: {
    trigger: 'item',
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
        show: true,
      },
    },
  },
  xAxis: {
    type: 'value',
    data: [],
    show: false,
    axisLabel: {show: false},
  },
  yAxis: {
    type: 'category',
    data: [],
    show: false,
    axisLabel: {show: false},
  },
  series: [
    {
      type: 'bar',
      data: [],
      yAxisIndex: '0',
      offset: ['40', '0'],
      barWidth: '35',
      label: {
        show: true,
        position: 'insideLeft',
        textStyle: {
          fontSize: 13,
          fontFamily: 'Montserrat',
          color: '#000',
        },
        formatter: '{c} {b} ',
      },
    },
  ],
};

// TODO: revisar por Lucia
export const OPTIONS_INTERVENCIONES_BAR = {
  color: [COLORS['naranjaPalido'], COLORS['tealPalido']],
  grid: {
    left: '0',
    right: '0',
    bottom: '0',
    top: '60',
    containLabel: true,
  },
  title: {
    text: TEXTS_SEGURIDAD[DEFAULT_LANGUAGE].get('intervenciones_totales'),
    subtext: TEXTS_SEGURIDAD[DEFAULT_LANGUAGE].get('tipologia_seguridad'),
    margin: '0',
    padding: 0,
    top: '0',
    left: '50%',
    textAlign: 'center',
    itemGap: 5,
    textStyle: {
      fontSize: 15,
      lineHeight: 15,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
    subtextStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      lineHeight: 12,
      fontWeight: 400,
      color: '#666',
      width: '100%',
    },
  },
  legend: {
    orient: 'horizontal',
    bottom: '5',
    left: 'center',
    width: '100%',
    itemHeight: 10,
    itemWidth: 10,
    itemGap: 10,
    padding: [0, 0],
    textStyle: {
      fontSize: '11',
      fontFamily: 'Montserrat',
      fontWeight: '400',
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
        title: TEXTS_COMMON[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
    },
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
  },
  tooltip: {
    trigger: 'item',
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
  },
  xAxis: {
    type: 'category',
    axisTick: { show: false },
    axisLine: {
      lineStyle: {
        color: '#ccc',
      },
    },
    axisLabel: {
      show: true,
      interval: 0,
      rotate: 50,
      padding: 0,
      margin: 3,
      verticalAlign: 'top',
      fontSize: 9,
      lineHeight: 10,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#333',
    },
    data: [],
  },
  yAxis: {
    type: 'value',
    position: 'left',
    axisLabel: {
      textStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
        fontWeight: 400,
      },
    },
  },
  series: [
    {
      data: [],
      type: 'bar',
    },
  ],
  media: [
    {
      query: {
        minWidth: 420,
      },
      option: {
        xAxis: {
          axisLabel: {
            textStyle: {
              fontSize: 9,
            },
          },
        },
        series: [
          {
            barWidth: 18,
          },
        ],
      },
    },
  ],
};

// TODO: revisar por Lucia
export const OPTIONS_INTERVENCIONES_PIE = {
  color: Object.values(COLORS),
  grid: {
    left: '1%',
    right: '1%',
    bottom: '2%',
    top: '20%',
    containLabel: true,
  },
  title: {
    text: TEXTS_SEGURIDAD[DEFAULT_LANGUAGE].get('intervenciones_totales'),
    subtext: TEXTS_SEGURIDAD[DEFAULT_LANGUAGE].get('tipologia_nivel_1'),
    left: 'center',
    top: 'top',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
    subtextStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      lineHeight: 12,
      fontWeight: 400,
      color: '#666',
      width: '100%',
    },
  },
  legend: {
    show: false,
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
        title: TEXTS_COMMON[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
    },
  },
  tooltip: {
    trigger: 'item',
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    formatter: (params) => formatTooltipPie(params, true),
  },
  series: [
    {
      type: 'pie',
      radius: '60%',
      data: [],
      label: {
        show: true,
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 11,
          lineHeight: 18,
        },
        formatter: '{b}\n{c0}%',
        distanceToLabelLine: 10,
      },
      labelLine: {
        length: 10,
        length: 15,
      },
    },
  ],
};

// TODO: revisar por Lucia
export const OPTIONS_INTERVENCIONES_STACKED = {
  color: Object.values(COLORS),
  grid: {
    left: '0',
    right: '0',
    bottom: '0',
    top: '50',
    containLabel: true,
  },
  title: {
    text: TEXTS_SEGURIDAD[DEFAULT_LANGUAGE].get('intervenciones_totales'),
    subtext: TEXTS_SEGURIDAD[DEFAULT_LANGUAGE].get('evolucion_horaria'),
    margin: '0',
    padding: 0,
    top: '0',
    left: '50%',
    textAlign: 'center',
    itemGap: 5,
    textStyle: {
      fontSize: 15,
      lineHeight: 15,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
    subtextStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      lineHeight: 12,
      fontWeight: 400,
      color: '#666',
      width: '100%',
    },
  },
  legend: {
    show: false,
    orient: 'horizontal',
    bottom: '5',
    left: 'center',
    width: '100%',
    itemHeight: 10,
    itemWidth: 10,
    itemGap: 10,
    padding: [0, 0],
    textStyle: {
      fontSize: '11',
      fontFamily: 'Montserrat',
      fontWeight: '400',
      color: '#666',
    },
  },
  tooltip: {
    confine: true,
    position: ['50%', '20%'],
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      fontWeight: 600,
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      saveAsImage: {
      },
    },
  },
  xAxis: [
    {
      type: 'category',
      axisLabel: {
        show: true,
        interval: 0,
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 11,
        },
      },
      data: [],
    },
  ],
  yAxis: [
    {
      type: 'value',
      alignTicks: true,
      position: 'left',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
      },
    },
  ],
  legend: {
    data: [],
    show: false,
  },
  series: [],
};
