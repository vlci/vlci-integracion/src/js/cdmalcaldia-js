import {
  printNoDataChart,
  printDataChart,
  initChart
} from 'vlcishared';
import { OPTIONS_INTERVENCIONES_PIE as options} from './options-charts';

export function echartIntervencionesTotalesPie(vis, datos) {
  const misDatosArray = datos.resultset;

  const idChart = 'chart_IntervencionesTipo1Hoy';
  const idNoData = 'no_data_IntervencionesTipo1Hoy';
  if (misDatosArray.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);
  }

  const data = misDatosArray.map(([value, name]) => ({
    value,
    name,
  }));
  
  options.series[0].data = data;

  initChart(idChart, options);
}
