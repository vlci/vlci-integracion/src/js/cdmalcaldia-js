import { initChart, resultsetWithFields } from 'vlcishared';
import { OPTIONS_INTERVENCIONES_BAR as options} from './options-charts';

export function echartIntervencionesTotalesBar(vis, datos) {
  const resultSet = resultsetWithFields(datos);

  resultSet.forEach((row) => {
    options.xAxis.data.push(row.district);
    options.series[0].data.push(row.count);
  });

  initChart('chart_IntervencionesDiaSeguridadCiudadana', options);
}
