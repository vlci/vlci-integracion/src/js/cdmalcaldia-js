import {
  resultsetWithFields,
  printNoDataChart,
  printDataChart,
  initChart,
} from 'vlcishared';
import { OPTIONS_INTERVENCIONES_STACKED as options} from './options-charts';

export function echartIntervencionesTotalesStacked(vis, datos) {
  const idChart = 'chart_IntervencionesHoras';
  const idNoData = 'no_data_IntervencionesHoras';
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    let seriesMap = {};
    const groupedData = {};

    const resultSet = resultsetWithFields(datos);

    // Agrupar los datos por hora
    resultSet.forEach((row) => {
      if (!groupedData[row.hora]) {
        groupedData[row.hora] = {};
        options.xAxis[0].data.push(row.hora);
      }
      groupedData[row.hora][row.category] = row.count;

      if (!options.legend.data.includes(row.category)) {
        options.legend.data.push(row.category);
        seriesMap[row.category] = {
          name: row.category,
          type: 'bar',
          stack: 'total',
          barWidth: 30,
          data: [],
        };
      }
    });

    options.xAxis[0].data.forEach((hora) => {
      options.legend.data.forEach((cat) => {
        seriesMap[cat].data.push(groupedData[hora][cat] || 0);
      });
    });

    options.series = Object.values(seriesMap);

    initChart(idChart, options);
  }
}
