import {
  initStackedBarChartMonthSeries,
  getMonth,
  addPointInteger,
  resultsetWithFields,
} from 'vlcishared';

import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXT_ACCESOS } from './texts';

let seriesData;

function sameMonthAllYearsFormatter(params) {
  const monthInAllYears = seriesData.filter(
    (entrie) => getMonth(entrie.mes) === params.seriesName,
  );

  let nameLabel = `<p>${params.seriesName}</p>`;

  monthInAllYears.forEach((month) => {
    const valuesLabel = `<div>${params.marker} ${month.anyo}:
          <strong style="float: right; margin-left:20px">
            ${month.accesos ? addPointInteger(month.accesos) : '-'}
          </strong>
        </div>`;
    nameLabel += valuesLabel;
  });
  return nameLabel;
}

export function echartAccesosSede(data) {
  seriesData = resultsetWithFields(data);
  const updateOptions = {
    title: {
      text: TEXT_ACCESOS[DEFAULT_LANGUAGE].get('title'),
      textStyle: {
        fontSize: 15,
        fontWeight: 400,
      },
    },
    tooltip: {
      trigger: 'item',
      formatter: (params) => sameMonthAllYearsFormatter(params),
    },
    xAxis: {
      name: `\n\n\n\n${TEXT_ACCESOS[DEFAULT_LANGUAGE].get('acces')}`,
    },
  };
  const updateDataOptions = {
    label: {
      show: false,
    },
  };

  initStackedBarChartMonthSeries(
    'chartAccesosSede',
    data.resultset,
    updateOptions,
    updateDataOptions,
  );
}
