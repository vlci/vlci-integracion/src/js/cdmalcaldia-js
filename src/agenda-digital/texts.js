export const TEXT_ACCESOS = {
  es_ES: new Map([
    ['acces', 'Accesos'],
    ['title', 'ACCESOS A LA SEDE ELECTRÓNICA'],
  ]),
  ca_ES: new Map([
    ['acces', 'Accessos'],
    ['title', 'ACCESSOS A SEU ELECTRÒNICA'],
  ]),
};
