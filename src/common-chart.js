import { addPointInteger, resultsetWithFields, initElement } from 'vlcishared';
import { DEFAULT_LANGUAGE, TEXTS_COMMON } from './common-language';

/** Formateo del tooltip de gauge y pie
 * @param {object} params elementos del parámetro a formatear
 * @param {boolean} isPie Determina si es pie o no para mostrar %
 * @returns cadena formateada.
 */
export function formatRoundTooltip(params, isPie) {
  let label = `${params.name} : `;
  label += `<span style="font-weight:600;">${addPointInteger(
    Math.round(params.value),
  )}</span>`;
  if (isPie) {
    label += `<span style="font-weight:600;"> (${params.percent}%)</span>`;
  }
  return params.marker + label;
}

export function setFechaDatosRecientes(datos, idElement) {
  const resultSet = resultsetWithFields(datos);
  const fechaMaxima = resultSet[0].fecha_maxima;
  const initLastUpdate = (element) => {
    // eslint-disable-next-line no-param-reassign
    element.textContent = `${fechaMaxima}`;
  };

  initElement(idElement, initLastUpdate);
}
