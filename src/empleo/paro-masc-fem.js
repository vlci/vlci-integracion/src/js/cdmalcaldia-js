import {
  resultsetWithFields,
  printNoDataChart,
  printDataChart,
  initChart,
  addPointInteger,
  formatTooltipPie,
} from 'vlcishared';

import { COLORS, COLORS_GENDER } from '../colors';
import { TEXTS_EMPLEO } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

const options = {
  color: [
    COLORS_GENDER['lilaFemenino'],
    COLORS_GENDER['azulMasculino'],
  ],
  grid: {
    left: '0',
    right: '0',
    bottom: '0',
    top: '0',
    containLabel: true,
  },
  tooltip: {
    trigger: 'item',
    confine: true,
    textStyle: {
      fontFamily: 'Montserrat',
    },
    renderMode: 'html',
    formatter: (params) => {
      params.name = `<span style="font-size: 90%;font-weight: 500;color: #666;">Desempleo ` + params.name + `</span>`;
      params.value = `<span style="font-weight: 600;color: #565656;">` + addPointInteger(params.value) + ` personas</span></span>`;
      return formatTooltipPie(params, false);
    },
  },
  series: [
    {
      type: 'pie',
      radius: '60%',
      startAngle: 45,
      data: [],
      label: {
        show: true,
        formatter(param) {
          return `${param.percent}%\nDesempleo\n${param.name}`;
        },
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 13,
        },
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  ],
};

export function echartParoMascFem(datos) {
  const results = resultsetWithFields(datos);
  const idChart = 'chartParoGenero';
  const idNoData = 'noDataChartParoGenero';

  results.forEach((row) => {
    options.series[0].data.push({
      value: row.kpivalue,
      name: row.sliceanddicevalue1cas,
    });
  });

  if (results.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);
  }

  initChart(idChart, options);
}
