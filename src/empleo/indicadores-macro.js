import {
  printDataInHTMLElements,
  formatMonthAnyo,
  formatTrimestre,
  resultsetWithFields,
  addPointInteger,
} from 'vlcishared';

export function indicadoresMacro(data) {
  const dataConfig = {
    trimestre2: {
      value: {
        element: '#kpiValorPrePrePrevioTrim',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#kpiPeriodPrePrePrevioTrim',
        format: formatTrimestre,
      },
      tendencia: {
        className: '.prePrePrevioTrim',
      },
    },
    trimestre3: {
      value: {
        element: '#kpiValorPrePrevioTrim',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#kpiPeriodPrePrevioTrim',
        format: formatTrimestre,
      },
      tendencia: {
        className: '.prePrevioTrim',
      },
    },
    trimestre4: {
      value: {
        element: '#kpiValorPrevioTrim',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#kpiPeriodPrevioTrim',
        format: formatTrimestre,
      },
      tendencia: {
        className: '.previoTrim',
      },
    },
    trimestre5: {
      value: {
        element: '#kpiValorActualTrim',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#kpiPeriodActualTrim',
        format: formatTrimestre,
      },
      tendencia: {
        className: '.actualTrim',
      },
    },
    mensual2: {
      value: {
        element: '#kpiValorPrePrePrevioMes',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#kpiPeriodPrePrePrevioMes',
        format: formatMonthAnyo,
      },
      tendencia: {
        className: '.prePrePrevioMes',
      },
    },
    mensual3: {
      value: {
        element: '#kpiValorPrePrevioMes',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#kpiPeriodPrePrevioMes',
        format: formatMonthAnyo,
      },
      tendencia: {
        className: '.prePrevioMes',
      },
    },
    mensual4: {
      value: {
        element: '#kpiValorPrevioMes',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#kpiPeriodPrevioMes',
        format: formatMonthAnyo,
      },
      tendencia: {
        className: '.previoMes',
      },
    },
    mensual5: {
      value: {
        element: '#kpiValorActualMes',
        format: addPointInteger,
      },
      calculationperiod: {
        element: '#kpiPeriodActualMes',
        format: formatMonthAnyo,
      },
      tendencia: {
        className: '.actualMes',
      },
    },
    sexo3: {
      value: {
        element: '#kpiValorActualFemenino',
        format: addPointInteger,
      },
      tendencia: {
        className: '.femeni',
      },
    },
    sexo4: {
      value: {
        element: '#kpiValorActualMasculino',
        format: addPointInteger,
      },
      tendencia: {
        className: '.masculi',
      },
    },
  };
  const dataWithFields = resultsetWithFields(data);
  printDataInHTMLElements(dataWithFields, dataConfig);
}
