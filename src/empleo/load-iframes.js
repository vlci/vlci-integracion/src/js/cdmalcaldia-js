import { loadIframeOnClickEvent } from 'vlcishared';

export function loadEmpleoFrames() {
  document
    .querySelector('#iframe-demanda')
    .setAttribute(
      'src',
      'https://app.powerbi.com/view?r=eyJrIjoiOTczMDIyNTYtMjk1Yi00MzNiLWFmYWQtZmZiNWJlYWJlNGJjIiwidCI6ImM2ODQ2NmVmLWFiOWItNGYwZS04ZTczLWViMjIwYWYyNTc5NiIsImMiOjl9',
    );

  loadIframeOnClickEvent(
    '#demanda-tab',
    '#iframe-demanda',
    'https://app.powerbi.com/view?r=eyJrIjoiOTczMDIyNTYtMjk1Yi00MzNiLWFmYWQtZmZiNWJlYWJlNGJjIiwidCI6ImM2ODQ2NmVmLWFiOWItNGYwZS04ZTczLWViMjIwYWYyNTc5NiIsImMiOjl9',
  );

  loadIframeOnClickEvent(
    '#apartamentos-tab',
    '#iframe-apartamentos',
    'https://app.powerbi.com/view?r=eyJrIjoiOWRmZTJlZTctOWQzNS00NTNlLWJhYzgtNjA3MTk2MGMyNDNkIiwidCI6ImM2ODQ2NmVmLWFiOWItNGYwZS04ZTczLWViMjIwYWYyNTc5NiIsImMiOjl9',
  );
}
