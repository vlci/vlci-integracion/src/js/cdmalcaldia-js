import {
  resultsetWithFields,
  printNoDataChart,
  printDataChart,
  initChart,
} from 'vlcishared';

import { COLORS, COLORS_GENDER } from '../colors';
import { TEXTS_EMPLEO } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

/** Se tiene que declarar a nivel de fichero para poder acceder ael
 * desde el tooltip formatter
 */
const meses = new Set();

/**
 * Función que cambia la location de un numero al idioma es-ES (Separador de miles y decimales).
 * @param {String} value Valor o numero expresado como string.
 * @returns Devuelve el mismo valor adaptado al idioma es-ES.
 */
const valueToLocaleString = (value) => value.toLocaleString('es-ES');

/**
 * Función que devuelve el formato para el toolkip de las graficas
 * @param {Object} params Objeto proporcionado por la libreria echarts con
 * todos los parametros que posee un tooltip.
 * @returns Devuelve un string html con el formato que se mostrara en el tooltip de la página.
 */
function tooltipFormat(params) {
  console.warn(params);
  //const fecha = DEFAULT_LANGUAGE === 'es_ES' ? 'Fecha' : 'Data';
  const value = valueToLocaleString(params.value);
  const html = `
  <div style="font-weight: bold; font-size:11px;">
    <span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:${
      params.color
    }"></span>
    ${[...meses][params.dataIndex]}
  </div>
  <div>
    ${params.seriesName} : <span style="font-weight: bold">${value}</span>
  </div>
   
  `;
  return html;
}

/**
 * Compara los datos de genero primero por año, luego por mes y por ultimo por genero
 * @param {*} entryA
 * @param {*} entryB
 * @returns
 */
const sortDataByDateAndGender = (entryA, entryB) => {
  const [entryAYear, entryAMonth] = entryA.calculationperiod.split('-');
  const [entryBYear, entryBMonth] = entryB.calculationperiod.split('-');

  if (entryAYear > entryBYear) return 1;
  if (entryAYear < entryBYear) return -1;
  if (entryAMonth > entryBMonth) return 1;
  if (entryAMonth < entryBMonth) return -1;
  if (entryA.sliceanddicevalue > entryB.sliceanddicevalue) return 1;
  if (entryA.sliceanddicevalue < entryB.sliceanddicevalue) return -1;

  return 0;
};

/**
 * Obtener la suma del paro femenino más masculino.
 * @param {*} data
 * @returns array data
 */
const getDataTotalValue = (data) => {
  const dataParoTotal = [];
  data.forEach((element, index, array) => {
    if (index % 2 === 0 && index + 1 < array.length) {
      dataParoTotal.push({
        kpivalue:
          parseFloat(element.kpivalue) + parseFloat(array[index + 1].kpivalue),
        calculationperiod: element.calculationperiod,
      });
    }
  });
  return dataParoTotal;
};

const options = {
  grid: {
    left: '0',
    right: '0',
    bottom: '0',
    top: '10',
  },
  tooltip: {
    trigger: 'item',
    show: true,
    confine: true,
    textStyle: {
      fontSize: 13,
      fontFamily: 'Montserrat',
    },
    formatter: tooltipFormat,
  },
  xAxis: [
    {
      type: 'category',
      show: false,
    },
  ],
  yAxis: [
    {
      type: 'value',
      min: 0,
      show: false,
    },
    {
      type: 'value',
      show: false,
    },
  ],
  series: [
    {
      name: 'Desempleo Total',
      type: 'line',
      smooth: true,
      yAxisIndex: 1,
      areaStyle: {
        color: '#ddd',
      },
      symbol: 'circle',
      symbolSize: '4',
      lineStyle: {
        width: '2',
      },
      tooltip: {
        position: 'bottom',
        confine: true,
      },
      zlevel: 1,
      color: '#8e8e8d',
      color: '#666',
    },
    {
      name: 'Mujeres Desempleadas',
      type: 'bar',
      zlevel: 2,
      color: COLORS_GENDER['lilaFemenino'],
    },
    {
      name: 'Hombres desempleados',
      type: 'bar',
      zlevel: 2,
      color: COLORS_GENDER['azulMasculino'],
    },
  ],
};

export function echartParoMensual(datos) {
  const results = resultsetWithFields(datos);
  results.sort(sortDataByDateAndGender);
  const idChart = 'chartParoAnual';
  const idNoData = 'noDataChartParoAnual';

  if (results.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
    return;
  }
  printDataChart(`#${idNoData}`, `#${idChart}`);

  const paroMujeres = [];
  const paroHombres = [];
  const paroTotal = getDataTotalValue(results).map(
    (element) => element.kpivalue,
  );

  results.forEach((row) => {
    meses.add(
      `${TEXTS_EMPLEO[DEFAULT_LANGUAGE].get(
        row.calculationperiod.substring(5),
      )} ${row.calculationperiod.substring(0, 4)}`,
    );
    if (
      row.sliceanddicevalue === TEXTS_EMPLEO[DEFAULT_LANGUAGE].get('GeneroM')
    ) {
      paroHombres.push(parseInt(row.kpivalue, 10));
    } else {
      paroMujeres.push(parseInt(row.kpivalue, 10));
    }
  });

  const maxBarChart = Math.max(...paroHombres.concat(paroMujeres)) * 2;
  const minLineChart = Math.min(...paroTotal) / 1.2;
  const maxLineChart = Math.max(...paroTotal);

  options.xAxis.data = meses;
  options.yAxis[0].max = maxBarChart;
  options.yAxis[1].max = maxLineChart;
  options.yAxis[1].min = minLineChart;
  options.series[0].data = paroTotal;
  options.series[1].data = paroMujeres;
  options.series[2].data = paroHombres;

  //   console.warn(options);
  initChart(idChart, options);
}
