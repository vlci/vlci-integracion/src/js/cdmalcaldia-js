import { 
  initBarChartComparationMonth,
  addPointInteger,
} from 'vlcishared';
import { COLORS } from '../colors';
import { TEXT_RRHH } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

export function echartTareasRealizadas(datos) {
  const dataArray = datos.resultset;

  const [[, , , maxValueRaw]] = dataArray;
  const maxValue = Math.ceil(maxValueRaw / 200000) * 200000;

  const specificOptions = {
    grid: {
      left: '0',
      right: '0',
      bottom: '30',
      top: '50',
      containLabel: true,
    },
    yAxis: [
      {
        type: 'value',
        max: maxValue,
        axisLabel: {
          fontFamily: 'Montserrat',
          fontSize: 10,
          formatter: (value) => addPointInteger(value),
        },
      },
    ],
    title: {
      text: TEXT_RRHH[DEFAULT_LANGUAGE].get('titleTareasRealizadas'),
      textStyle: {
        fontSize: 15,
        fontFamily: 'Montserrat',
        fontWeight: 500,
        color: '#666',
      },
    },
    legend: {
      top: 'bottom',
      show: true,
      textStyle: {
        fontFamily: 'Montserrat',
      },
    },
    toolbox: {
      show: true,
      right: '0',
      top: '0',
      feature: {
        restore: {
          title: TEXT_RRHH[DEFAULT_LANGUAGE].get('reset'),
          textStyle: {
            fontSize: 6,
            fontFamily: 'Montserrat',
            fontWeight: 300,
          },
        },
        saveAsImage: {
          title: TEXT_RRHH[DEFAULT_LANGUAGE].get('download'),
          textStyle: {
            fontSize: 6,
            fontFamily: 'Montserrat',
            fontWeight: 300,
          },
        },
      },
      emphasis: {
        iconStyle: {
          borderColor: '#ffcd00',
          textFill: '#666',
        },
      },
    },
    tooltip: {
      textStyle: {
        fontFamily: 'Montserrat',
        fontSize: 12,
      },
    },
    series: [
      {
        color: '#F1AB86',
      },
      {
        color: '#48A9A6',
      },
    ],
  };

  initBarChartComparationMonth(
    'chartTareasRealizadas',
    dataArray,
    specificOptions,
  );
}
