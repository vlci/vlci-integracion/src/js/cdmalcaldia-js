import { 
  initBarChartComparationMonth,
  addPointInteger,
} from 'vlcishared';
import { COLORS } from '../colors';
import { TEXT_RRHH } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

export function echartTareasPendientes(datos) {
  const dataArray = datos.resultset;
  const unidadMedida = TEXT_RRHH[DEFAULT_LANGUAGE].get('tasks');

  const [[, , , maxValueRaw]] = dataArray;
  const maxValue = Math.ceil(maxValueRaw / 200000) * 200000;

  // Opciones especificas sobre los datos
  const updateDataOptions = {
    label: {
      width: '50',
      overflow: 'truncate',
      ellipsis: 'Tn',
      textStyle: {
        fontWeight: 500,
      },
    },
  };

  const specificOptions = {
    grid: {
      left: '0',
      right: '0',
      bottom: '30',
      top: '50',
      containLabel: true,
    },
    title: {
      text: TEXT_RRHH[DEFAULT_LANGUAGE].get('titleTareasPendientes'),
      textStyle: {
        fontSize: 15,
        fontFamily: 'Montserrat',
        fontWeight: 500,
        color: '#666',
      },
    },
    yAxis: [
      {
        type: 'value',
        max: maxValue,
        axisLabel: {
          fontFamily: 'Montserrat',
          fontSize: 10,
          formatter: (value) => addPointInteger(value),
        },
      },
    ],
    legend: {
      top: 'bottom',
      show: true,
      textStyle: {
        fontFamily: 'Montserrat',
      },
    },
    toolbox: {
      show: true,
      right: '0',
      top: '0',
      feature: {
        restore: {
          title: 'Restablecer',
          title: TEXT_RRHH[DEFAULT_LANGUAGE].get('reset'),
          textStyle: {
            fontSize: 6,
            fontFamily: 'Montserrat',
            fontWeight: 300,
          },
        },
        saveAsImage: {
          title: TEXT_RRHH[DEFAULT_LANGUAGE].get('download'),
          textStyle: {
            fontSize: 6,
            fontFamily: 'Montserrat',
            fontWeight: 300,
          },
        },
      },
      emphasis: {
        iconStyle: {
          borderColor: '#ffcd00',
          textFill: '#666',
        },
      },
    },
    tooltip: {
      textStyle: {
        fontFamily: 'Montserrat',
        fontSize: 12,
      },
    },
    series: [
      {
        color: COLORS.naranjaPalido,
      },
      {
        color: COLORS.tealPalido,
      },
    ],
  };

  initBarChartComparationMonth(
    'chartTareasPendientes',
    dataArray,
    specificOptions,
  );
}
