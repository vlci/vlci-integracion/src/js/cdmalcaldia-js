export { echartTareasRealizadas } from './tareas-realizadas-mes';
export { echartTareasPendientes } from './tareas-pendientes-mes';
export { echartStackedBarBacklog } from './char-backlog';
export { echartTareasRealizadasAnyo } from './tareas-realizadas-anyo';
