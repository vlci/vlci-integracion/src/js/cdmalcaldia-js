import {
  printNoDataChart,
  printDataChart,
  formatTooltip,
  formatShortMonthShortYear,
  initStackedBarChart,
  addPointInteger,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXT_RRHH } from './texts';

export function echartStackedBarBacklog(datos) {
  const idChart = 'chartBacklog';
  const idNoData = 'no_dataBacklog';

  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    const misDatosArray = datos.resultset;

    const date = formatShortMonthShortYear(misDatosArray[0][0]);
    const updateOptions = {
      grid: {
        left: '0',
        right: '2%',
        bottom: '30',
        top: '50',
        containLabel: true,
      },
      title: {
        text: `${TEXT_RRHH[DEFAULT_LANGUAGE].get(
          'titleBacklogMensual',
        )} - ${date.toUpperCase()}`,
        left: 'center',
        top: '0',
        textStyle: {
          fontSize: 15,
          fontFamily: 'Montserrat',
          fontWeight: 500,
          color: '#666',
        },
      },
      legend: {
        show: false,
      },
      toolbox: {
        show: true,
        right: '0',
        top: '0',
        feature: {
          restore: {
            title: TEXT_RRHH[DEFAULT_LANGUAGE].get('reset'),
            textStyle: {
              fontSize: 6,
              fontFamily: 'Montserrat',
              fontWeight: 300,
            },
          },
          saveAsImage: {
            title: TEXT_RRHH[DEFAULT_LANGUAGE].get('download'),
            textStyle: {
              fontSize: 6,
              fontFamily: 'Montserrat',
              fontWeight: 300,
            },
          },
        },
        emphasis: {
          iconStyle: {
            borderColor: '#ffcd00',
            textFill: '#666',
          },
        },
      },
      tooltip: {
        textStyle: {
          fontFamily: 'Montserrat',
          fontSize: 12,
        },
        formatter: (params) => {
          const param = [params];
          param[0].seriesName = formatShortMonthShortYear(param[0].seriesName);
          return formatTooltip(param, false, true, 'meses');
        },
      },
      xAxis: {
        name: '',
        max: (values) => Math.round(values.max + 10),
        splitNumber: 3,
        axisLabel: {
          show: true,
          formatter: (params) =>
            `${params} ` + `${TEXT_RRHH[DEFAULT_LANGUAGE].get('months')}`,
          interval: 0,
          margin: 10,
          textStyle: {
            fontSize: 11,
            fontFamily: 'Montserrat',
            fontWeight: 400,
          },
        },
      },
      yAxis: {
        axisTick: { show: false },
        axisLabel: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
      },
      series: [
        {
          color: COLORS.naranjaPalido,
          offset: ['0', '-35'],
          label: { show: false },
        },
      ],
    };
    const updateDataOptions = {
      label: {
        formatter: (params) => addPointInteger(params.value),
      },
    };

    initStackedBarChart(
      idChart,
      misDatosArray,
      updateOptions,
      updateDataOptions,
    );
  }
}
