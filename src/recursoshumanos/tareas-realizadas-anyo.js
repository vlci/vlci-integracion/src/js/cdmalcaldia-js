import {
  resultsetWithFields,
  printNoDataChart,
  printDataChart,
  initChart,
  addPointInteger,
  formatToMillions,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXT_RRHH } from './texts';

const ANYO_COLUMN = 'año';
const TAREAS_COLUMN = 'tareas_realizadas';
const TAREAS_PEND_COLUMN = 'tareas_pendientes';

/**
 * Opciones por defecto para todos los graficos stacked bar
 * Atributos que son necesarios rellenar : title{text},data,series.
 */
const options = {
  grid: {
    left: '0',
    right: '15',
    bottom: '30',
    top: '50',
    containLabel: true,
  },
  title: {
    left: 'center',
    top: 'top',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    top: 'bottom',
    show: true,
    textStyle: {
      fontFamily: 'Montserrat',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      restore: {
        title: TEXT_RRHH[DEFAULT_LANGUAGE].get('reset'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
      saveAsImage: {
        title: TEXT_RRHH[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
    },
    emphasis: {
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },
    },
  },
  tooltip: {
    valueFormatter(value) {
      return `${addPointInteger(value)} ${TEXT_RRHH[DEFAULT_LANGUAGE].get(
        'tasks',
      )}`;
    },
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      fontWeight: 600,
    },
  },
  xAxis: [
    {
      type: 'category',
      boundaryGap: false,
      axisLabel: {
        show: true,
        interval: 0,
        margin: 10,
        textStyle: {
          fontSize: 11,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
      },
      data: [],
    },
  ],
  yAxis: [
    {
      type: 'value',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => formatToMillions(value),
      },
    },
  ],
  series: [
    {
      type: 'line',
      data: [],
      symbolSize: 0,
      areaStyle: {
        opacity: 0.8,
      },
      zlevel: 1,
      name: TEXT_RRHH[DEFAULT_LANGUAGE].get('legendTareasRealizadasAnyo'),
      color: COLORS.tealPalido,
      smooth: 0.1,
    },
    {
      type: 'line',
      data: [],
      symbolSize: 0,
      areaStyle: {
      },
      name: TEXT_RRHH[DEFAULT_LANGUAGE].get('titleTareasPendientesAnyo'),
      color: COLORS.naranjaPalido,
      smooth: 0.1,
    },
  ],
};

export function echartTareasRealizadasAnyo(datos) {
  const idChart = 'chartTareasRealizadasAnyo';
  const idNoData = 'no_dataTareasRealizadasAnyo';

  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    // Reiniciar datos
    options.xAxis[0].data = [];
    options.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      options.xAxis[0].data.push(`${row[ANYO_COLUMN]}`);
      options.series[0].data.push(row[TAREAS_COLUMN]);
      options.series[1].data.push(row[TAREAS_PEND_COLUMN]);
    });

    options.title.text = TEXT_RRHH[DEFAULT_LANGUAGE].get(
      'titleTareasRealizadasAnyo',
    );

    initChart(idChart, options);
  }
}
