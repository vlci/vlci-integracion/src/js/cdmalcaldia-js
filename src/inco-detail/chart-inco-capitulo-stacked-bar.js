import {
  resultsetWithFields,
  formatMonthAnyo,
  printNoDataChart,
  printDataChart,
  initChart,
  updateStackedBarOptions,
  formatName,
  formatTooltip,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_INCO } from './texts';

const CAPITULO_COLUMN = 'capitulo';
const ID_CAPITULO_COLUMN = 'id_capitulo';
const PORCENTAJE_REAL_VS_PRESUP_COLUMN = 'real_vs_presupuestado';
const PREV_DEFINITIVA_COLUMN = 'prevision_definitiva';
const DERECHOS_RECONOCIDOS_NETOS_COLUMN = 'derechos_reconocidos_netos';
const DATE = 'fecha';
const SIZE_VALUE = 8;

const options = {
  grid: {
    left: '0',
    right: '0',
    bottom: '25',
    top: '50',
    containLabel: true,
  },
  toolbox: {
    feature: {
      restore: {
        title: TEXTS_INCO[DEFAULT_LANGUAGE].get('reset'),
      },
      saveAsImage: {
        title: TEXTS_INCO[DEFAULT_LANGUAGE].get('download'),
      },
    },
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    formatter: (params) => formatTooltip(params, false, true),
  },
  xAxis: [
    {
      type: 'category',
    },
  ],
  series: [
    {
      name: `${TEXTS_INCO[DEFAULT_LANGUAGE].get('prev_definitiva')}`,
      yAxisIndex: 1,
      type: 'bar',
      barWidth: 23,
      data: [],
      color: COLORS.naranjaPalido,
    },
    {
      name: `${TEXTS_INCO[DEFAULT_LANGUAGE].get('derechos_reconoc_netos')}`,
      yAxisIndex: 1,
      type: 'bar',
      barWidth: 23,
      data: [],
      color: COLORS.tealPalido,
    },
    {
      name: TEXTS_INCO[DEFAULT_LANGUAGE].get('real_vs_presupuestados'),
      type: 'line',
      data: [],
      color: COLORS.cranberry,
    },
  ],
  media: [
    {
      query: {
        minWidth: 770,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              textStyle: {
                fontSize: 10,
              },
            },
          },
        ],
      },
    },
  ],
};

export function echartStackedBarIncoCapitulo(vis, datos) {
  const idChart = 'chartIngresosCapitulo';
  const idNoData = 'no_dataIngresosCapitulo';

  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    let updatedOptions = {};
    updatedOptions = updateStackedBarOptions(options);

    // Reiniciar datos
    updatedOptions.xAxis[0].data = [];
    updatedOptions.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);
    const date = formatMonthAnyo(resultSet[0][DATE]);
    resultSet.forEach((row) => {
      updatedOptions.xAxis[0].data.push(
        `${row[ID_CAPITULO_COLUMN]}.${row[CAPITULO_COLUMN]}`,
      );
      updatedOptions.series[0].data.push(row[PREV_DEFINITIVA_COLUMN]);
      updatedOptions.series[1].data.push(
        row[DERECHOS_RECONOCIDOS_NETOS_COLUMN],
      );
      updatedOptions.series[2].data.push(row[PORCENTAJE_REAL_VS_PRESUP_COLUMN]);
    });

    updatedOptions.xAxis[0].axisLabel = {
      interval: 0,
      formatter: (value) => formatName(value, SIZE_VALUE),
      textStyle: {
        fontSize: 9,
        lineHeight: 13,
        fontFamily: 'Montserrat',
      },
    };

    updatedOptions.title.text = `${TEXTS_INCO[DEFAULT_LANGUAGE].get(
      'chartNameIncoCapitulo',
    )} - ${date}`;

    initChart(idChart, updatedOptions);
  }
}
