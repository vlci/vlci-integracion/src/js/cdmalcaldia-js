import {
  resultsetWithFields,
  formatMonthAnyo,
  printNoDataChart,
  printDataChart,
  formatTooltip,
  initChart,
  updateStackedBarOptions,
  formatShortMonthYear,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_INCO } from './texts';

const DATE_COLUMN = 'fecha';
const PORCENTAJE_REAL_VS_PRESUP_COLUMN = 'real_vs_presupuestado';
const PREV_DEFINITIVA_COLUMN = 'prevision_definitiva';
const DERECHOS_RECONOCIDOS_NETOS_COLUMN = 'derechos_reconocidos_netos';

/**
 * Opciones por defecto para todos los graficos stacked bar
 * Atributos que son necesarios rellenar : title{text},data,series.
 */
const options = {
  title: {
    text: TEXTS_INCO[DEFAULT_LANGUAGE].get('chartNameIncoEjecucion'),
  },
  grid: {
    left: '0',
    right: '0',
    bottom: '82',
    top: '50',
  },
  toolbox: {
    feature: {
      restore: {
        title: TEXTS_INCO[DEFAULT_LANGUAGE].get('reset'),
      },
      saveAsImage: {
        title: TEXTS_INCO[DEFAULT_LANGUAGE].get('download'),
      },
    },
  },
  tooltip: {
    confine: true,
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    formatter: (params) => {
      params[0].name = formatMonthAnyo(params[0].name);
      return formatTooltip(params, false, true);
    },
  },
  series: [
    {
      name: `${TEXTS_INCO[DEFAULT_LANGUAGE].get('prev_definitiva')}`,
      yAxisIndex: 1,
      type: 'bar',
      barWidth: 23,
      data: [],
      color: COLORS.naranjaPalido,
    },
    {
      name: `${TEXTS_INCO[DEFAULT_LANGUAGE].get('derechos_reconoc_netos')}`,
      yAxisIndex: 1,
      type: 'bar',
      barWidth: 23,
      data: [],
      color: COLORS.tealPalido,
    },
    {
      name: TEXTS_INCO[DEFAULT_LANGUAGE].get('real_vs_presupuestados'),
      type: 'line',
      data: [],
      color: COLORS.cranberry,
    },
  ],
};

export function echartStackedBarIncoEjecucion(vis, datos) {
  const idChart = 'chartEvolucionEjecucion';
  const idNoData = 'no_dataEvolucionEjecucion';
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    let updatedOptions = {};
    updatedOptions = updateStackedBarOptions(options);

    // Reiniciar datos
    updatedOptions.xAxis[0].data = [];
    updatedOptions.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      updatedOptions.xAxis[0].data.push(`${row[DATE_COLUMN]}`);
      updatedOptions.series[0].data.push(row[PREV_DEFINITIVA_COLUMN]);
      updatedOptions.series[1].data.push(
        row[DERECHOS_RECONOCIDOS_NETOS_COLUMN],
      );
      updatedOptions.series[2].data.push(row[PORCENTAJE_REAL_VS_PRESUP_COLUMN]);
    });

    updatedOptions.xAxis[0].axisLabel = {
      interval: 0,
      formatter: (value) => formatShortMonthYear(value),
      textStyle: {
        fontSize: 11,
        fontFamily: 'Montserrat',
        fontWeight: 400,
      },
    };

    initChart(idChart, updatedOptions);
  }
}
