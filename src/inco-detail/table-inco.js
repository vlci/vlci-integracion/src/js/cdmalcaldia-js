import {
  initTable,
  formatNameHeaderColumn,
  downloadCSVTable,
  downloadPDFTable,
  checkAllGroupsCollapsed,
  resultsetWithFields,
  formatMonthAnyo,
} from 'vlcishared';

import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_INCO } from './texts';

const CAPITULO_COLUMN = 'capitulo';
const SUBCONCEPTO_COLUMN = 'subconcepto';
const PREV_INICIAL_COLUMN = 'prevision_inicial';
const MOD_INCIAL_COLUMN = 'modificacion_inicial';
const PREV_DEFINITIVA_COLUMN = 'prevision_definitiva';
const DERECHOS_RECONOCIDOS_COLUMN = 'derechos_reconocidos';
const DERECHOS_RECONOCIDOS_NETOS_COLUMN = 'derechos_reconocidos_netos';
const DERECHOS_RECONOCIDOS = 'derechos_reconocidos';
const DERECHOS_ANULADOS_COLUMN = 'derechos_anulados';
const RECAUDACION_NETA_COLUMN = 'recaudacion_neta';
const DERECHOS_PEND_COBRO_COLUMN = 'derechos_pendientes_cobro';
const DERECHOS_CANCELADOS_COLUMN = 'derechos_cancelados';

const formatterMoney = {
  decimal: ',',
  thousand: '.',
  symbol: '€',
  symbolAfter: 'p',
  precision: false,
};

const commonObjectMetric = {
  width: '9%',
  hozAlign: 'center',
  formatter: 'money',
  formatterParams: formatterMoney,
  topCalc: 'sum',
  topCalcParams: {
    precision: 2,
  },
  topCalcFormatter: 'money',
  topCalcFormatterParams: formatterMoney,
};

// Propiedades de las columnas de la tabla
const columnasTabla = [
  {
    field: SUBCONCEPTO_COLUMN,
    sorter: 'string',
    width: '20%',
    hozAlign: 'center',
    frozen: true,
    headerSort: false,
  },
  {
    title: TEXTS_INCO[DEFAULT_LANGUAGE].get('prevision_inicial'),
    field: PREV_INICIAL_COLUMN,
    titleFormatter: (cell) => formatNameHeaderColumn(cell, 9),
    headerSort: false,
    ...commonObjectMetric,
  },
  {
    title: TEXTS_INCO[DEFAULT_LANGUAGE].get('modificacion_inicial'),
    field: MOD_INCIAL_COLUMN,
    titleFormatter: (cell) => formatNameHeaderColumn(cell, 12),
    headerSort: false,
    ...commonObjectMetric,
  },
  {
    title: TEXTS_INCO[DEFAULT_LANGUAGE].get('prevision_definitiva'),
    field: PREV_DEFINITIVA_COLUMN,
    titleFormatter: (cell) => formatNameHeaderColumn(cell, 10),
    headerSort: false,
    ...commonObjectMetric,
  },
  {
    title: TEXTS_INCO[DEFAULT_LANGUAGE].get('derechos_reconocidos'),
    field: DERECHOS_RECONOCIDOS_COLUMN,
    titleFormatter: (cell) => formatNameHeaderColumn(cell, 9),
    headerSort: false,
    ...commonObjectMetric,
  },
  {
    title: TEXTS_INCO[DEFAULT_LANGUAGE].get('derechos_anulados'),
    field: DERECHOS_ANULADOS_COLUMN,
    titleFormatter: (cell) => formatNameHeaderColumn(cell, 9),
    headerSort: false,
    ...commonObjectMetric,
  },
  {
    title: TEXTS_INCO[DEFAULT_LANGUAGE].get('derechos_cancelados'),
    field: DERECHOS_CANCELADOS_COLUMN,
    titleFormatter: (cell) => formatNameHeaderColumn(cell, 9),
    headerSort: false,
    ...commonObjectMetric,
    width: '8%',
  },
  {
    title: TEXTS_INCO[DEFAULT_LANGUAGE].get('derechos_reconocidos_netos'),
    field: DERECHOS_RECONOCIDOS_NETOS_COLUMN,
    titleFormatter: (cell) => formatNameHeaderColumn(cell, 9),
    headerSort: false,
    ...commonObjectMetric,
  },
  {
    title: TEXTS_INCO[DEFAULT_LANGUAGE].get('recaudacion_neta'),
    field: RECAUDACION_NETA_COLUMN,
    titleFormatter: (cell) => formatNameHeaderColumn(cell, 11),
    headerSort: false,
    ...commonObjectMetric,
  },
  {
    title: TEXTS_INCO[DEFAULT_LANGUAGE].get('derechos_pendientes_cobro'),
    field: DERECHOS_PEND_COBRO_COLUMN,
    titleFormatter: (cell) => formatNameHeaderColumn(cell, 19),
    headerSort: false,
    ...commonObjectMetric,
  },
];

export function tableINCO(datos) {
  const idChart = 'tableInco';
  const nombreFichero = 'Ingreso Corriente';
  const elementeHTMLPDF = 'download-pdf';
  const elementeHTMLCSV = 'download-csv';

  // Cambio de la fecha del titulo
  const fechaTitulo = formatMonthAnyo(resultsetWithFields(datos)[0].fecha);
  document.querySelector('#ultimaFecha').textContent = fechaTitulo;

  // Datos que se mostrará en la tabla
  const dataObj = datos.resultset.map((item) => ({
    [CAPITULO_COLUMN]: item[1],
    [SUBCONCEPTO_COLUMN]: item[2],
    [PREV_INICIAL_COLUMN]: item[3],
    [MOD_INCIAL_COLUMN]: item[4],
    [PREV_DEFINITIVA_COLUMN]: item[5],
    [DERECHOS_RECONOCIDOS]: item[6],
    [DERECHOS_ANULADOS_COLUMN]: item[7],
    [DERECHOS_CANCELADOS_COLUMN]: item[8],
    [DERECHOS_RECONOCIDOS_NETOS_COLUMN]: item[9],
    [RECAUDACION_NETA_COLUMN]: item[10],
    [DERECHOS_PEND_COBRO_COLUMN]: item[11],
  }));

  const option = {
    height: '100%',
    scrollable: true,
    layout: 'fitDataTable',
    groupBy: CAPITULO_COLUMN,
    columnCalcs: 'both',
    groupClosedShowCalcs: true,
    groupToggleElement: 'header',
    columns: columnasTabla,
    data: dataObj,
  };

  const table = initTable(idChart, option);
  table.setGroupHeader((value) => `${value}`);

  // Crear listener para cada vez que cambia la visualización en la tabla
  table.on('groupVisibilityChanged', () => {
    if (checkAllGroupsCollapsed(table)) {
      document.getElementById(elementeHTMLCSV).disabled = true;
    } else {
      document.getElementById(elementeHTMLCSV).disabled = false;
    }
  });

  // Descargar tabla en CSV
  downloadCSVTable(table, nombreFichero, elementeHTMLCSV);

  // Descargar tabla PDF
  downloadPDFTable(table, nombreFichero, elementeHTMLPDF);

  // Expandir todos los grupos de la tabla
  document.getElementById('expandAll').addEventListener('click', () => {
    table.setGroupStartOpen(true);

    // Obtener todos los grupos de la tabla
    let groups = table.getGroups();
    for (let i = 0; i < groups.length; i++) {
      groups = table.getGroups();
      groups[i]._group.show();
    }
  });

  // Contraer todos los grupos de la tabla
  document.getElementById('contractAll').addEventListener('click', () => {
    table.setGroupStartOpen(true);

    // Obtener todos los grupos de la tabla
    let groups = table.getGroups();
    for (let i = 0; i < groups.length; i++) {
      groups = table.getGroups();
      groups[i]._group.hide();
    }
  });
}
