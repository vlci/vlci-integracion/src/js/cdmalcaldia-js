export const DEFAULT_LANGUAGE = 'es_ES';

export const MONTHS_YEAR = {
  es_ES: new Map([
    ['1', 'Enero'],
    ['2', 'Febrero'],
    ['3', 'Marzo'],
    ['4', 'Abril'],
    ['5', 'Mayo'],
    ['6', 'Junio'],
    ['7', 'Julio'],
    ['8', 'Agosto'],
    ['9', 'Septiembre'],
    ['10', 'Octubre'],
    ['11', 'Noviembre'],
    ['12', 'Diciembre'],
  ]),
  ca_ES: new Map([
    ['1', 'Gener'],
    ['2', 'Febrer'],
    ['3', 'Març'],
    ['4', 'Abril'],
    ['5', 'Maig'],
    ['6', 'Juny'],
    ['7', 'Juliol'],
    ['8', 'Agost'],
    ['9', 'Setembre'],
    ['10', 'Octubre'],
    ['11', 'Novembre'],
    ['12', 'Desembre'],
  ]),
};

export const TEXTS_COMMON = {
  es_ES: new Map([
    ['last_update', 'Última actualización'],
  ]),
  ca_ES: new Map([
    ['last_update', 'Última actualització'],
  ])
};
