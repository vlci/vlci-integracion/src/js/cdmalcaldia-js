import {
  printDataChart,
  printNoDataChart,
  formatTooltip,
  updateStackedBarOptions,
  resultsetWithFields,
  formatToMillions,
  formatMonthAnyo,
  initChart,
  formatShortMonthYear,
} from 'vlcishared';
import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_PMP } from './texts';

const PERIODO_MEDIO_PAGADAS = 'periodo_medio_pagadas';
const PERIODO_MEDIO_PAGO_PROVEEDORES = 'periodo_medio_pago_proveedores';
const PERIODO_MEDIO_PENDIENTES = 'periodo_medio_pendientes';
const IMPORTE_TOTAL_PAGADO = 'importe_total_pagado';
const IMPORTE_TOTAL_PENDIENTE = 'importe_total_pendiente';

const dateWithMonthLength = 6;

const optionsPMPGlobal = {
  title: {
    text: TEXTS_PMP[DEFAULT_LANGUAGE].get('pmp_ayuntamiento'),
    left: 'center',
  },
  grid: {
    left: '0',
    right: '0',
    bottom: '90',
    top: '50',
    containLabel: true,
  },
  legend: {
    data: [
      TEXTS_PMP[DEFAULT_LANGUAGE].get(IMPORTE_TOTAL_PAGADO),
      TEXTS_PMP[DEFAULT_LANGUAGE].get(IMPORTE_TOTAL_PENDIENTE),
      TEXTS_PMP[DEFAULT_LANGUAGE].get(PERIODO_MEDIO_PAGO_PROVEEDORES),
      TEXTS_PMP[DEFAULT_LANGUAGE].get(PERIODO_MEDIO_PENDIENTES),
      TEXTS_PMP[DEFAULT_LANGUAGE].get(PERIODO_MEDIO_PAGADAS),
    ],
    type: 'plain',
    orient: 'horizontal',
    bottom: '0',
    left: 'center',
    width: '100%',
    padding: 8,
    itemGap: 10,
    itemHeight: 10,
    textStyle: {
      fontSize: 10,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#333',
    },
  },
  toolbox: {
    show: true,
    feature: {
      restore: {
        title: TEXTS_PMP[DEFAULT_LANGUAGE].get('reset'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
      saveAsImage: {
        title: TEXTS_PMP[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      },
    },
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    confine: true,
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    formatter: (params) => formatTooltip(params, false, true),
  },
  xAxis: [
    {
      type: 'category',
      data: undefined,
      boundaryGap: true,
      splitLine: {
        show: false,
      },
      axisLine: {
        lineStyle: {
          color: '#ccc',
        },
      },
      axisLabel: {
        interval: 0,
        margin: 10,
        textStyle: {
          fontSize: 9,
          fontFamily: 'Montserrat',
          fontWeight: 400,
          color: '#333',
        },
        formatter: (value) => {
          if (value.length === dateWithMonthLength) {
            return formatShortMonthYear(value);
          }
          return value;
        },
      },
    },
  ],
  yAxis: [
    {
      type: 'value',
      name: TEXTS_PMP[DEFAULT_LANGUAGE].get('pmp_dias'),
      nameTextStyle: {
        padding: [-25, 0, 0, -22],
        fontSize: 10,
        fontFamily: 'Montserrat',
        fontWeight: 400,
      },
      axisLabel: {
        margin: 10,
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
      },
    },
    {
      type: 'value',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => `${formatToMillions(value)}€`,
      },
    },
  ],
  series: [
    {
      name: TEXTS_PMP[DEFAULT_LANGUAGE].get(PERIODO_MEDIO_PAGADAS),
      type: 'bar',
      yAxisIndex: 0,
      data: [],
      color: COLORS.naranjaPalido,
    },
    {
      name: TEXTS_PMP[DEFAULT_LANGUAGE].get(PERIODO_MEDIO_PENDIENTES),
      type: 'bar',
      yAxisIndex: 0,
      data: [],
      color: COLORS.amarilloPalido,
    },
    {
      name: TEXTS_PMP[DEFAULT_LANGUAGE].get(PERIODO_MEDIO_PAGO_PROVEEDORES),
      type: 'bar',
      yAxisIndex: 0,
      data: [],
      color: COLORS.tealPalido,
    },
    {
      name: TEXTS_PMP[DEFAULT_LANGUAGE].get(IMPORTE_TOTAL_PENDIENTE),
      type: 'line',
      yAxisIndex: 1,
      data: [],
      color: COLORS.cranberry,
      lineStyle: {
        width: 2,
        type: 'solid',
      },
    },
    {
      name: TEXTS_PMP[DEFAULT_LANGUAGE].get(IMPORTE_TOTAL_PAGADO),
      type: 'line',
      yAxisIndex: 1,
      data: [],
      color: COLORS.azul,
      lineStyle: {
        width: 2,
        type: 'solid',
      },
    },
  ],
  media: [
    {
      query: {
          minWidth: 505,
      },
      option: {
        grid: {
          bottom: '90',
        },
        legend: {
          borderColor: 'orange',
        },
      },
    },
    {
      query: {
          minWidth: 540,
      },
      option: {
        grid: {
          bottom: '90',
        },
      },
    },
    {
      query: {
          minWidth: 555,
        },
      option: {
        grid: {
          bottom: '80',
        },
        legend: {
          textStyle: {
            fontSize: 11,
          },
        },
      },
    },
    {
      query: {
          minWidth: 570,
        },
      option: {
        grid: {
          bottom: '68',
        },
        legend: {
          textStyle: {
            fontSize: 10,
          },
        },
      },
    },
    {
      query: {
          minWidth: 610,
      },
      option: {
        grid: {
          bottom: '90',
        },
        legend: {
          textStyle: {
            fontSize: 11,
          },
          padding: 10,
          itemGap: 10,
        },
        xAxis: [
          {
            axisLabel: {
              textStyle: {
                fontSize: 10,
              },
            },
          },
        ],
      },
    },
    {
      query: {
          minWidth: 710,
      },
      option: {
        grid: {
          bottom: '68',
        },
      },
    },
    
    {
      query: {
          minWidth: 880,
      },
      option: {
        grid: {
          bottom: '55',
        },
        legend: {
          textStyle: {
            fontSize: 11,
          },
          borderColor: 'cornflowerblue',
        },
      },
    },
  ],
};

const optionsPMPEntidades = {
  title: {
    text: TEXTS_PMP[DEFAULT_LANGUAGE].get('pmp_entidades'),
    left: 'center',
    itemGap: 5,
    subtextStyle: {
      fontFamily: 'Montserrat',
      fontSize: 13,
      fontWeight: 500,
      color: '#666',
      width: '100%',
    },
  },
  grid: {
    left: '0',
    right: '1',
    bottom: '0',
    top: '50',
    containLabel: true,
  },
  toolbox: {
    show: true,
    feature: {
      restore: {
        title: TEXTS_PMP[DEFAULT_LANGUAGE].get('reset')
      },
      saveAsImage: {
        title: TEXTS_PMP[DEFAULT_LANGUAGE].get('download'),
      },
    },
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
    },
    formatter: (params) => formatTooltip(params, false, true),
  },
  legend: {show: false,},
  xAxis: [
    {
      type: 'category',
      data: [],
      boundaryGap: ['20%', '20%'],
      gridIndex: 0 ,
      offset: 0,
      axisLine: {
        lineStyle: {
          color: '#ccc',
        },
      },
      axisLabel: {
        show: true,
        interval: 0,
        rotate: 0,
        padding: 0,
        margin: 10,
        width: 39,
        overflow: 'break',
        verticalAlign: 'top',
        fontSize: 9,
        lineHeight: 11,
        fontFamily: 'Montserrat',
        fontWeight: 400,
        color: '#333',
      },
    },
  ],
  yAxis: [
    {
      type: 'value',
      name: TEXTS_PMP[DEFAULT_LANGUAGE].get('pmp_dias'),
      nameTextStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
        fontWeight: 400,
        padding: [-25, 0, 0, -22],
      },
      position: 'left',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
      },
    },
  ],
  series: [
    {
      name: TEXTS_PMP[DEFAULT_LANGUAGE].get('dias'),
      type: 'bar',
      barWidth: 20,
      data: [],
      color: COLORS.tealPalido,
    },
  ], 
  media: [
    {
      query: {
          minWidth: 600,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              width: 44,
            },
          },
        ],
        series: [
          {
            barWidth: 28,
          },
        ],
      },
    },
    {
      query: {
          minWidth: 615,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              width: 42,
            },
          },
        ],
      },
    },
    {
      query: {
          minWidth: 675,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              width: 46,
            },
          },
        ],
        series: [
          {
            barWidth: 28,
          },
        ],
      },
    },
    {
        query: {
            minWidth: 705,
        },
        option: {
          xAxis: [
            {
              axisLabel: {
                width: 49,
              },
            },
          ],
          series: [
            {
              barWidth: 28,
            },
          ],
        },
    },
    {
        query: {
            minWidth: 880,
        },
        option: {
          xAxis: [
            {
              axisLabel: {
                width: 58,
              },
            },
          ],
        },
    },
    {
        query: {
            minWidth: 930,
        },
        option: {
          xAxis: [
            {
              axisLabel: {
                width: 60,
                fontSize: 10,
              },
            },
          ],
        },
    },
    {
      query: {
          minWidth: 1020,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              width: 68,
            },
          },
        ],
      },
    },
    {
      query: {
          minWidth: 1060,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              width: 70,
            },
          },
        ],
      },
    },
    {
      query: {
          minWidth: 1140,
      },
      option: {
        xAxis: [
          {
            axisLabel: {
              width: 74,
            },
          },
        ],
      },
    },
  ],
};

export function echartPMPAyuntamiento(datos) {
  const idChart = 'chartPMPAyuntamiento';
  const idNoData = 'no_dataPMPAyuntamiento';

  if (datos.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    let updatedOptions = {};
    updatedOptions = updateStackedBarOptions(optionsPMPGlobal);

    // Reiniciar datos
    updatedOptions.xAxis[0].data = [];
    updatedOptions.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);

    resultSet.forEach((row) => {
      updatedOptions.xAxis[0].data.push(
        `${row.periodicidad_gasto__desccortoval}`,
      );
      updatedOptions.series[0].data.push(row[PERIODO_MEDIO_PAGADAS]);
      updatedOptions.series[1].data.push(row[PERIODO_MEDIO_PENDIENTES]);
      updatedOptions.series[2].data.push(row[PERIODO_MEDIO_PAGO_PROVEEDORES]);
      updatedOptions.series[3].data.push(row[IMPORTE_TOTAL_PENDIENTE]);
      updatedOptions.series[4].data.push(row[IMPORTE_TOTAL_PAGADO]);
    });

    initChart(idChart, updatedOptions);
  }
}

export function echartPMPEntidades(datos) {
  const idChart = 'chartPMPEntidades';
  const idNoData = 'no_dataPMPEntidades';

  if (datos.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    let updatedOptions = {};
    updatedOptions = updateStackedBarOptions(optionsPMPEntidades);

    // Reiniciar datos
    updatedOptions.xAxis[0].data = [];
    updatedOptions.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);

    updatedOptions.title.subtext = formatMonthAnyo(resultSet[0].fecha_estado);

    resultSet.forEach((row) => {
      updatedOptions.xAxis[0].data.push(`${row.entidad}`);
      updatedOptions.series[0].data.push(row.suma_pmp);
    });

    initChart(idChart, updatedOptions);
  }
}
