import {
  initTable,
  formatNameHeaderColumn,
  addPointInteger,
  getMaxFieldNumberFromData,
  formatMonthAnyo,
  resultsetWithFields,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_PMP } from './texts';

const COD_ORG_COLUMN = 'codigo_organico';
const DESCRIPCION_COLUMN = 'descripcion';
const IMPACTO_PMP_COLUMN = 'tiempo_importe_pendiente';
const PMO_PAGADAS_COLUMN = 'pmo_pagadas';
const PMO_PENDIENTES_COLUMN = 'pmo_pendientes';
const PMP_DIAS_COLUMN = 'pmp_dias';
const PMO_IMP_PAGADO_COLUMN = 'importe_total_pagado';
const PMO_IMP_PEND_COLUMN = 'importe_total_pend_pago';

// Propiedades de las columnas de la tabla
const columnasTabla = [
  {
    title: TEXTS_PMP[DEFAULT_LANGUAGE].get('cod_organico'),
    field: COD_ORG_COLUMN,
    width: '8%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 7);
    },
  },
  {
    title: TEXTS_PMP[DEFAULT_LANGUAGE].get('desc_cod_organico'),
    field: DESCRIPCION_COLUMN,
    sorter: 'string',
    width: '20%',
  },
  {
    title: TEXTS_PMP[DEFAULT_LANGUAGE].get('tiempo_importe_pendiente'),
    field: IMPACTO_PMP_COLUMN,
    width: '12%',
    hozAlign: 'left',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 14);
    },

    formatter: 'progress',
    formatterParams: {
      min: 0,
      max: 100,
      color: COLORS['tealPalido'],
    },
  },
  {
    title: TEXTS_PMP[DEFAULT_LANGUAGE].get('dias'),
    field: PMP_DIAS_COLUMN,
    width: '12%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 14);
    },
    formatter: function formatDecimal(cell) {
      return addPointInteger(cell.getValue());
    },
  },
  {
    title: TEXTS_PMP[DEFAULT_LANGUAGE].get('pmo_pagadas'),
    field: PMO_PAGADAS_COLUMN,
    width: '12%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 17);
    },
    formatter: function formatDecimal(cell) {
      return addPointInteger(cell.getValue());
    },
  },
  {
    title: TEXTS_PMP[DEFAULT_LANGUAGE].get('pmo_pendientes'),
    field: PMO_PENDIENTES_COLUMN,
    width: '12%',
    hozAlign: 'center',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 17);
    },
    formatter: function formatDecimal(cell) {
      return addPointInteger(cell.getValue());
    },
  },
  {
    title: TEXTS_PMP[DEFAULT_LANGUAGE].get('imp_pagado'),
    field: PMO_IMP_PAGADO_COLUMN,
    width: '12%',
    hozAlign: 'right',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 14);
    },
    formatter: 'money',
    formatterParams: {
      decimal: ',',
      thousand: '.',
      symbol: '€',
      symbolAfter: 'p',
      negativeSign: true,
      precision: false,
    },
  },
  {
    title: TEXTS_PMP[DEFAULT_LANGUAGE].get('imp_pendiente'),
    field: PMO_IMP_PEND_COLUMN,
    width: '12%',
    hozAlign: 'right',
    titleFormatter: function format(cell) {
      return formatNameHeaderColumn(cell, 14);
    },
    formatter: 'money',
    formatterParams: {
      decimal: ',',
      thousand: '.',
      symbol: '€',
      symbolAfter: 'p',
      negativeSign: true,
      precision: false,
    },
  },
];

export function tablePMP(vis, datos) {
  const idChart = 'tablePMP';

  // Cambio de la fecha del titulo

  const fechaTitulo = formatMonthAnyo(
    resultsetWithFields(datos)[0].fecha_estado,
  );
  document.querySelector('#ultimaFecha').textContent = fechaTitulo;

  // Datos que se mostrará en la tabla
  const maxImpactoPmp = getMaxFieldNumberFromData(datos, IMPACTO_PMP_COLUMN);
  columnasTabla[2].formatterParams.max = maxImpactoPmp;
  const dataObj = datos.resultset.map((item) => ({
    [COD_ORG_COLUMN]: item[1],
    [DESCRIPCION_COLUMN]: item[2],
    [IMPACTO_PMP_COLUMN]: item[3],
    [PMO_PAGADAS_COLUMN]: item[4],
    [PMO_PENDIENTES_COLUMN]: item[5],
    [PMP_DIAS_COLUMN]: item[6],
    [PMO_IMP_PAGADO_COLUMN]: item[7],
    [PMO_IMP_PEND_COLUMN]: item[8],
  }));

  const option = {
    height: '100%',
    scrollable: true,
    layout: 'fitDataTable',
    columns: columnasTabla,
    data: dataObj,
  };

  initTable(idChart, option);
}
