export { getSelectCommonOptions, selectFilterAll } from 'vlcishared';
export {
  echartPMPAyuntamiento,
  echartPMPEntidades,
} from './chart-pmp-LineChart';
export { tablePMP } from './table-pmp';
