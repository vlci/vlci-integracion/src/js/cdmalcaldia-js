export const TEXTS_CONTRATOS_DETAIL = {
  es_ES: new Map([
    ['imp_sin_iva', 'IMPORTE (SIN IVA)'],
    ['cont_contratos', 'NÚMERO DE CONTRATOS MENORES'],
    ['chartNameImpArea', 'IMPORTE (SIN IVA)'],
    ['reset', 'Restablecer'],
    ['download', 'Descargar'],
    ['objeto_contrato', 'OBJETO DEL CONTRATO'],
    ['leyendNoVAT', '€ (sin iva)'],
  ]),
  ca_ES: new Map([
    ['imp_sin_iva', 'IMPORT (SENSE IVA)'],
    ['cont_contratos', 'NOMBRE DE CONTRACTES MENORS'],
    ['chartNameImpArea', 'IMPORT (SENSE IVA)'],
    ['reset', 'Restablir'],
    ['download', 'Desar'],
    ['objeto_contrato', 'OBJECTE DEL CONTRACTE'],
    ['leyendNoVAT', '€ (sense iva)'],
  ]),
};
