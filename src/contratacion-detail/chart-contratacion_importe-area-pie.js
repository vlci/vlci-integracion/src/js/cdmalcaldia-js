import { resultsetWithFields, addPointInteger, initChart, formatTooltipPie } from 'vlcishared';
import { COLORS } from '../colors';
import { TEXTS_CONTRATOS_DETAIL } from './texts';
import { DEFAULT_LANGUAGE } from '../common-language';

const option = {
  color: [
    COLORS['naranjaPalido'],
    COLORS['tealPalido'],
    COLORS['amarilloPalido'],
    COLORS['azulPalido'],
    COLORS['cranberryPalido'],
    COLORS['marinoPalido'],
    COLORS['naranja'],
    COLORS['teal'],
    COLORS['amarillo'],
    COLORS['azulCeruleo'],
    COLORS['cranberry'],
    COLORS['marino'],
  ],
  grid: {
    left: '0',
    right: '0',
    bottom: '0',
    top: '50',
    containLabel: true,
  },
  title: {
    text: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('chartNameImpArea'),
    left: 'center',
    top: '0',
    textStyle: {
      fontSize: 15,
      fontFamily: 'Montserrat',
      fontWeight: 500,
      color: '#666',
    },
  },
  legend: {
    selectedMode: false,
    orient: 'vertical',
    top: '50',
    left: 'right',
    itemHeight: '11',
    textStyle: {
      fontSize: 9,
      lineHeight: 13,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
      width: '180',
      overflow: 'break',
    },
  },  
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      restore: {
        title: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('reset'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        }, 
      },
      saveAsImage: {
        title: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      }
    },
    emphasis: { 
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },        
    },
  },
  tooltip: {
    trigger: 'item',
    confine: true,
    valueFormatter(value) {
      return addPointInteger(value)+ '€';
    },
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 11,
      fontWeight: 600,
    },
  },
  series: [
    {
      name: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('chartNameImpArea'),
      type: 'pie',
      radius: '43%',
      center: ['28%', '50%'],
      data: [],
      label: {
        show: true,
        formatter(param) {
          return `${addPointInteger(param.percent)}%`;
        },
        textStyle: {
          fontFamily: 'Montserrat',
        },
      },
      labelLine: {
        length: 2,
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  ],
  media: [
    {
        query: {
            minWidth: 520,
        },
        option: {
            legend: {
              top: 'middle',
              textStyle: {
                fontSize: 11,
                width: 'auto',
                overflow: 'none',
              },
            },
            series:
              {
                radius: '40%',
                center: ['25%', '50%'],
              },
        },
    },
    {
        query: {
            minWidth: 610,
        },
        option: {
            series:
              {
                radius: '55%',
                center: ['25%', '50%'],
              },
        },
    },
    {
        query: {
            minWidth: 635,
        },
        option: {
            legend: {
              textStyle: {
                fontSize: 11,
              },
            },
            series:
              {
                radius: '60%',
              },
        },
    },
    {
        query: {
            minWidth: 650,
        },
        option: {
            series:
              {
                radius: '70%',
                center: ['28%', '50%'],
                labelLine: {
                  length: 5,
                },
              }, 
        },
    },
    {
        query: {
            minWidth: 730,
        },
        option: {
            series:
              {
                radius: '65%',
              }, 
        },
    },
    {
        query: {
            minWidth: 690,
            maxHeight: 300,
        },
        option: {
            legend: {
              top: 45,
              itemHeight: '9',
              textStyle: {
                fontSize: 9,
              },
            }, 
        },
    },
    {
        query: {
            minWidth: 700,
        },
        option: {
            series:
              {
                radius: '75%',
                center: ['30%', '50%'],
              }, 
        },
    },
    {
        query: {
            minWidth: 1000,
        },
        option: {
            series:
              {
                radius: '70%',
              }, 
        },
    },
  ],
};

export function echartContratosAreaPie(vis, datos) {
  // Reiniciar datos
  option.series[0].data = [];

  const resultSet = resultsetWithFields(datos);
  resultSet.forEach((row) => {
    option.series[0].data.push({ value: row.importe_sin_iva, name: row.area });
  });
  initChart('chartImporteArea', option);
}
