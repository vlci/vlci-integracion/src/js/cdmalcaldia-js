import {
  printNoDataChart,
  printDataChart,
  initChart,
  addPointInteger,
  configureDataBar,
  formatMonthAnyo,
  formatShortMonthYear,
  formatTooltip,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_CONTRATOS_DETAIL } from './texts';

const colors = [
  COLORS['amarilloPalido'],
  COLORS['naranjaPalido'],
  COLORS['tealPalido'],
  COLORS['azulPalido'],
  COLORS['cranberryPalido'],
  COLORS['marinoPalido'],
  COLORS['naranja'],
  COLORS['teal'],
  COLORS['amarillo'],
  COLORS['azulCeruleo'],
  COLORS['cranberry'],
  COLORS['marino'],
];

/**
 * Opciones por defecto para todos los graficos stacked bar
 * Atributos que son necesarios rellenar : title{text},data,series.
 */
const options = {
  grid: {
    left: '0',
    right: '0',
    bottom: '30',
    top: '50',
    containLabel: true,
  },
  legend: {
    data: [],
    orient: 'horizontal',
    bottom: '0',
    left: 'center',
    width: '100%',
    textStyle: {
      fontSize: 10,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      restore: {
        title: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('reset'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        }, 
      },
      saveAsImage: {
        title: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      }
    },
    emphasis: { 
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },        
    },
  },
  tooltip: {
    trigger: 'axis',
    confine: true,
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    formatter: (params) => {
      params[0].name = formatMonthAnyo(params[0].name);
      return formatTooltip(params, false, true, '€');
    },
  },
  xAxis: [
    {
      type: 'category',
      axisLabel: {
        show: true,
        interval: 0,
        margin: 10,
        textStyle: {
          fontSize: 11,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => {
          if (value.length === dateWithMonthLength) {
            return formatShortMonthYear(value);
          }
          return value;
        },
        formatter: (value) => formatShortMonthYear(value),
      },
      data: [],
    },
  ],
  yAxis: [
    {
      type: 'value',
      position: 'left',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => addPointInteger(value) + TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('leyendNoVAT'),
      },
    },
  ],
  series: [],
  media: [
    {
        query: {
            minWidth: 675,
        },
        option: {
              legend: {
                textStyle: {
                  fontSize: 11,
                },
              },
        },
    },
  ],
};

export function echartBarContratosTipoMes(vis, datos) {
  
  
  const idChart = 'chartImporteTipoMes';
  const idNoData = 'no_dataImporteTipoMes';
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    const [series, date, tiposArray] = configureDataBar(
      datos.resultset,
      colors.slice(),
      false,
    );
    if (tiposArray) {
      options.legend.data = tiposArray;
    }

    options.xAxis[0].data = Array.from(date.values());
    options.series = series;

    initChart(idChart, options);
  }
}
