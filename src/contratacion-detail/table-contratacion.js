import {
  initTable,
  formatCellMultiplesLines,
  downloadCSVTable,
  downloadPDFTable,
} from 'vlcishared';

import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_CONTRATOS_DETAIL } from './texts';

const CONTRATO_COLUMN = 'objeto_contrato';
const IMPORTE_SIN_IVA_COLUMN = 'importe_sin_iva';

const formatterMoney = {
  decimal: ',',
  thousand: '.',
  symbol: '€',
  symbolAfter: 'p',
  precision: false,
};

const commonObjectMetric = {
  width: '20%',
  hozAlign: 'center',
  formatter: 'money',
  formatterParams: formatterMoney,
  topCalc: 'sum',
  topCalcParams: {
    precision: 2,
  },
  topCalcFormatter: 'money',
  topCalcFormatterParams: formatterMoney,
};

// Propiedades de las columnas de la tabla
const columnasTabla = [
  {
    title: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('objeto_contrato'),
    field: CONTRATO_COLUMN,
    sorter: 'string',
    width: '80%',
    hozAlign: 'left',
    formatter: (cell) => formatCellMultiplesLines(cell, 78),
  },
  {
    title: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('imp_sin_iva'),
    field: IMPORTE_SIN_IVA_COLUMN,
    ...commonObjectMetric,
  },
];

export function tableContratacion(vis, datos) {
  const idChart = 'tableContratos';
  const nombreFichero = 'Contratos Menores';
  const elementeHTMLPDF = 'download-pdf';
  const elementeHTMLCSV = 'download-csv';

  // Datos que se mostrará en la tabla
  const dataObj = datos.resultset.map((item) => ({
    [CONTRATO_COLUMN]: item[0],
    [IMPORTE_SIN_IVA_COLUMN]: item[1],
  }));

  const option = {
    height: '100%',
    scrollable: true,
    layout: 'fitDataTable',
    columnCalcs: 'both',
    columns: columnasTabla,
    data: dataObj,
  };

  const table = initTable(idChart, option);

  // Descargar tabla en CSV
  downloadCSVTable(table, nombreFichero, elementeHTMLCSV);

  // Descargar tabla PDF
  downloadPDFTable(table, nombreFichero, elementeHTMLPDF);
}
