import {
  resultsetWithFields,
  printNoDataChart,
  printDataChart,
  initChart,
  addPointInteger,
  formatTooltip,
} from 'vlcishared';

import { COLORS } from '../colors';
import { DEFAULT_LANGUAGE } from '../common-language';
import { TEXTS_CONTRATOS_DETAIL } from './texts';

const TIPO_COLUMN = 'tipo_contrato';
const IMP_SIN_IVA_COLUMN = 'importe_sin_iva';
const COUNT_CONTRATOS_COLUMN = 'cont_contratos';

/**
 * Opciones por defecto para todos los graficos stacked bar
 * Atributos que son necesarios rellenar : title{text},data,series.
 */
const options = {
  grid: {
    left: '0',
    right: '0',
    bottom: '30',
    top: '90',
    containLabel: true,
  },
  legend: {
    data: [
      TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('imp_sin_iva'),
      TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('cont_contratos'),
    ],
    orient: 'horizontal',
    bottom: '0',
    left: 'center',
    width: '100%',
    textStyle: {
      fontSize: 11,
      fontFamily: 'Montserrat',
      fontWeight: 400,
      color: '#666',
    },
  },
  toolbox: {
    show: true,
    right: '0',
    top: '0',
    feature: {
      restore: {
        title: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('reset'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        }, 
      },
      saveAsImage: {
        title: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('download'),
        textStyle: {
          fontSize: 6,
          fontFamily: 'Montserrat',
          fontWeight: 300,
        },
      }
    },
    emphasis: { 
      iconStyle: {
        borderColor: '#ffcd00',
        textFill: '#666',
      },        
    },
  },
  tooltip: {
    trigger: 'axis',
    confine: true,
    textStyle: {
      fontFamily: 'Montserrat',
      fontSize: 12,
    },
    valueFormatter(value) {
      return addPointInteger(value);
    },
    formatter: (params) => formatTooltip(params, true, true, '€'),
  }, 
  xAxis: [
    {
      type: 'category',
      axisLabel: {
        show: true,
        interval: 0,
        margin: 10,
        textStyle: {
          fontSize: 11,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
      },
      data: [],
    },
  ],
  yAxis: [
    {
      type: 'value',
      alignTicks: true ,
      position: 'right',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => `${(value)}`,
      },
    },
    {
      type: 'value',
      position: 'left',
      axisLabel: {
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          fontWeight: 400,
        },
        formatter: (value) => addPointInteger(value) + TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('leyendNoVAT'),
      },
    },
  ],
  series: [
    {
      name: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('imp_sin_iva'),
      yAxisIndex: 1,
      type: 'bar',
      barMaxWidth: 90,
      data: [],
      color: COLORS['naranjaPalido'],
    },
    {
      name: TEXTS_CONTRATOS_DETAIL[DEFAULT_LANGUAGE].get('cont_contratos'),
      type: 'line',
      data: [],
      color: COLORS['tealPalido'],
    },
  ],
};

export function echartBarContratosTipo(vis, datos) {
  const idChart = 'chartImporteTipo';
  const idNoData = 'no_dataImporteTipo';
  if (datos.resultset.length === 0) {
    printNoDataChart(`#${idChart}`, `#${idNoData}`);
  } else {
    printDataChart(`#${idNoData}`, `#${idChart}`);

    // Reiniciar datos
    options.xAxis[0].data = [];
    options.series.forEach((serie) => {
      serie.data = [];
    });

    const resultSet = resultsetWithFields(datos);
    resultSet.forEach((row) => {
      options.xAxis[0].data.push(`${row[TIPO_COLUMN]}`);
      options.series[0].data.push(row[IMP_SIN_IVA_COLUMN]);
      options.series[1].data.push(row[COUNT_CONTRATOS_COLUMN]);
    });

    initChart(idChart, options);
  }
}
