const path = require('path');
const webpack = require('webpack');

module.exports = {
  devServer: {
    port: 8888,
    client: {
      overlay: false,
    },
  },
  devtool: 'eval-cheap-module-source-map',
  mode: 'development',
  entry: {
    initialCalls: './src/index-initial-calls.js',
    inicio: './src/inicio/index-inicio.js',
    agendaDigital: '/src/agenda-digital/index-agenda-digital.js',
    medioambiente: './src/medioambiente/index-medioambiente.js',
    mobilidad: './src/mobilidad/index-mobilidad.js',
    recursosHumanos: './src/recursoshumanos/index-recursos-humanos.js',
    participacion: './src/participacion/index-participacion.js',
    pmp: './src/pmp/index-pmp.js',
    gastos: './src/gastos/index-gastos.js',
    ingresos: './src/ingresos/index-ingresos.js',
    contratacion: './src/contratacion/index-contratacion.js',
    empleo: './src/empleo/index-empleo.js',
    inco: './src/inco-detail/index-inco.js',
    gaco: './src/gaco-detail/index-gaco.js',
    comunicacion: './src/comunicacion/index-comunicacion',
    contratos_detail: './src/contratacion-detail/index-contratacion-detail.js',
    navigation: './src/navigation/index-navigation.js',
    pmp_detail: './src/pmp-detail/index-pmp-detail.js',
    personal: './src/personal/index-personal-detail.js',
    seguridad: './src/seguridad/index-seguridad.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    filename: '[name].js',
    library: '[name]',
    libraryTarget: 'umd',
    clean: true,
    publicPath: '/dist/js/',
  },
  plugins: [
    // fix "process is not defined" error:
    // (do "npm install process" before running the build)
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
  ],
  externals: {
    echarts: 'echarts',
    'tabulator-tables': 'tabulator-tables',
  },
  resolve: {
    alias: {
      vlcishared: path.resolve(
        __dirname,
        'node_modules/@vlci/vlcishared/dist/js/vlcishared.js',
      ),
    },
  },
};
